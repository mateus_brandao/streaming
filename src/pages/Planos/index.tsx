import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';
import visto from '../../assets/imagens/visto.png';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

function Checkout(){
    return(
        <>

        <div id="checkout-container">
            
                <div id="title-paso2">
                    <p>PASSO <strong>3</strong> de <strong>4</strong></p>
                    <h3>Escolha o melhor plano para você</h3>
                    <p>Troque de plao quando quiser.</p>
                </div>
                <div id="cards" className="card-plans">
                    <div className="card-option">
                         <h4>Basico</h4>
                         <i className="fas fa-circle"></i>
                         <p>Lorem Ipsum é simplesmente um texto fictício da indústria de impressão e composição.</p>
                         <i className="fas fa-circle"></i>
                         <p>quando um impressor desconhecido pegou uma galé do tipo e embaralhou para fazer um livro de amostra de tipos.</p>
                         <i className="fas fa-circle"></i>
                         <p>É um fato estabelecido há muito tempo que um leitor se distrairá com</p>
                         <div  className="btn-plan">  <Link className="link" to="/checkout">Escolher plano</Link> </div>
                    </div>
                    <div className="card-option">
                         <h4>Padrão</h4>
                         <i className="fas fa-circle"></i>
                         <p>Lorem Ipsum é simplesmente um texto fictício da indústria de impressão e composição.</p>
                         <i className="fas fa-circle"></i>
                         <p>quando um impressor desconhecido pegou uma galé do tipo e embaralhou para fazer um livro de amostra de tipos.</p>
                         <i className="fas fa-circle"></i>
                         <p>É um fato estabelecido há muito tempo que um leitor se distrairá com</p>
                         <div  className="btn-plan">  <Link className="link" to="/checkout">Escolher plano</Link> </div>
                         
                    </div>
                    <div className="card-option">
                         <h4>Premium</h4>
                         <i className="fas fa-circle"></i>
                         <p>Lorem Ipsum é simplesmente um texto fictício da indústria de impressão e composição.</p>
                         <i className="fas fa-circle"></i>
                         <p>quando um impressor desconhecido pegou uma galé do tipo e embaralhou para fazer um livro de amostra de tipos.</p>
                         <i className="fas fa-circle"></i>
                         <p>É um fato estabelecido há muito tempo que um leitor se distrairá com</p>
                         <div  className="btn-plan">  <Link className="link" to="/checkout">Escolher plano</Link> </div>
                        
                    </div>
                </div>
           

        </div>
        <Footer/>
        </>
    )
}

export default Checkout;
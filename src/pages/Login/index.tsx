import React, { Props, useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Footer from '../../components/Footer';
import api from '../../services/api';
import { GeneralContext } from '../../contexts/GeneralContext';
import Imgdisp from '../../assets/imagens/Asset.jpg';
// redes sociais
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import AppleLogin from 'react-apple-login';
import { tokenToString } from 'typescript';
function Login() {
    // respostas das redes sociais
    async function responseFacebook(response) {
        const data = new FormData();
        data.append('name', response.name)
        data.append('email', response.name)
        data.append('token', response.userID)
        data.append('social_network', 'facebook')
        api.post('/socialLogin', data)
            .then(response => {
               if(response.data.result){
                   console.log(response.data)
               }
            })
            .catch(function (error) {
                console.log('erro')
            })
    }

    async function responseGoogle(response){
        const data = new FormData();
        data.append('token', response.googleId);
        data.append('email', response.profileObj.email);
        data.append('name', response.profileObj.givenName);
        data.append('social_network', 'google')
        api.post('/socialLogin', data)
         .then(response =>{
             console.log(response.data)
         })
         .catch(function(error){
             console.log('erro')
         })
        console.log(response);
    }
   
    const idGoogle = '493230704833-jl67nrvh43s5krjhchh0b6id240puf5l.apps.googleusercontent.com';
    const idFacebook = '2627295150727407';
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [rememberMe, setRememberMe] = useState(false);
    const history = useHistory();
    const [accessEvent, setAccessEvent] = useState(false);
    const { setOpen, setMessage } = useContext(GeneralContext);

    useEffect(() => {
        if(accessEvent == true){
            validateLogin()
        }else{
            return
        }

    },[accessEvent]);
   
    useEffect(() => {
        if (sessionStorage.getItem('magnificat_access') != null) history.push('/inicio');
        else {
            if (localStorage.getItem('magnificat_access') != null) {
                sessionStorage.setItem('magnificat_access', localStorage.getItem('magnificat_access') || '');
                history.push('/inicio');
            }
        }
    }, []);

    function validateLogin() {
        if (email.length > 5 && email.indexOf('@') != -1 && email.indexOf('.') != -1) {
            if (password.length > 6) sendLogin();
            else message('A senha deve conter no mínimo 6 caracteres');
        }
        else message('Insira um email válido');
        
    }
    window.addEventListener('keydown', function(e){
        const elemente = e.key;
        if(elemente === 'Enter'){
            setAccessEvent(true)
        }
    })
    
    console.log(accessEvent)

    function sendLogin() {
        const data = new FormData();
        data.append('email', email);
        data.append('password', password);
        api.post('auth/login', data).then(response => {
            if (response.data.result) {
                if (response.data.response.original.access_token) {
                    let access_token = response.data.response.original.access_token;
                    if (rememberMe) localStorage.setItem('magnificat_access', access_token);

                    sessionStorage.setItem('magnificat_access', access_token);
                    // window.location.href = "https://magnificat.video/live/";
                    history.push('/inicio');
                } else{
                     message('Houve um erro inesperado ao tentar realizar o login.');
                     setAccessEvent(false)
                }
            }
            else{
            message(response.data.response);
            setAccessEvent(false)
            }
        });
    }
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    return (
        <div id="container_login">
            <div className="login_content">
                <div className="login_container_left">
                    <img className="logo-sobre-efect" src={Imgdisp} alt="" />
                    <div className="efect3"></div>
                </div>

                <div className="login-container__right">
                    <div className="login_content-rigth">
                        <h1> Faça login </h1>
                        <Link to="/" className="sub">Ou <span className="active_login">cadastre-se</span> para assistir</Link>
            
                        <div className="group-input-user">
                            <input type="email" placeholder="nome no usuário ou email"
                                value={email} onChange={event => setEmail(event.target.value)} />
                            <input type="password" placeholder="Senha"
                                value={password} onChange={event => setPassword(event.target.value)} />
                        </div>


                        <div className="login_input-group">
                            <fieldset>
                                <input type="checkbox" name="Flembrete" id="Flembrete"
                                    onClick={() => setRememberMe(!rememberMe)} />
                                <label className="" htmlFor="Flembrete">Lembre-me</label>
                            </fieldset>
                            <Link to="/redefinir-senha" className="lembre-me" href="#">Esqueci minha senha</Link>
                        </div>
                        {/*Botão oculto em */}
                        <button onClick={() =>  setAccessEvent(true)} className="btn btn_login"><span>Entrar</span></button>
                    
                        <span className="txt-btn-social">Ou entre com sua rede social preferida</span>

                        {/* <button disabled={true} className="btn-social btn-apple"><i className="fab fa-apple"></i>Continuar com a conta apple
                        </button> */}
                        {/* <AppleLogin clientId="com.react.apple.login" redirectURI="https://redirectUrl.com" /> */}
                        <GoogleLogin
                            clientId={idGoogle}
                            render={renderProps => (
                                <button className="btn-social" onClick={renderProps.onClick} disabled={renderProps.disabled}><i className="fab fa-google"></i>Continuar com a conta google</button>
                            )}
                            buttonText="Login"
                            onSuccess={responseGoogle}
                            onFailure={responseGoogle}
                            cookiePolicy={'single_host_origin'}
                        />
                        <FacebookLogin
                            appId={idFacebook}
                            callback={responseFacebook}
                            render={renderProps => (
                                <button className="btn-social" onClick={renderProps.onClick}><i className="fab fa-facebook-f"></i> Continuar com o facebook</button>
                            )}
                        />

                    </div>
                </div>

            </div>


            <Footer />
        </div>

    );
}

export default Login;
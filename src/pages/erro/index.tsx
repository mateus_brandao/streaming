import React from 'react';
import BgErro from '../../assets/imagens/Logo/SVG/logomagnificat.svg'


function pageErro() {
    return (
        <div className="container-erro">
             
            <img src={BgErro} alt="" />

        
            <div className="erro_content">
                <h3>Nome do erro</h3>
                <p>Lorem Ipsum é simplesmente um texto fictício da indústria de impressão e composição. Lorem Ipsum tem sido o texto fictício padrão da indústria desde 1500,</p>
                <button className="btn-erro">Voltar</button>
            </div>
          
        </div>
    );
}
export default pageErro;
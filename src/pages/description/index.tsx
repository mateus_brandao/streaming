import React, { useContext, useEffect, useRef, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import BkgVimeo from '../../components/bkg-video/index';
import { info } from 'node:console';
import BtnBorderBlue from '../../components/Buttons/Button-border-blue';
import BtnWite from '../../components/Buttons/Button-wite';
import api from '../../services/api';
import Vimeo from '@u-wave/react-vimeo';
import { GeneralContext } from '../../contexts/GeneralContext';
import userEvent from '@testing-library/user-event';
import { Category, Genre, Trailer, Extra } from '../../types/typesdef';

interface DescriptionParams {
    id?: string;
}
interface UserMovie {
    id: number;
    user_id: number;
    movie_id: number;
    current_time: string;
    viewed: boolean;
    favorite: boolean;
    rating: string;
    movie?: Movie;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    categories: Category[];
    genres: Genre[];
    url: string;
    trailers: Trailer[];
    extras?: Extra[];
    users?: UserMovie[];
    duration: string;
    cost: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface InfoMovie {
    VimeoID: number;
    tralierVimeo: number;
    MovieTitle: string;
    indicatedAge: number;
    movieTime: string;
    genre: string;
    release: number;
    country: string;
    direction: string;
    production: string;
    Script: string;
    composition: string;
    cast: string;
    linkPlay: string;
    shortSynopsis: string;
    sinopse: string;
    expirationDate: string;
}
interface SimilarMovie {
    id: number;
    name: string;
    movies: Movie[];
}

const Description = () => {
    const params = useParams<DescriptionParams>();
    const [movie, setMovie] = useState<Movie>();
    const [similarFilm, setSimilarFilm] = useState<SimilarMovie>();
    const [infoMovie, setInfoMovie] = useState<InfoMovie>({
        'VimeoID': 0,
        'tralierVimeo': 0,
        'MovieTitle': "",
        'indicatedAge': 0,
        'movieTime': `0h`,
        'genre': "",
        'release': 0,
        'country': "",
        'direction': "",
        // 'approval': `${88}% gostaram desse filme`,
        'production': '',
        'Script': '',
        'composition': '',
        'cast': '',
        'linkPlay': '/reproduzir/0',
        'shortSynopsis': ``,
        'sinopse': '',
        'expirationDate': ''
    });
    const [vimeoId, setVimeoId] = useState<string | null>(null);
    const [tabActive, setTabActive] = useState<string>('detalhes');
    const { setOpen, setMessage } = useContext(GeneralContext);
    let btn1: any
    let btn2: any
    let btn3: any
    let ss: any

    // BEGIN:: FUNCTION CONTENT
    // function sinopse() {
    //     ss = document.getElementById('ss')
    //     btn1 = document.getElementById('btn1')
    //     btn2 = document.getElementById('btn2')
    //     btn3 = document.getElementById('btn3')
    //     btn3.classList.remove("active-abas")
    //     btn2.classList.remove("active-abas");
    //     btn1.classList.add("active-abas");
    //     ss.innerHTML = `
    //         <h5 style="padding-top: 1rem;">Conteúdo Sinopse</h5>
    //         <div style="padding-top: 1rem; padding-bottom: 1rem;">
    //         ${infoMovie.sinopse}
    //         </div>
    //     `
    // }
    function trailer() {
        let html = [] as String[];
        let htmlExtras = [] as String[];

        if (movie) {
            html = movie.trailers.map(trailer => {
                return (
                    `<div style="background-image: url('${trailer.thumbnail}'); background-size: cover; width: 310px; height: 174px; position: relative; cursor: pointer;" onclick='${() => alert(trailer.url)}'>
                    <p style="position: absolute; bottom: 0; left: 0; margin-bottom: 0; padding-bottom: 8px; padding-top: 4px; padding-left: 10px; padding-right: 10px; width: calc(100% - 20px); background: rgba(0,0,0,.5);">${trailer.name}</p>
                </div>`
                );
            });
            if (movie.extras) {
                htmlExtras = movie.extras.map(extra => {
                    return (
                        `<div style="background-image: url('${extra.thumbnail}'); background-size: cover; width: 310px; height: 174px; position: relative;">
                        <p style="position: absolute; bottom: 0; left: 0; margin-bottom: 0; padding-bottom: 8px; padding-top: 4px; padding-left: 10px; padding-right: 10px; width: calc(100% - 20px); background: rgba(0,0,0,.5);">${extra.name}</p>
                    </div>`
                    );
                });
            }
        }

        ss = document.getElementById('ss')
        btn1 = document.getElementById('btn1')
        btn2 = document.getElementById('btn2')
        btn3 = document.getElementById('btn3')
        btn3.classList.remove("active-abas")
        btn2.classList.add("active-abas");
        btn1.classList.remove("active-abas");
        ss.innerHTML = `
        <h5 style="padding-top: 1rem;">Conteúdo TRAILER & EXTRAS</h5>
        <div style="padding-top: 1rem; padding-bottom: 1rem; display: flex; gap: .5rem">
        ${html.join(' ')}
        </div>
        <div style="padding-bottom: 1rem; display: flex; gap: .5rem">
        ${htmlExtras.join(' ')}
        </div>
    `
    }
    // function detalhes() {
    //     ss = document.getElementById('ss')
    //     btn1 = document.getElementById('btn1')
    //     btn2 = document.getElementById('btn2')
    //     btn3 = document.getElementById('btn3')
    //     btn3.classList.add("active-abas");
    //     btn2.classList.remove("active-abas")
    //     btn1.classList.remove("active-abas");
    //     ss.innerHTML = `
    //     <h5 style="padding-top: 1rem;">Ficha técnica</h5>
    //     <div class="colum-txt" style="padding-top: 1rem; padding-bottom: 1rem;">
    //         Título Original: ${infoMovie.MovieTitle} <br/>
    //         Duração: ${infoMovie.movieTime}<br/>
    //         Gênero: ${infoMovie.genre}<br/>
    //         Ano de lançamento:${infoMovie.release}<br/>
    //         País: ${infoMovie.country}<br/>

    //         Direção: ${infoMovie.direction} <br/>
    //         Produção: ${infoMovie.production} <br/>
    //         Roteiro: ${infoMovie.Script} <br/>
    //         Música composta por: ${infoMovie.composition} <br/>
    //         Elenco: ${infoMovie.cast} 
    //     </div>
    //     `
    // }
    // END:: FUNCTION CONTENT | BEGIN:: access_token
    const [access_token, setAccessToken] = useState("");
    useEffect(() => {
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => { if (access_token != "" && params.id) { getMovie(); } }, [params.id, access_token]);
    // END:: access_token
    useEffect(() => {
        if (movie) {
            let date = new Date(movie.expiration);
            let expiration_date = date.getMonth() + '/' + date.getFullYear();
            let datetime = new Date(`Thu, 01 Jan 1970 ${movie.duration}`);
            let hour = `${datetime.getHours()}h ${datetime.getMinutes() == 0 ? '' : datetime.getMinutes() + ' min'}`;
            let names_genres = movie.genres.map(item => item.name);
            setInfoMovie({
                'VimeoID': Number(movie.url),
                'tralierVimeo': Number(movie.trailers[0].url),
                'MovieTitle': movie.name,
                'indicatedAge': Number(movie.parental_rating),
                'movieTime': hour,
                'genre': names_genres.join(', '),
                'release': (new Date(movie.release)).getFullYear(),
                'country': movie.country,
                'direction': movie.direction,
                // 'approval': `${88}% gostaram desse filme`,
                'production': movie.production,
                'Script': movie.script,
                'composition': movie.composition,
                'cast': movie.cast,
                'linkPlay': `/reproduzir/${movie.id}`,
                'shortSynopsis': movie.description,
                'expirationDate': expiration_date,
                'sinopse': movie.sinopse
            });
        }
        GetMovieCategotry()
    }, [movie]);

    function getMovie() {
        api.get(`auth/getMovieById/${params.id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) setMovie(response.data.response);
                else message(response.data.response);
            });
    }
    function toggleFavorite() {
        if (movie) {
            const data = new FormData();

            data.append('movie_id', String(movie.id));

            api.post('auth/toggleFavorite', data, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                if (response.data.result) {
                    let temp = movie;
                    temp.users = [];
                    temp.users[0] = response.data.response;
                    setMovie({ ...temp });
                }
                else message(response.data.response);
            });
        }
    }
    // BEGIN:: POPUP
    function openPopUp(vimeo_id: string) {
        console.log(vimeo_id);
        let popUp: any;
        popUp = document.querySelector('.pop-up-video')
        popUp.style.display = "flex";
        setVimeoId(vimeo_id);
    }
    function closePopUp() {
        let popUp: any;
        popUp = document.querySelector('.pop-up-video')
        popUp.style.display = "none";
        setVimeoId(null);
    }
    // END:: POPUP
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    // Função do slid
    let slider: number;
    slider = 210;
    let x: number;


    const [scrolX, setScrolX] = useState(0)
    const Scroll = useRef<HTMLDivElement>(null);
    const leftSlider = () => {
        if (Scroll.current) {
            x = scrolX + slider;
            Scroll.current.scrollTo(x, 0);
            setScrolX(x)
        }
    }

    const rigthSlider = () => {
        if (Scroll.current) {
            if (scrolX > 0) {
                x = scrolX - slider;
                Scroll.current.scrollTo(x, 0);
                setScrolX(x)
            }
        }

    }

    async function GetMovieCategotry() {
        let filme = movie?.categories[0].id
        await api.get(`auth/getMoviesCatalogByCategoryId/${filme}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    const categorySimilar = response.data.response;
                    if(categorySimilar.length > 0){
                        setSimilarFilm(response.data.response[0])
                    }
                    // const smovie = categorySimilar.movies
                    // setSimilarFilm(smovie)
                }
                else message(response.data.response);
            })
            .catch(error => {
                console.log(error)
            })
    }
    return (
        <div id="contatiner-description">
            <Header />
            {(movie && infoMovie.VimeoID != 0) && (
                <BkgVimeo infoMovie={infoMovie} />
            )}
            <Link to="/inicio" className="btn-voltar" type="button"><i className="fas fa-arrow-left"></i>Voltar</Link>
            <div className="description__banner">
                <div className="goback-btn-description">
                    <Link className="btn-dv" to="/inicio"><i className="material-icons">chevron_left</i></Link>
                    <h1>{infoMovie['MovieTitle']}</h1>
                </div>
                <div>
                    <span style={{ border: '2px solid #ccc', padding: '.1rem', borderRadius: '4px' }}>{infoMovie['indicatedAge']}</span> <span>{infoMovie.release}</span> <span>{infoMovie.genre}</span> <span>| {infoMovie['movieTime']}</span>
                </div>
                {/* <div>
                    <i className="far fa-thumbs-up"></i> {infoMovie.approval}
                </div> */}
                <div className="shortSynopsis">
                    {infoMovie.shortSynopsis}
                </div>

                <div className="group-buttons">
                    {movie && (
                        <BtnWite name="Assista" route={`/reproduzir/${movie.id}`} />
                    )}


                    <div className="border-blue">
                        <button className="assinale-listing" onClick={toggleFavorite}>
                            <i className={`fas fa-${(movie && movie.users && movie.users.length > 0 && movie.users[0].favorite == true) ? 'check' : 'plus'}`}></i>
                          Minha lista
                        </button>
                    </div>
                    {/* <div className="border-blue">
                    <button   type="button" className="img-border-blue" >
                    <i style={{ fontSize: '80%', marginRight: '.2rem' }} ></i> 
                    </button>
                </div> */}
                </div>

                <div className="expirationDate">
                    Disponivel até: {infoMovie.expirationDate}
                </div>
            </div>
            <div className="efect"></div>
            <section className="nav-abas">
                <ul id="container-abas">
                    <li id="btn1" onClick={() => setTabActive('sinopse')}
                        className={`${tabActive == 'sinopse' ? 'active-abas' : ''}`}>SINOPSE</li>
                    <li id="btn2" onClick={() => setTabActive('trailer')}
                        className={`${tabActive == 'trailer' ? 'active-abas' : ''}`}>TRAILER & EXTRAS</li>
                    <li id="btn3" onClick={() => setTabActive('detalhes')}
                        className={`${tabActive == 'detalhes' ? 'active-abas' : ''}`}>DETALHES</li>
                </ul>
                <div className="nav-abas-container-content">
                    <div id="ss" className="nav-abas-content">
                        {tabActive == 'detalhes' && (
                            <>
                                <h5 style={{ paddingTop: '1rem' }}>Ficha técnica</h5>
                                <div className="colum-txt" style={{ paddingTop: '1rem', paddingBottom: '1rem' }}>
                                    Título Original: {infoMovie.MovieTitle} <br />
                                    Duração: {infoMovie.movieTime}<br />
                                    Gênero: {infoMovie.genre}<br />
                                    Ano de lançamento: {infoMovie.release}<br />
                                    País: {infoMovie.country}<br />

                                    Direção: {infoMovie.direction} <br />
                                    Produção: {infoMovie.production} <br />
                                    Roteiro: {infoMovie.Script} <br />
                                    Música composta por: {infoMovie.composition} <br />
                                    Elenco: {infoMovie.cast}
                                </div>
                            </>
                        )}
                        {tabActive == 'trailer' && (
                            <>
                                {/* <h5 style={{ paddingTop: '1rem' }}>Conteúdo TRAILER & EXTRAS</h5> */}
                                <div className="trailer-extra">
                                    {movie && movie.trailers.map(trailer => {
                                        return (
                                            <div className="content__trailer" onClick={() => openPopUp(trailer.url)} key={trailer.id}>
                                                <img src={trailer.thumbnail} alt="" />
                                                <p style={{}}>{trailer.name}</p>
                                            </div>
                                        )
                                    })}
                                </div>
                                <div style={{ paddingTop: '1rem', paddingBottom: '1rem', display: 'flex', gap: '.5rem' }}>
                                    {movie && movie.extras && movie.extras.map(extra => {
                                        return (
                                            <div onClick={() => openPopUp(extra.url)} key={extra.id} style={{
                                                backgroundImage: `url('${extra.thumbnail}')`,
                                                backgroundSize: 'cover',
                                                width: '310px',
                                                height: '174px',
                                                position: 'relative',
                                                cursor: 'pointer'
                                            }}>
                                                <p style={{
                                                    position: 'absolute',
                                                    bottom: 0,
                                                    left: 0,
                                                    marginBottom: 0,
                                                    paddingBottom: '8px',
                                                    paddingTop: '4px',
                                                    paddingLeft: '10px',
                                                    paddingRight: '10px',
                                                    width: 'calc(100% - 20px)',
                                                    background: 'rgba(0,0,0,.5);'
                                                }}>{extra.name}</p>
                                            </div>
                                        )
                                    })}
                                </div>
                            </>
                        )}
                        {tabActive == 'sinopse' && (
                            <>
                                {/* <h5 style={{ paddingTop: '1rem' }}>Conteúdo Sinopse</h5> */}
                                <div className="edit-txt-sinopese" style={{ paddingTop: '1rem', paddingBottom: '1rem' }}>
                                    {infoMovie.sinopse}
                                </div>
                            </>
                        )}
                    </div>

                </div>
            </section>
            <div className="pop-up-video">
                <div onClick={closePopUp} className="close-pop-up">X Clique aqui para sair.</div>
                <div className="pop-up-video__content">
                    {vimeoId != null && (
                        <iframe src={`https://player.vimeo.com/video/${vimeoId}`} width="640" height="360" allow="autoplay; fullscreen; picture-in-picture"></iframe>
                        // <Vimeo
                        //     video={vimeoId}
                        //     responsive={true}
                        //     loop={false}
                        //     muted={false}
                        //     autoplay={false}
                        // />
                    )}
                </div>
            </div>
            {/* filmes relacionados */}
            <div className="movie-relacionado">
            <h4 style={{ textTransform: 'uppercase', fontWeight: 'normal' }}>Filmes parecidos com {infoMovie['MovieTitle']}</h4>
                <div className="container-my-carousel">
                    <div ref={Scroll} className="my-carousel-sroll ">
                        <div className="my-carousel container-category__content">
                            {similarFilm && similarFilm.movies.map(movie =>{
                                let date = new Date(`Thu, 01 Jan 1970 ${movie.duration}`);
                                let hour = `${date.getHours()}h ${date.getMinutes() == 0 ? '' : date.getMinutes() + 'm'}`;
                                let firstDate = new Date(movie.release)
                                let favorite = (movie.users && movie.users.length > 0 && movie.users[0].favorite == true) ? true : false;
                                let like = (movie.users && movie.users.length > 0 && movie.users[0].rating == '1') ? true : false;
                                let deslike = (movie.users && movie.users.length > 0 && movie.users[0].rating == '-1') ? true : false;
                                return (
                                    <div key={movie.id} className="my-carousel-item">
                                    <div className="container-video-card">
                                        <div className="aling-video-card">
                                            <Vimeo
                                                video={movie.trailers.length > 0 ? movie.trailers[0].url : 0}
                                                // background={true}
                                                responsive={true}
                                                loop={true}
                                                muted={true}
                                                autoplay={false}
                                            />
                                        </div>
                                        <div className="card-info-n">
                                            <div className="control-card-movie">
                                                <Link to={`/reproduzir/${movie.id}`}>
                                                    <i className="fas fa-play"></i>
                                                </Link>
                                                <Link to={`/descricao-do-filme/${movie.id}`}>
                                                    <i className="fas fa-chevron-down"></i>
                                                </Link>
                                            </div>
                                            <div className="info_card_muvie__bootom_slik" >
                                                <div className="info_card_muvie__itens_slik">
                                                    {/* <div className="info_card_muvie__aproval">96% relevante</div>*/}
                                                    <h4 style={{ color: '#1C1E29', margin: 0, textTransform: 'uppercase' }}>{movie.name}</h4>
                                                    <div id="idicative-age" className="info_card_muvie__idicative-class_slik">
                                                        {movie.parental_rating}
                                                    </div>
                                                    <div className="info_card_muvie_ano-lancamento_slik">
                                                        {firstDate.getFullYear()}
                                                    </div>
                                                    {movie.genres.map((genre => {
                                                        return (
                                                            <div className="genre-card_slik" key={genre.id}>{genre.name},</div>
                                                        );
                                                    }))}
                                                    <div className="minutes-card_slik">| {hour}</div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <img src={movie.thumbnail} alt="" />
                                </div>
                            );})}
                        </div>
                    </div>
                   
                   <div onClick={rigthSlider} className="btn-my-carrosel-left">
                        <button><i className="material-icons">chevron_left</i></button>
                    </div>

                    <div onClick={leftSlider} className="btn-my-carrosel-rigth">
                        <button><i className="material-icons">navigate_next</i></button>
                    </div>
                </div>

            </div>
            <Footer />
        </div>
    );

}
export default Description;


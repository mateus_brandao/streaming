import React, { useContext } from 'react';
import Header from '../../components/Dash/Header';
import updateAccount from '../../components/Dash/Dash-acont';
import Acont from '../../components/Dash/Dash-acont';
import { Link } from 'react-router-dom';
import { GeneralContext } from '../../contexts/GeneralContext';
function ReturnNavigation() {
    const {user, setLoadOpen, openLoad, loadPage} = useContext (GeneralContext);
    return (
        <div id="container-return-navigation">
            <Header />
            <div className="header__user-edit">
                <div className="user-edit__salutation">
                    <p>
                        <span>Conta</span><br />
                        Estes são os dados da sua conta, <br />
                        edite quando quiser.
                    </p>
                </div>
                <Link to="/painel-de-controle" className="user-edit__btn-goback">
                    Voltar para o painel
                </Link>
            </div>

            <div className="return-nav">
                <Acont />
            </div>

        </div>
    );
}
export default ReturnNavigation;
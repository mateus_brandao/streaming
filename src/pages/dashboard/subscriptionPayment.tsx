import React, { useContext } from 'react';
import Header from '../../components/Dash/Header';
import { Link } from 'react-router-dom';
import { GeneralContext } from '../../contexts/GeneralContext';

function SubscriptionPayment() {
    const {user, setLoadOpen, openLoad, loadPage} = useContext (GeneralContext);
    let currentPlan = ['Básico', 'Padrão', 'Premium']
    let formPayment = ['Crédito', 'Débido', 'Boleto']
    let monthlyValue = ['R$ 9,90', 'R$ 32,90', 'R$ 45,90']
    
    return (
        <div id="subscription-payment">
            <Header />
            <div className="header__user-edit">
                <div className="user-edit__salutation">
                    <p>
                        <span>{user.first_name}</span><br />
                        Esses são os dados de pagamento da sua conta, <br />
                        edite quando quiser.
                    </p>
                </div>
               {/* <Link to="/painel-de-controle" className="user-edit__btn-goback">
                    Voltar para o painel
                </Link>*/}
            </div>
            <div className="payment-information">
                <div className="payment-information__current-plan">
                    <div>
                        <span><strong>Plano atual:</strong> {currentPlan[0]}</span>
                    </div>
                    <div>
                        <span><strong>Valor mensal:</strong> {monthlyValue[0]}</span>
                    </div>
                    <div>
                        <span><strong>Forma de pagamento:</strong> {formPayment[0]}</span>
                    </div>
                    <div className="group-btn__change-plam ">
                        <Link to="/alterar-plano" className="change_plan">
                            Alterar plano
                        </Link>
                        <Link to="/painel-de-controle" className="goback">
                            voltar
                        </Link>
                    </div>
                   
                </div>
            </div>
        </div>

    );
}

export default SubscriptionPayment;
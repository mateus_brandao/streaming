import React, { useEffect } from "react";
import { Link } from 'react-router-dom';
import Footer from '../../components/Footer';
import DashN from '../../components/Dash/Header/index'
import DashInit from '../../components/Dash/Dash-init/index';
import Acont from '../../components/Dash/Dash-acont';



import ContentAddMovie from '../../components/Dashboard/dashboard-section/section-add-movie/index';

function Dashboard() {
   
    return (
        <div id="container-dashboard">
           {/* <Header /> */}
           <DashN />
           <DashInit />
           <Footer />
          
          
        </div>
    );
}
export default Dashboard;

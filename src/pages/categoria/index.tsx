import React, { useEffect, useRef, useState } from 'react';
import Header from '../../components/Header';
import ButonBlue from '../../components/Buttons/Button-border-blue';
import { Link, useParams } from 'react-router-dom';
import Footer from '../../components/Footer';
import api from '../../services/api';
import { Category, Genre, Trailer, Extra } from '../../types/typesdef';
import defaltBanner from '../../assets/imagens/magnificatbase-02.jpg';
import Vimeo from '@u-wave/react-vimeo';


const CategoryInfo = {
    title: 'Nome da coleção',
    discription: `Breve descrição objetiva sobre a coleção, com histórico 
    e apresentando o homenageado ou a ação, com
    informações interessantes para o assinante.`,
    sobre: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    `
}
interface DescriptionParams {
    id?: string;
}
interface UserMovie {
    id: number;
    user_id: number;
    movie_id: number;
    current_time: string;
    viewed: boolean;
    favorite: boolean;
    rating: string;
    movie?: Movie;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    categories: Category[];
    genres: Genre[];
    url: string;
    trailers: Trailer[];
    extras?: Extra[];
    users?: UserMovie[];
    duration: string;
    cost: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface SimilarMovie {
    id: number;
    name: string;
    movies: Movie[];
}

const Categoria = () => {
    const params = useParams<DescriptionParams>();
    const [activeAba, setActiveAba] = useState(false)
    const [similarFilm, setSimilarFilm] = useState<SimilarMovie>();
    const [currentCategory, setCurrentCategory ] = useState<Category>();
    let active: string;
    let active2: string;
    let visible: string;
    let visible2: string;
    const [access_token, setAccessToken] = useState("");
    useEffect(() => {
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => { if (access_token != "" && params.id) { GetMovieCategotry() } }, [params.id, access_token]);

    async function GetMovieCategotry(){
        let id = params.id
        await api.get(`auth/getMoviesCatalogByCategoryId/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    const categorySimilar = response.data.response;
                    if(categorySimilar.length > 0){
                        setSimilarFilm(response.data.response[0])
                        setCurrentCategory(response.data.response[0])
                    }
                    // const smovie = categorySimilar.movies
                    // setSimilarFilm(smovie)
                }
                // else message(response.data.response);
            })
            .catch(error => {
                console.log(error)
            })
    }

    if (activeAba == false) {
        active = 'active-category';
        active2 = ' ';
        visible = 'block'
        visible2 = 'none'

       
    } else {
        active = ' '
        active2 = 'active-category'
        visible = 'none'
        visible2 = 'block'
    }

    let slider: number;
    slider = 210;
    let x: number;


    const [scrolX, setScrolX] = useState(0)
    const Scroll = useRef<HTMLDivElement>(null);
    const leftSlider = () => {

        if (Scroll.current) {
            x = scrolX + slider;
            Scroll.current.scrollTo(x, 0);
            setScrolX(x)
        }
    }

    const rigthSlider = () => {
        if (Scroll.current) {
            if (scrolX > 0) {
                x = scrolX - slider;
                Scroll.current.scrollTo(x, 0);
                setScrolX(x)
            }
        }

    }

    return (
        <>
            <Header />
            <div className="container-category-page">
                <div className="banner__category">
                    <div className="category-info">
                        <span>Coleção</span>
                        <div className="title-category">
                            <Link to="/inicio" className="category__get-out">
                                <i className="material-icons">chevron_left</i>
                            </Link>
                            <div className="separation">
                            </div>
                            <h1>{currentCategory && currentCategory.name}</h1>
                        </div>
                        <div className="category__description">
                            <p>{CategoryInfo.discription}</p>
                        </div>
                    </div>
                    <img src={defaltBanner} alt="" />
                    <div className="efect"></div>
                </div>
                <div className="category__main">
                    <nav>
                        <ul>
                            <li className={active} onClick={() => setActiveAba(false)} >Titulos</li>
                            <li className={active2} onClick={() => setActiveAba(true)} >Sobre {CategoryInfo.title}</li>
                        </ul>
                        <div>

                        </div>
                    </nav>
                </div>


                <div className="content-category-abas">
                    <div 
                    style={{ display: visible}}
                    
                    >
                        <div className="container-my-carousel">
                        <div ref={Scroll} className="my-carousel-sroll ">
                        <div className="my-carousel container-category__content">
                            {similarFilm && similarFilm.movies.map(movie =>{
                                let date = new Date(`Thu, 01 Jan 1970 ${movie.duration}`);
                                let hour = `${date.getHours()}h ${date.getMinutes() == 0 ? '' : date.getMinutes() + 'm'}`;
                                let firstDate = new Date(movie.release)
                                let favorite = (movie.users && movie.users.length > 0 && movie.users[0].favorite == true) ? true : false;
                                let like = (movie.users && movie.users.length > 0 && movie.users[0].rating == '1') ? true : false;
                                let deslike = (movie.users && movie.users.length > 0 && movie.users[0].rating == '-1') ? true : false;
                                return (
                                    <div key={movie.id} className="my-carousel-item">
                                    <div className="container-video-card">
                                        <div className="aling-video-card">
                                            <Vimeo
                                                video={movie.trailers.length > 0 ? movie.trailers[0].url : 0}
                                                // background={true}
                                                responsive={true}
                                                loop={true}
                                                muted={true}
                                                autoplay={false}
                                            />
                                        </div>
                                        <div className="card-info-n">
                                            <div className="control-card-movie">
                                                <Link to={`/reproduzir/${movie.id}`}>
                                                    <i className="fas fa-play"></i>
                                                </Link>
                                                <Link to={`/descricao-do-filme/${movie.id}`}>
                                                    <i className="fas fa-chevron-down"></i>
                                                </Link>
                                            </div>
                                            <div className="info_card_muvie__bootom_slik" >
                                                <div className="info_card_muvie__itens_slik">
                                                    {/* <div className="info_card_muvie__aproval">96% relevante</div>*/}
                                                    <h4 style={{ color: '#1C1E29', margin: 0, textTransform: 'uppercase' }}>{movie.name}</h4>
                                                    <div id="idicative-age" className="info_card_muvie__idicative-class_slik">
                                                        {movie.parental_rating}
                                                    </div>
                                                    <div className="info_card_muvie_ano-lancamento_slik">
                                                        {firstDate.getFullYear()}
                                                    </div>
                                                    {movie.genres.map((genre => {
                                                        return (
                                                            <div className="genre-card_slik" key={genre.id}>{genre.name},</div>
                                                        );
                                                    }))}
                                                    <div className="minutes-card_slik">| {hour}</div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <img src={movie.thumbnail} alt="" />
                                </div>
                            );})}
                        </div>
                    </div>
                   
                            <div onClick={rigthSlider} className="btn-my-carrosel-left">
                                <button><i className="material-icons">chevron_left</i></button>
                            </div>

                            <div onClick={leftSlider} className="btn-my-carrosel-rigth">
                                <button><i className="material-icons">navigate_next</i></button>
                            </div>
                        </div>
                    
                    
                    </div>
                    <div 
                    className="content-aba-txt"
                    style={{ display: visible2}}
                    >
                        <p>{CategoryInfo.sobre}</p>
                        <div className="container-btn-category">

                        <ButonBlue name="leia mais" />
                        </div>
                    </div>
                </div>
                {/* <div className="indication-movie">
                        <span >VOCÊ PODERÁ GOSTAR</span>
                        
                        <div className="container-my-carousel">
                            <div ref={Scroll} className="my-carousel-sroll" >
                                <div className="my-carousel">

                                    <div key="" className="my-carousel-item in-production">
                                    </div>
                                    <div key="" className="my-carousel-item in-production">
                                    </div>
                                    <div key="" className="my-carousel-item in-production">
                                    </div>
                                    <div key="" className="my-carousel-item in-production">
                                    </div>
                                    <div key="" className="my-carousel-item in-production">
                                    </div>
                                    <div key="" className="my-carousel-item in-production">
                                    </div>
                                    <div key="" className="my-carousel-item in-production">
                                    </div>

                                </div>
                                <div className="fim-carrosel">
                                </div>
                            </div>
                            <div onClick={rigthSlider} className="btn-my-carrosel-left">
                                <button><i className="material-icons">chevron_left</i></button>
                            </div>

                            <div onClick={leftSlider} className="btn-my-carrosel-rigth">
                                <button><i className="material-icons">navigate_next</i></button>
                            </div>
                        </div>
                    
                    </div>
                 */}
            </div>
                <Footer />
        </>
    );
}

export default Categoria;
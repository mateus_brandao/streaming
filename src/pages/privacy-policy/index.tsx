import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/Dash/Header'
import Footer from '../../components/Footer'

const PrivacyPolicy = () => {
    return (
        <>
            <div className="container_privacy-poticy">
                <Header />
                <div className="content_privacy">
                    <h2>Regulamento Geral de Proteção de Dados </h2>

                    <p>
                        1. O que é o Regulamento Geral de Proteção de Dados.<br />
                    Em agosto de 2020, entrou em vigor a Lei Geral de Proteção de Dados (LGPD), que define uma série de regras sobre a coleta, armazenamento, tratamento e compartilhamento de dados pessoais por empresas e organizações em todo o país.
                    </p><br />
                    <p>
                        2. O direito de acesso às suas informações pessoais: <br />
                    Muitas das informações que armazenamos sobre nossos assinantes são facilmente acessíveis (,) entrando na conta por um navegador e clicando na opção Conta (Account).
                    </p><br />
                    <p>
                        3. Existem determinadas circunstâncias nas quais você tem o direito de exigir que suas informações pessoais sejam excluídas (“apagadas”) ou removidas dos sistemas da Magnificat. É possível encontrar mais informações sobre como excluir e remover informações em nosso FAQ.
                    </p><br />
                    <p>
                        3. Você tem o direito de reclamar com qualquer autoridade de proteção de dados (,) sobre a coleta e o uso de suas informações pessoais pela Magnificat.Video. Para fazer solicitações, ou caso você tenha outras dúvidas sobre nossas práticas de privacidade, entre em contato pelo e-mail  suporte@magnificat.video Responderemos a todas as solicitações recebidas de indivíduos que desejam exercer seus direitos à proteção de dados em conformidade com as leis de proteção de dados aplicáveis.
                    </p><br />
                    <p>
                        2. Abaixo as perguntas mais frequentes sobre nossa Política de Privacidade: Quais são os dados pessoais coletados pela  Magnificat.Video?
                    </p><br />
                    <p>
                        - Informações da conta - Informações do proprietário da conta que você tenha enviado à Magnificat.Vídeo, como e-mail e número de telefone (disponíveis em Assinatura e pagamento), assim como as informações de escolha de plano.

                    </p><br />
                    <p>
                        - Configurações de comunicação – As comunicações da Magnificat.Video que você gostaria de receber (disponíveis em Configurações).
                    </p><br />
                    <p>
                        - Informações de pagamento e cobrança – Informações de pagamento que você forneceu à Magnificat.Video e informações sobre as cobranças que fizemos ou tentamos fazer na sua forma de pagamento pela sua assinatura (disponíveis em Assinatura e cobrança).
                    </p><br />
                    <p>
                        - Perfis – Detalhes sobre quaisquer perfis que tenham sido criados na sua conta Magnificat.Video, inclusive as preferências de reprodução (disponíveis em Conta).
                    </p><br />
                    <p>
                        - Histórico de interação com conteúdo – Um histórico do que foi assistido e informações sobre suas interações com títulos de conteúdos da Magnificat.Video, como filmes e séries que você classificou (disponível em Conta).
                    </p><br />
                    <p>
                        - Informações de endereço IP – Informações associadas à última vez que um aparelho específico foi usado para acessar o serviço com sua conta Magnificat.Video a partir de um IP em particular (disponível em Configurações).
                    </p><br />
                    <p>
                        Na página Conta, é possível editar as seguintes informações pessoais da sua conta Magnificat.Video:
                    </p><br />
                    <ul className="lista-politica-privacidade">
                        <li>e-mail</li>
                        <li>forma de pagamento</li>
                        <li>número de telefone</li>
                    </ul> <br />
                    <p>
                        Também é possível atualizar a senha, ajustar as configurações de comunicação e ocultar títulos exibidos na seção “O que foi assistido”. Além disso, você pode entrar em contato com o atendimento ao cliente e obter ajuda para alterar as seguintes informações:
                    </p> <br /> <br />
                    <ul className="lista-politica-privacidade">
                        <li>nome</li>
                        <li>email</li>
                        <li>forma de pagamento</li>
                        <li>senha</li>
                    </ul> <br />
                    <p>
                        Ao entrar em contato com o atendimento ao cliente, você deverá passar por um processo de verificação antes que nossos representantes possam ajudar a fazer modificações na conta.
                    </p><br />
                    <p>
                        Além disso, a Magnificat.Video  nunca solicitará que você revele sua senha. Se receber qualquer mensagem nesse sentido, por favor, informe à Magnificat.Video e não a revele a terceiros em nenhuma hipótese. Se, por qualquer motivo, tiver fornecido sua senha a terceiros, pedimos que a altere imediatamente. Da mesma maneira, pedimos que altere sua senha caso suspeite que terceiros tenham acesso a ela ou note movimentações estranhas relacionadas ao seu uso dos serviços, produtos ou plataformas Magnificat.Video.
                    </p><br />
                    <p>
                        Certas informações também podem ser coletadas quando você entra em contato com a Magnificat.Video, seja através do Serviço de Atendimento ao Consumidor (SAC) ou pelo nosso e-mail suporte@magnificat.video
                    </p><br />
                    <br />
                    <p>
                        A Magnificat.Video também acompanha as evoluções na legislação e nas regulamentações sobre privacidade de dados, para garantir que você possa exercer todos os direitos sobre seus dados pessoais.
                    </p><br />
                    <strong>Descrição Geral através do uso de Cookies </strong>.<br />
                    <br />
                    <strong>1. Uso de Cookies</strong>.<br />
                    <p>Os cookies são usados para observar hábitos de visita.</p>
                    <p>
                        Necessários para o funcionamento do site. Eles permitem que você navegue em nossos sites e use os serviços e recursos (por exemplo, cookies de segurança para autenticar usuários, evitar a utilização fraudulenta de credenciais de login e proteger os dados do usuário de terceiros não autorizados).
                    </p><br />
                    <p>O que são cookies e qual sua utilidade</p>
                    <p>
                        Cookies são pequenos arquivos de texto enviados e armazenados no seu computador. Estes pequenos arquivos servem para reconhecer, acompanhar e armazenar a sua navegação como usuário na Internet. Possibilitará à Magnificat oferecer um serviço mais personalizado, inclusive, a oferta de conteúdo e publicidade específico para cada pessoa.
                    </p><br />
                    <p>3. Em geral, os cookies são utilizados para:</p>
                    <ul className="lista-politica-privacidade">
                        <li>Proporcionar serviços diferenciados, lembrando quem você é e quais são os seus hábitos de navegação;</li>
                        <li>companhar o andamento de promoções. Quando uma promoção organizada pela Magnificat usa cookies, as informações gravadas no cookie indicam a pontuação do usuário;</li>
                        <li>Medir certos padrões de navegação, mapeando quais áreas dos portais dos Serviços você visitou e seus hábitos de visita como um todo. Usamos essa informação para verificar a rotina de navegação dos nossos usuários, e assim oferecer conteúdo e/ou serviços cada vez mais personalizados</li>
                        <li>Facilitar e agilizar o preenchimento de formulários. As informações contidas nos cookies de cada usuário podem ser utilizadas para preencher previamente os formulários de coleta de dados existentes na Internet.</li>


                    </ul><br />
                    <p>4. Como alterar ou bloquear cookies</p><br />
                    <p>A maioria dos navegadores é configurada para aceitar automaticamente os cookies. Você pode, contudo, alterar as configurações para bloquear cookies ou alertá-lo quando um cookie estiver sendo enviado ao seu dispositivo. Existem várias formas de gerenciar cookies, sendo possível criar um bloqueio geral para cookies, bloquear cookies de um site específico e até mesmo bloquear cookies de terceiros em relação a um site. Bloquear todos os cookies vai afetar o funcionamento da sua experiência, pois não será possível identificar suas preferências e recomendar conteúdo e publicidade relevantes.</p>
                    <p>Consulte as instruções do seu navegador para saber mais sobre como ajustar ou alterar suas configurações, lembrando que a configuração desejada deve ser replicada em todos os dispositivos utilizados para acessar os Serviços (como computadores, smartphones, tablets). Se desejar, consulte aqui os links para alterar a configuração de cookies nos principais navegadores.</p>

                    <h2>Termos e Condições de uso da Magnificat </h2>
                    <p>
                        Bem-vindo ao nosso Termo de Uso da  Magnificat.Video.
                        Nossa empresa fornece um serviço personalizado de assinatura que permite aos nossos assinantes acessar filmes, séries, programas de televisão, podcast, shows e outros conteúdos de vídeo (“Conteúdo Digital”) transmitidos pela Internet para determinados televisores, computadores e outros aparelhos conectados à Internet.
                    </p><br />
                    <p>
                        Este Termo de uso regula a sua utilização do serviço da  Magnificat.Video.  As Regras de Uso oferecem informações importantes, dos Serviços da  Magnificat.Video”, “nosso serviço” ou “o serviço” significam o serviço personalizado fornecido pela da  Magnificat Video, para busca e visualização do conteúdo da Magnificat.Video, incluindo todos os recursos e funcionalidades, recomendações e avaliações, do site e as interfaces do usuário, assim como todo o conteúdo e software associados ao serviço.
                    </p><br />
                    <p>
                        Quando nos referimos a “você”, “seu” ou “usuário” nos referimos a você, a pessoa que acessa ou utiliza o nosso serviço.
                        Ao utilizar o nosso serviço, você concorda em aceitar e estar vinculado a estes Termos e quaisquer Termos Adicionais aplicáveis, e confirmar que leu e entendeu. Você também reconhece, concorda e consente com nossas práticas de dados, conforme descrito em nossa Política de Privacidade LGPD.
                    </p><br />
                    <h4>1. Nosso Serviço</h4>
                    <p>
                        1. A plataforma oferece, a qualquer usuário que tenha aceitado esse Termo de Uso, um serviço de transmissão de vídeo sob demanda de obras audiovisuais para as quais nós detemos os direitos de exploração digital (“Conteúdo”).
                    </p><br />
                    <p>
                        2. Estes Termos afetam seus direitos, responsabilidades e obrigações legais e regem seu uso do nosso conteúdo. Se você não desejar estar vinculado a estes Termos e a quaisquer Termos Adicionais, não utilize nosso Conteúdo.
                    </p><br />
                    <h4>2. Assinatura</h4><br />
                    <p>
                        2. Sua assinatura Magnificat.Video permanecerá vigente até que seja cancelada. Para utilizar o serviço Magnificat.Video, você precisa ter acesso à Internet e um aparelho compatível com o nosso conteúdo, bem como fornecer uma ou mais Formas de Pagamento.
                    </p><br />
                    <p>
                        2. “Forma de Pagamento” refere-se a uma forma de pagamento utilizada, válida e aceitável, que poderá incluir o pagamento por meio da sua conta com terceiros. A menos que cancele a assinatura antes da data de cobrança, você nos autoriza a cobrar a taxa de assinatura do próximo ciclo de faturamento usando a sua Forma de Pagamento cadastrada por você.
                    </p><br />
                    <p>
                        3. Poderemos oferecer vários planos de assinatura, inclusive planos promocionais ou assinaturas disponibilizadas por terceiros em combinação com a oferta de seus próprios produtos e serviços. A Magnificat.Video não se responsabiliza pelos produtos e serviços fornecidos por tais terceiros. Alguns planos de assinatura podem possuir condições e limitações diferentes, que serão reveladas no momento de sua inscrição ou em outras comunicações disponibilizadas a você. Você pode encontrar detalhes específicos a respeito da sua assinatura da Magnificat.Video  acessando a nossa plataforma no campo “Conta”, no seu perfil na parte superior de todas as páginas da plataforma da Magnificat.Video.

                    </p><br />
                    <p>
                        4. Os serviços por assinatura, a extensão do Conteúdo Digital por Assinatura disponível, e os títulos específicos disponíveis de serviços por assinatura, podem mudar ao longo do tempo e conforme o local sem aviso prévio (exceto conforme venha a ser exigido pela lei aplicável). A menos que indicado de outra forma, as mudanças de preço surtem efeitos a contar do início do próximo período de assinatura. Caso você não concorde com uma mudança de assinatura, você pode cancelar sua assinatura conforme a Seção 4(4.3) abaixo. O preço de assinaturas ou filiações pode incluir IVA e/ou outros tributos. Quando aplicável tais tributos serão recolhidos pela parte com a qual você contratar o serviço, a qual pode ser a Magnificat.Video ou um terceiro.
                    </p><br />
                    <h4>3. Ofertas promocionais </h4><br />
                    <p>
                        1. A Magnificat.Video. poderá disponibilizar ofertas, planos ou assinaturas promocionais especiais (“Ofertas”). A elegibilidade para as Ofertas é determinada pela Magnificat.Video a seu exclusivo critério. A Magnificat.Video se reserva o direito de revogar uma Oferta e suspender sua conta se determinarmos que você não é elegível. Moradores de uma mesma residência com uma assinatura Magnificat.Video atual ou recente podem não ser elegíveis para Ofertas introdutórias específicas. A Magnificat.Video poderá usar informações como identificação do aparelho, forma de pagamento ou e-mail da conta usados em uma assinatura Magnificat.Video atual ou recente para determinar a elegibilidade. Os requisitos de elegibilidade, bem como outras limitações e condições, serão revelados quando você se inscrever para a Oferta ou em outras comunicações que disponibilizamos para você.
                    </p><br />
                    <p>
                        2. A Magnificat.Video cobrará, de acordo com a sua Forma de Pagamento escolhida, o preço da assinatura referente ao próximo ciclo de faturamento ao final do período de utilização (gratuita), a não ser que você cancele sua assinatura antes do final do referido período. Para consultar o preço da assinatura e a data de término do seu período de utilização gratuita, acesse nossa plataforma Magnificat.Video na página “Conta”.

                    </p><br />
                    <h4>4. Faturamento e Cancelamento</h4><br />
                    <p>
                        1. Ciclo de faturamento: O preço da assinatura do serviço Magnificat.Video  e todos os outros encargos incorridos por você em relação ao uso do serviço, tais como impostos e possíveis taxas de transação, serão cobrados, de acordo com a Forma de Pagamento escolhida, na data de pagamento específica indicada na página “Conta”. A duração do ciclo de faturamento dependerá do tipo de assinatura escolhido quando você assinou o serviço. Em alguns casos, a data de pagamento poderá ser alterada, por exemplo, se a Forma de Pagamento não puder ser cobrada, se você alterar o plano de assinatura ou se a sua assinatura paga começar em um dia que não existe em um determinado mês. A Magnificat.Video poderá solicitar antecipadamente uma autorização do valor a ser cobrado de sua assinatura ou cobranças relacionadas ao serviço utilizado,  de acordo com a Forma de Pagamento. Inclusive a autorização de até aproximadamente um mês de serviço, no ato de sua inscrição. Em certos casos seu saldo ou limite de crédito poderá ser reduzido para refletir a autorização durante seu período de utilização gratuita.
                    </p><br />
                    <p>
                        2. Formas de Pagamento: Para utilizar o serviço Magnificat.Video você precisará fornecer uma ou mais Formas de Pagamento. Você nos autoriza a cobrar de qualquer forma associada à sua conta, caso a sua Forma de Pagamento principal seja negada ou não esteja mais disponível para nós para efetuarmos a cobrança do valor da assinatura e você concorda que as Formas de Pagamento disponíveis tanto para operações de débito como de crédito poderão ser processadas de ambas as formas.
                    </p><br />
                    <p>
                        3. Você continuará responsável por todos os valores não pagos. Se um pagamento não for liquidado com sucesso, devido à perda de validade, insuficiência de saldo ou qualquer outro motivo, e você não cancelar a sua conta, poderemos suspender o seu acesso ao serviço até que consigamos fazer a cobrança de uma Forma de Pagamento válida. Para algumas Formas de Pagamento, às instituições financeiras e/ou os prestadores de serviço de meios de pagamento poderão cobrar determinadas taxas, como taxas sobre transações financeiras internacionais ou outras taxas relacionadas à Forma de Pagamento. A cobrança de impostos locais pode variar de acordo com a Forma de Pagamento usada. Consulte a instituição financeira da sua Forma de Pagamento para obter mais detalhes.
                    </p><br />
                    <p>
                        4. Cancelamento: Você poderá cancelar sua assinatura Magnificat.Video  a qualquer momento e você continuará a ter acesso ao serviço Magnificat.Video  até o fim do período de faturamento. Na extensão permitida pelas leis aplicáveis, os pagamentos não são reembolsáveis e a Magnificat.Video  não oferece reembolsos ou créditos por períodos de assinatura utilizados parcialmente ou por conteúdo Magnificat.Video  não assistido. Para cancelar, acesse a página “Conta” e siga as instruções de cancelamento. Se você cancelar sua assinatura, sua conta será automaticamente encerrada ao fim do período de cobrança em andamento. Se você informou à Magnificat.Video indicando como Forma de Pagamento uma conta junto a terceiros e deseja cancelar sua assinatura a Magnificat.Video  talvez seja preciso efetuar o cancelamento juntamente a tais terceiros (por exemplo, acessando sua conta com esse terceiro e desativando a renovação automática ou cancelando a assinatura do serviço por meio desse terceiro).
                    </p><br />
                    <p>
                        5. Alteração de preços e planos de assinatura:. De tempos em tempos, poderemos alterar nossos planos de assinatura e os preços de nosso serviço. Os valores de assinatura poderão ser revisados periodicamente para melhor adequação ao contínuo aprimoramento de nosso serviço. Nós também poderemos ajustar o valor de sua assinatura anualmente, ou com maior frequência, conforme permitido pela legislação vigente, de acordo com a inflação estabelecida pelo Índice Geral de Preços do Mercado (IGP-M), publicado pela Fundação Getúlio Vargas, ou outro índice equivalente aplicável ao serviço Magnificat.Video. Quaisquer alterações de preço ou alterações nos seus planos de assinatura serão aplicadas nos ciclos de faturamento subsequentes ao envio da notificação para você.
                    </p><br />
                    <h4> 5. Serviço Magnificat.Video</h4>
                    <p>
                        1. Caso você tenha menos de 18 anos de idade, ou a idade de maioridade em seu local, você pode usar o Serviço apenas com o envolvimento de seu pai/mãe ou responsável.

                    </p><br />
                    <p>
                        2. O serviço Magnificat.Video e todo o conteúdo visualizado por intermédio, destinam-se exclusivamente para uso pessoal e não comercial, não podendo ser compartilhados com pessoas de fora da sua família. Durante sua assinatura a Magnificat.Video, concede a você um direito limitado, não exclusivo e intransferível para acessar o serviço Magnificat.Video e assistir ao conteúdo da Magnificat.Video. Exceto pelo descrito acima, nenhum outro direito, titularidade ou participação lhe é concedido. Você concorda em não utilizar o serviço em exibições públicas.
                    </p><br />
                    <p>
                        3. Você pode assistir ao conteúdo da Magnificat.Video, primordialmente no país onde fez sua inscrição e somente nas áreas geográficas onde oferecermos nosso serviço e tivermos licenciado esse conteúdo. O conteúdo que pode estar disponível irá variar segundo a sua localização geográfica e será alterado periodicamente.
                    </p><br />
                    <p>
                        4. O serviço Magnificat.Video, inclusive o catálogo de conteúdo, é atualizado regularmente. Além disso, diferentes aspectos do serviço são testados de forma contínua, incluindo o site, as interfaces de usuário, os recursos promocionais e a disponibilidade do conteúdo da Magnificat.Video.
                    </p><br />
                    <p>
                        5. Você concorda em usar o serviço Magnificat.Video, incluindo todos os recursos e funcionalidades associadas de acordo com as leis, regras e regulamentos aplicáveis ou outras restrições de uso do serviço ou conteúdo previstas nas mesmas. Você também concorda em não arquivar, reproduzir, distribuir, modificar, exibir, executar, publicar, licenciar ou criar trabalhos derivados, colocar à venda ou utilizar (exceto nas formas expressamente autorizadas por estes Termos de uso) o conteúdo e as informações contidas ou obtidas do serviço Magnificat.Video ou por meio deste. Você também concorda em não contornar, remover, alterar, desativar, degradar ou adulterar quaisquer das proteções de conteúdo do serviço Magnificat.Video, usar qualquer robô, spider, scraper ou outros meios automatizados para acessar o serviço Magnificat.Video, descompilar, executar engenharia reversa ou desmembrar qualquer software ou outros produtos ou processos acessíveis pelo serviço Magnificat.Video inserir qualquer código ou produto ou manipular o conteúdo do serviço Magnificat.Video de qualquer forma ou usar métodos de data mining, coleta de dados ou extração de dados. Além disso, você concorda em não fazer upload, publicar, enviar por e-mail, comunicar ou transmitir de qualquer forma qualquer material designado para interromper, destruir ou limitar a funcionalidade de qualquer software, hardware ou equipamento de telecomunicações associado ao serviço Magnificat.Video, incluindo vírus de software, código, arquivos ou programas. A Magnificat.Video poderá cancelar ou restringir seu uso do serviço se você violar estes Termos de uso ou se envolver no uso ilegal ou fraudulento do serviço.
                    </p><br />
                    <p>
                        6. A qualidade da imagem do conteúdo da Magnificat.Video pode variar de aparelho para aparelho e pode ser afetada por diversos fatores, incluindo sua localização, a largura de banda disponível e a velocidade da sua conexão com a Internet. A disponibilidade de alta definição (HD), ultra-alta definição (Ultra HD) e High Dynamic Range (HDR) está sujeita aos seus serviços de Internet e recursos do aparelho usado. NEM TODO O CONTEÚDO ESTÁ DISPONÍVEL EM TODOS OS FORMATOS, COMO HD, ULTRA HD OU HDR, E NEM TODOS OS PLANOS DE ASSINATURA PERMITEM O RECEBIMENTO DE CONTEÚDO EM TODOS OS FORMATOS. AS CONFIGURAÇÕES PADRÃO DE REPRODUÇÃO EM REDES DE TELEFONIA CELULAR EXCLUEM CONTEÚDO HD, ULTRA HD E HDR. A VELOCIDADE DE CONEXÃO MÍNIMA PARA QUALIDADE DE DEFINIÇÃO PADRÃO (SD) É 1,0 MBPS. NO ENTANTO, RECOMENDAMOS UMA CONEXÃO MAIS RÁPIDA PARA OBTER UMA MELHOR QUALIDADE DE VÍDEO. RECOMENDAMOS UMA VELOCIDADE MÍNIMA DE DOWNLOAD DE 3,0 MBPS POR TRANSMISSÃO PARA RECEBER CONTEÚDO EM HD (DEFINIDO COMO RESOLUÇÃO 720P OU SUPERIOR). RECOMENDAMOS UMA VELOCIDADE MÍNIMA DE DOWNLOAD DE 15,0 MBPS POR TRANSMISSÃO PARA RECEBER ULTRA HD (DEFINIDO COMO RESOLUÇÃO 4K OU SUPERIOR). Você se responsabiliza por todas as tarifas de acesso à Internet. Por favor, consulte seu provedor de Internet para obter informações sobre os custos de utilização de dados. O tempo necessário para começar a assistir ao conteúdo da Magnificat.Video poderá variar de acordo com uma série de fatores, incluindo a sua localização, a largura de banda disponível no momento, o conteúdo que você tenha selecionado e as configurações do aparelho compatível com a Magnificat.Video utilizado.
                    </p><br />
                    <p>
                        7. O software da Magnificat.Video foi desenvolvido pela ou para a Magnificat.Video e foi projetado para permitir a  visualização do conteúdo da Magnificat.Video em aparelhos compatíveis com a Magnificat.Video. Esse software poderá variar conforme o aparelho/mídia. A funcionalidade e os recursos também poderão variar segundo o aparelho utilizado. Você concorda que o uso do serviço poderá exigir softwares de terceiros, sujeito a licenças de terceiros. Você concorda que poderá receber automaticamente versões atualizadas do software da Magnificat.Video e de software relacionado a terceiros.
                    </p><br />
                    <h4>6. Uso do Nosso Serviço:</h4><br />
                    <p>
                        6.1 A Magnificat.Video somente concede autorização para você visualizar o nosso conteúdo, sendo que, protegido por direitos autorais, a reprodução e o download são estritamente proibidos.
                    </p><br />
                    <p>
                        6.2 Você compromete-se a não utilizar nenhuma medida técnica que possa contornar a impossibilidade de download do nosso Conteúdo, visando permitir a sua conservação permanente em suportes físicos de qualquer tipo (disco rígido, memória flash, DVD, CD, K7...), bem como o compartilhamento nas redes da Internet, sejam quais forem os meios utilizados.
                    </p><br />
                    <p>
                        6.3 O Usuário declara que o fato de infringir uma medida de proteção técnica o expõe, por aplicação das disposições da legislação, ao pagamento de uma multa equivalente a 5 (cinco) vezes o valor do último pagamento efetuado, e que o fato de conscientemente adquirir ou oferecer a terceiros, direta ou indiretamente, meios concebidos ou especialmente adaptados para infringir uma medida de proteção técnica é, por sua vez, punível com uma pena de prisão de seis meses e uma multa equivalente a 10 (dez) vezes o valor do último pagamento efetuado.
                    </p><br />
                    <p>
                        7. Senhas e acesso à conta:  O assinante que criou a conta Magnificat.Video e cuja Forma de Pagamento é cobrada pelo serviço (“Proprietário da Conta”) possui acesso e controle sobre a conta Magnificat.Video e os aparelhos compatíveis com a Magnificat.Video usados para acessar o nosso serviço, sendo responsável por qualquer atividade que ocorre em sua conta Magnificat.Video. Para manter o controle sobre a conta e evitar que qualquer pessoa possa acessá-la (o que incluiria informações dos títulos assistidos da conta), o Proprietário da Conta deve manter o controle sobre os aparelhos compatíveis com a Magnificat.Video utilizados para acessar o serviço e não revelar a ninguém a senha ou os detalhes da Forma de Pagamento associada à conta. Você é responsável por atualizar e manter exatas as informações fornecidas à Magnificat.Video relativas à sua conta. A Magnificat.Video poderá cancelar ou suspender a sua conta para proteger você, a Magnificat.Video ou nossos parceiros contra falsidade ideológica ou outras atividades fraudulentas.
                    </p><br />
                    <p>
                        8. Garantias e isenção de responsabilidades: O SERVIÇO MAGNIFICAT,VIDEO É FORNECIDO “NO ESTADO EM QUE SE ENCONTRA”, SEM GARANTIAS OU CONDIÇÕES. ESPECIFICAMENTE, O NOSSO SERVIÇO PODE NÃO ESTAR LIVRE DE INTERRUPÇÕES OU ERROS. VOCÊ RENUNCIA A TODAS E QUAISQUER INDENIZAÇÕES POR DANOS INDIRETOS, MORAIS E MATERIAIS CONTRA A MAGNIFICAT,VIDEO. A MAGNIFICAT,VIDEO NÃO É RESPONSÁVEL PELA QUALIDADE DE SUA CONEXÃO COM A INTERNET. Estes Termos não limitarão nenhuma garantia ou direito renunciáveis de  proteção ao consumidor, aos quais você possa ter direito de acordo com as leis de seu país de residência.
                    </p><br />
                    <p>

                    </p><br />
                    <p>
                        8.2 O Usuário assume total responsabilidade por qualquer ação, conteúdo, informação ou dados, que ele baixe, poste, envie ou armazene em conexão com o uso da plataforma. O Usuário deverá indenizar a Magnificat.Video por todos os danos, perdas e despesas incorridas como resultado de suas palavras ou ações.
                    </p><br />
                    <p>
                        8.3 Por razões de segurança e ordem pública, podemos reter, por um período de tempo razoável, de acordo com os regulamentos aplicáveis, uma cópia do conteúdo compartilhado por um Usuário, mesmo que o Usuário tenha excluído sua conta ou que tal conteúdo tenha sido excluído da plataforma.
                    </p><br />
                    <h4>9. Propriedade e seus direitos de usar o serviço e o conteúdo</h4><br />
                    <p>
                        1. Titularidade. O Serviço e todo o seu Conteúdo, incluindo todos os direitos autorais, patentes, marcas registradas, marcas de serviço, nomes comerciais e todos os outros direitos de propriedade intelectual no Serviço e no Conteúdo ("Propriedade Intelectual"), são de propriedade ou controlados pela Magnificat.Video, nossos licenciadores e alguns outros terceiros, salvo se o conteúdo já estiver em domínio público. Todos os direitos, títulos e interesses sobre o Conteúdo e a Propriedade Intelectual disponível através do Serviço são propriedade da Magnificat.Video, de nossos licenciadores ou de certos terceiros, salvo se o conteúdo já estiver em domínio público, e são protegidos pelos direitos autorais, marcas registradas, patentes e/ou outros direitos de propriedade intelectual, em toda a extensão possível. A Magnificat.Video detém os direitos autorais na seleção, compilação, montagem, arranjo e aperfeiçoamento do Conteúdo do Serviço.
                    </p><br />
                    <h4>10. Diversos </h4><br />
                    <p>
                        10.1. Legislação aplicável. Estes Termos de uso devem ser regidos por e interpretados de acordo com a legislação do Brasil.
                    </p><br />
                    <p>
                        10.2. Materiais não solicitados. A Magnificat.Video não aceita materiais ou ideias não solicitadas de conteúdo da Magnificat.Video e não é responsável pela semelhança de seu conteúdo ou programação em qualquer mídia com materiais ou ideias transmitidos à Magnificat.Video.
                    </p><br />
                    <p>
                        10.3. Atendimento ao cliente. Para obter mais informações sobre o serviço Magnificat.Video, suas funcionalidades e recursos, ou caso precise de ajuda com sua conta, por favor, acesse o Centro de ajuda Magnificat.Video em nosso site. Em alguns casos, a melhor opção para o atendimento ao cliente é utilizar uma ferramenta de acesso remoto que dá acesso total ao seu computador. Se você não quiser que tenhamos esse acesso, você não deve fornecer seu consentimento para o uso da ferramenta de suporte por acesso remoto. A Magnificat.Video ajudará você de outras formas. No caso de quaisquer conflitos entre estes Termos de uso e informações fornecidas pelo atendimento ao cliente, estes Termos de uso prevalecerão.
                    </p><br />
                    <p>
                        10.4. Subsistência. Se qualquer disposição ou disposições destes Termos de uso forem consideradas inválidas, ilegais ou não aplicáveis, a validade, legalidade e aplicabilidade das demais disposições devem permanecer em pleno vigor.
                    </p><br />
                    <p>
                        10.5. Alterações dos termos de uso e cessão. A Magnificat.Video poderá alterar estes Termos de uso periodicamente. Os usuários estarão cientes das mudanças, colocando de forma proeminente no site ou aplicativo um aviso sobre a alteração. Estas mudanças podem exigir o consentimento dos usuários, conforme apropriado. Em qualquer caso, qualquer uso futuro dos serviços Magnificat.Video implica a aceitação total pelos usuários das mudanças feitas nesta Política de Privacidade. Na medida do permitido por lei, estas mudanças serão aplicáveis a partir do momento em que forem postadas.
                    </p><br />
                    <p>
                        10.6. Comunicações eletrônicas. Enviaremos informações relacionadas à sua conta (por exemplo, autorizações de pagamento, cobranças, alterações de senha ou Forma de Pagamento, mensagens de confirmação, notificações) somente em formato eletrônico como, por exemplo, por meio de emails para o endereço fornecido durante a inscrição.
                    </p><br />
                    <p>
                        10.7. Contato. Caso você tenha algum motivo de reclamação, nos esforçaremos para resolver o problema e evitar qualquer reincidência no futuro. Você pode sempre entrar em contato conosco usando os seguintes detalhes:
                        Razão Social: MAGNIFICAT VIDEO ENTRETENIMENTO E COMUNICAÇÃO MULTIMIDIA LTDA
                       
                    </p><br />
                   <strong>E-mail:</strong><br />
                    <p>suporte@magnificat.video</p>
                    <p>contato@magnificat.video</p><br />
                    <Link className="btn-politica" to="/cadastre-se">Voltar</Link>
                </div>
            </div>
            <div className="privacy_footer">
                <Footer />
            </div>
        </>
    );
}
export default PrivacyPolicy;
import api from '../../services/api';
import React, { ReactNode, useState } from 'react';
import logoblue from '../../assets/imagens/Logo/SVG/logomagnificat.svg';

function ResetPassword() {
    const [email, setEmail] = useState('');
    const [token, setToken] = useState('');
    const [messageRequest, setMessageRequest] = useState<ReactNode>(<>
        <h3>Esqueceu sua senha?</h3>
        <p>Digite seu email a baixo para enviarmos o codigo de verificação.</p>
    </>);
    const [newPassword, setNewPassword]= useState('');
    const [checkNewPassword, setCheckNewPassword] = useState('');

    let steps: any;
    steps = document.querySelectorAll('.reset-password__step')
    // Enviando email.
    async function sendEmail() {
        if (validateEmail()) {
            const data = new FormData();
            data.append('email', email);
            api.post('/send-token-recovery-password', data)
                .then(response => {
                    if (response.data.result) {
                        setMessageRequest(
                            <>
                                <h3>Código enviado com sucesso</h3>
                                <p>Verifique seu email, enviamos um código para redefinir sua senha</p>
                                <p>Digite o codigo a baixo</p>
                            </>
                        );
                        steps[0].classList.remove('active-step-password');
                        steps[1].classList.add('active-step-password');
                        steps[2].classList.remove('active-step-password');
                    }
                    else {
                        setMessageRequest(
                            <>
                                <h3>Houve um erro</h3>
                                <p>{response.data.response}</p>
                            </>
                        );
                    }
                })
                .catch(function (error) {

                    setMessageRequest(
                        <>
                            <h3>Houve um erro inesperado</h3>
                            <p>Verifique sua conexão com a internet e tente novamente.</p>
                        </>
                    );
                })
        }
    }
    // Validando email
    function validateEmail() {
        return !(email.indexOf('@') == -1 || email.indexOf('.') == -1 || email.length < 6);
    }
    // Enviando Token
    async function sendToken() {
        if (validateToken()) {
            const data = new FormData();
            data.append('token', token);
            api.post('/check-token-recovery-password', data)
                .then(response => {
                    if (response.data.result) {
                        setMessageRequest(
                            <>
                                <h3>Redefir senha.</h3>
                                <p>Digite uma nova senha.</p>
                            </>
                        )
                        steps[0].classList.remove('active-step-password');
                        steps[1].classList.remove('active-step-password');
                        steps[2].classList.add('active-step-password');

                    }
                    else{
                        setMessageRequest(
                            <>
                                <h3>Houve um erro</h3>
                                <p>{response.data.response}</p>
                            </>
                        );
                    }

                })

        }

    }
    // Validando Token
    function validateToken() {
        return !(token.length < 6);
    }
    // Nova senha
    function redefinePassword(){
        if(validateNewPassword()){
            const data = new FormData();
            data.append('password', newPassword);
            api.post('/redefine-password', data)
            .then(response => {
                if(response.data.result){
                    window.location.replace('/inicio')

                }
                else {
                    setMessageRequest(
                        <>
                            <h3>Houve um erro</h3>
                            <p>{response.data.response}</p>
                        </>
                    );
                }
                
            })
            .catch(function (error) {

                setMessageRequest(
                    <>
                        <h3>Houve um erro inesperado</h3>
                        <p>Verifique sua conexão com a internet e tente novamente.</p>
                    </>
                );
            })
          
        }

    }
    // Validando nova senha
    function validateNewPassword(){
        let error = [] as String[]
        if (newPassword != checkNewPassword) error.push('As senhas não são identicas');
        if (newPassword.length < 8) error.push('A senha deve conter no minimo 8 caracteres')
        return error.length > 0 ? false : true;
    }
    return (
        <div id="container-reset-password">
            <div className="reset-password__content">
                {/* Recebendo Email e enviando token */}
                <div className="reset-password__step active-step-password" >
                    {messageRequest}
                    <input value={email} onChange={event => setEmail(event.target.value)} placeholder="Seu email" type="email" name="reset-password" id="reset-password" /><br />
                    <button className="btn-disabled" onClick={sendEmail} disabled={email.indexOf('@') == -1 || email.indexOf('.') == -1 || email.length < 6}>Enviar</button>
                </div>
                {/* checando token */}
                <div className="reset-password__step ">
                    {messageRequest}
                    <input value={token} onChange={event => setToken(event.target.value)} placeholder="seu codigo" type="text" name="token" id="token" /><br />
                    <button onClick={sendToken} disabled={false}>Enviar</button>
                </div>
                {/* redefinindo senha */}
                <div className="reset-password__step">
                    {messageRequest}
                    <input value={newPassword} onChange={event => setNewPassword(event.target.value)} placeholder="nova senha" type="text" name="token" id="token" /><br />
                    <input value={checkNewPassword} onChange={event => setCheckNewPassword(event.target.value)} placeholder="repita a senha" type="text" name="token" id="token" /><br />
                    <button onClick={redefinePassword} disabled={false}>Enviar</button>
                </div>
            </div>


            <img src={logoblue} alt="" />
        </div>
    );
}

export default ResetPassword;
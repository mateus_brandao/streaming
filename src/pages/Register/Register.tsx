import React from 'react';

import { RegisterProvider } from '../../contexts/RegisterContext';
import RegisterController from '../../components/Register/RegisterController';

function Register() {
    return (
        <RegisterProvider>
            <RegisterController/>
        </RegisterProvider>
    );
}
export default Register;
import React, { ChangeEvent, useContext, useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import Input from '../../components/Dashboard/Dashboard-input/index';
import Header from '../../components/Header';
import BtnBorderBlue from '../../components/Buttons/Button-border-blue';
import BtnRed from '../../components/Buttons/Button-red';
import BtnWite from '../../components/Buttons/Button-wite';
import api from "../../services/api";
import { GeneralContext } from "../../contexts/GeneralContext";

interface User {
    first_name: string;
    last_name: string;
    email: string;
    permission: string;
}
interface Category {
    id: number;
    name: string;
}
interface Genre {
    id: number;
    name: string;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    category?: string;
    genre?: string;
    url: string;
    trailer_url?: string;
    trailer_name?: string;
    trailer_thumbnail?: string;
    duration: string;
    approval: string;
    cost?: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface Extra{
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}

function DashboardAdm() {
    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'provider' });
    const [approveMovies, setApproveMovies] = useState<Movie[]>([]);
    const [movies, setMovies] = useState<Movie[]>([]);
    const {setOpen, setMessage} = useContext(GeneralContext);
    // BEGIN:: REGISTER MOVIE
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [sinopse, setSinopse] = useState('');
    const [parentalRating, setParentalRating] = useState('');
    const [release, setRelease] = useState('');
    const [country, setCountry] = useState('');
    const [direction, setDirection] = useState('');
    const [production, setProduction] = useState('');
    const [script, setScript] = useState('');
    const [composition, setComposition] = useState('');
    const [cast, setCast] = useState('');
    const [category, setCategory] = useState<String[]>([]);
    const [genre, setGenre] = useState<String[]>([]);
    const [url, setUrl] = useState('');
    const [duration, setDuration] = useState('');
    const [cost, setCost] = useState('');
    const [expiration, setExpiration] = useState('');
    const [thumbnail, setThumbnail] = useState('');
    const [wallpaper, setWallpaper] = useState('');
    const [trailersUrl, setTrailersUrl] = useState('');
    const [trailersName, setTrailersName] = useState('');
    const [trailersThumbnail, setTrailersThumbnail] = useState('');
    const [extras, setExtras] = useState<Extra[]>([]);
    // END:: REGISTER MOVIE
    const [categories, setCategories] = useState<Category[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [newCategory,setNewCategory] = useState('');
    const [newGenre,setNewGenre] = useState('');
    const [validate,setValidate] = useState(false);
    // BEGIN:: GALLERY
    const [gallery, setGallery] = useState([]);
    const [galleryType, setGalleryType] = useState('thumbnail');
    const [uploadImages, setUploadImages] = useState<File[]>([]);
    // END:: GALLERY | BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");
        
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getCategories();
            getGenres();
            getApproveMovies();
            getMovies();
            getUser();
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    useEffect(() => {
        if(validate){
            storeMovie();
            setValidate(false);
        } 
    },[validate]);

    function navAbas(n: number) {
        let aba: any
        let qtdAba: number
        let current: number
        aba = document.querySelectorAll('#contral_abas li')
        current = n;
        qtdAba = aba.length;
        if (current === 0) {
            aba[0].classList.add("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 1) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.add("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 2) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.add("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 3) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.add("adm-active");
        }

    }
    // BEGIN:: HANDLE EXTRA
    function addExtra() {
        let lastId = 1;
        if(extras.length > 0){
            lastId = extras[extras.length-1].id + 1;
        }
        setExtras([ ...extras,{
            id: lastId,
            url: '',
            name: '',
            thumbnail: '',
        }]);
    }
    function removeExtra(id: number) {
        let tempExtras = extras;
        setExtras([]);
        tempExtras.splice(extras.findIndex(extra => extra.id === id), 1);
        setExtras(tempExtras);
    }
    function handleExtra(extraParams: Extra){
        let tempExtras = extras;
        let index = extras.findIndex(extra => extra.id === extraParams.id);
        
        tempExtras[index] = extraParams;
            
        setExtras(tempExtras);
        console.log(extras);
    }
    // ADD:: HANDLE EXTRA
    function resetAddMovie() {
        setName('');
        setDescription('');
        setSinopse('');
        setParentalRating('');
        setRelease('');
        setCountry('');
        setDirection('');
        setProduction('');
        setScript('');
        setComposition('');
        setCast('');
        setCategory([]);
        setGenre([]);
        setUrl('');
        setTrailersUrl('');
        setTrailersName('');
        setTrailersThumbnail('');
        setDuration('');
        setCost('');
        setExpiration('');
        setThumbnail('');
        setWallpaper('');
    }
    function validateMovie() {
        console.log(genre,category);

        let error = [] as String[]

        if (name.length == 0) error.push("O título do filme é obrigatório");
        if (description.length == 0) error.push("A descrição do filme é obrigatória");
        if (sinopse.length == 0) error.push("A sinopse é obrigatória");
        if (parentalRating.length == 0) error.push("A classificação indicativa é obrigatória");
        if (release.length == 0) error.push("O ano de lançamento é obrigatório");
        if (country.length == 0) error.push("O país é obrigatório");
        if (direction.length == 0) error.push("O campo direção é obrigatório");
        if (production.length == 0) error.push("O campo produção é obrigatório");
        if (script.length == 0) error.push("O roteiro é obrigatório");
        if (composition.length == 0) error.push("A composição musical é obrigatória");
        if (cast.length == 0) error.push("O elenco é obrigatório");
        if (category.length == 0) error.push("A categoria é obrigatória");
        if (genre.length == 0) error.push("O gênero é obrigatório");
        if (url.length == 0) error.push("O campo 'Id filme vimeo' é obrigatório");
        if (thumbnail.length == 0) error.push("A thumbnail é o  brigatória");
        if (wallpaper.length == 0) error.push("A capa do filme é obrigatória");
        if (trailersUrl.length == 0) error.push("O campo 'Id treiler vimeo' é obrigatório");
        if (trailersName.length == 0) error.push("O título do trailer é obrigatório");
        if (trailersThumbnail.length == 0) error.push("A thumbnail do trailer é obrigatória");
        if (duration.length == 0) error.push("A duração do filme é obrigatória");
        if (expiration.length == 0) error.push("A data de expiração do filme é obrigatória");
        if(cost.length == 0) error.push("O preço é obrigatório");

        if (error.length > 0) {
            let text = "";
            error.forEach(msg => text += msg + "\n");
            message(text);
        }
        return error.length > 0 ? false : true;
    }
    function handleCheckCategoryAndGenre(){
        let checkCategories = document.getElementsByName('checkCategories') as NodeListOf<HTMLInputElement>;        
        let arrCategories = [] as String[];
        Array.from(checkCategories).forEach(category => {
            if(category.checked){
                arrCategories.push(String(category.getAttribute('value')));
                category.checked = false;
            }
        });
        setCategory(arrCategories);

        let checkGenres = document.getElementsByName('checkGenres') as NodeListOf<HTMLInputElement>;        
        let arrGenres = [] as String[];
        Array.from(checkGenres).forEach(genre => {
            if(genre.checked){
                arrGenres.push(String(genre.getAttribute('value')));
                genre.checked = false;
            }
        });
        setGenre(arrGenres);
        
        setValidate(true);
    }
    async function storeMovie() {
        if (validateMovie()) {
            const data = new FormData();

            data.append('name', name);
            data.append('description', description);
            data.append('sinopse', sinopse);
            data.append('parental_rating', parentalRating);
            data.append('release', release);
            data.append('country', country);
            data.append('direction', direction);
            data.append('production', production);
            data.append('script', script);
            data.append('composition', composition);
            data.append('cast', cast);
            data.append('cost', cost);
            category.forEach((item,index) => { data.append(`category[${index}]`, String(item)); });
            genre.forEach((item,index) => { data.append(`genre[${index}]`, String(item)); })
            data.append('url', url);
            data.append('trailer_url', trailersUrl);
            data.append('trailer_name', trailersName);
            data.append('trailer_thumbnail', trailersThumbnail);
            data.append('duration', duration);
            data.append('expiration', expiration);
            data.append('thumbnail', thumbnail);
            data.append('wallpaper', wallpaper);

            api.post('auth/registerMovie', data, { headers: { "Authorization": `Bearer ${access_token}` } })
               .then(response => {
                if (response.data.result) {
                    setApproveMovies([...approveMovies, response.data.response]);
                    message('Filme cadastrado com sucesso');
                    resetAddMovie();
                }
                else message(response.data.response);
            });
        }
    }
    function storeCategory() {
        if(newCategory.length == 0){
            message('Digite o nome da nova categoria');
            return ;
        }
        const data = new FormData();

        data.append('name', newCategory);

        api.post('auth/registerCategory', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
            if (response.data.result) {
                setCategories([...categories, response.data.response]);
                message('Categoria cadastrada com sucesso');
                setNewCategory('');
            }
            else message(response.data.response);
        });
    }
    function storeGenre() {
        if(newGenre.length == 0){
            message('Digite o nome do novo gênero');
            return ;
        }
        const data = new FormData();

        data.append('name', newGenre);

        api.post('auth/registerGenre', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
            if (response.data.result) {
                setGenres([...genres, response.data.response]);
                message('Gênero cadastrado com sucesso');
                setNewGenre('');
            }
            else message(response.data.response);
        });
    }
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    // BEGIN: APPROVE MOVIE
    function approveMovie(id: number){
        const data = new FormData();

        data.append('movie_id', String(id));
        api.post('auth/approveMovie',data,{ headers: {"Authorization" : `Bearer ${access_token}`} })
           .then(response => {
            if(response.data.result){ 
                message('Filme Aprovado!');
                getMovies();
                getApproveMovies();
            }
            else message(response.data.response);
        });
    }
    function disapproveMovie(id: number){
        const data = new FormData();

        data.append('movie_id', String(id));
        api.post('auth/disapproveMovie',data,{ headers: {"Authorization" : `Bearer ${access_token}`} })
           .then(response => {
            if(response.data.result){
                message('Filme Reprovado!'); 
                getMovies();
                getApproveMovies();
            }
            else message(response.data.response);
        });
    }
    // END: APPROVE MOVIE | BEGIN:: DELETE MOVIE / CATEGORY / GENRES
    function deleteMovie(id: number){
        api.get(`auth/deleteMovie/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if(response.data.result){
                message('Filme Excluído!'); 
                getMovies();
            }
            else message(response.data.response);
        });
    }
    function deleteApproveMovie(id: number){
        api.get(`auth/deleteApproveMovie/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if(response.data.result){
                message('Filme Excluído!');
                getApproveMovies();
            }
            else message(response.data.response);
        });
    }
    function deleteCategory(id: number){
        api.get(`auth/deleteCategory/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if(response.data.result){
                message('Categoria Excluída!'); 
                getCategories();
            }
            else message(response.data.response);
        });
    }
    function deleteGenre(id: number){
        api.get(`auth/deleteGenre/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if(response.data.result){
                message('Categoria Excluída!'); 
                getGenres();
            }
            else message(response.data.response);
        });
    }
    // BEGGIN:: GETTERS API
    function getCategories() {
        api.get('auth/indexCategory', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setCategories(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getGenres() {
        api.get('auth/indexGenre', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setGenres(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getApproveMovies() {
        api.get('auth/movieWaitingApproval', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { setApproveMovies(response.data.response) }
                else message(response.data.response);
            });
    }
    function getMovies() {
        api.get('auth/indexMovie', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setMovies(response.data.response)
                }
                else message(response.data.response);
            });
    }
    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
            }
        });
    }
    function getGallery(type: string){
        api.get(`auth/getGallery/${type}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) setGallery(response.data.response);
        });
    }
    // END:: GETTERS API
    // BEGGIN: GALLERY 
    let modalPayment: any;
    function openModalGallery(type: string){
        getGallery(type);
        setGalleryType(type);
        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "100%";
        modalPayment.style.opacity = "1";
        modalPayment.style.display = "block";
    }
    function closeModalGallery(){
        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "0" ;
        modalPayment.style.opacity = "0" ;
        modalPayment.style.display = "none";
        setGallery([]);
    }
    function selectedImage(image_url: string){
        switch(galleryType){
            case 'thumbnail':            setThumbnail(image_url);        break;
            case 'wallpaper':            setWallpaper(image_url);        break;
            case 'horizontal-thumbnail': setTrailersThumbnail(image_url); break;
            default:                     message('Galeria inválida');    break;
        }
        closeModalGallery();
    }
    function handleSelectImages(event: ChangeEvent<HTMLInputElement>){
        if(!event.target.files) return ;
        const selectedImages = Array.from(event.target.files);
        setUploadImages(selectedImages);
    }
    function submitUploadImages(){
        const data = new FormData();

        data.append('type', galleryType);
        uploadImages.forEach((image,index) => {
            data.append(`images[${index}]`,image);
        });

        api.post('auth/uploadImages', data, { headers: { "Authorization": `Bearer ${access_token}` } })
           .then(response => {
            if (response.data.result) {
                message(response.data.response);
                closeModalGallery();
                openModalGallery(galleryType);
            }
            else message(response.data.response);
        });
    }
    // END:: GALLERY
    return (
        <div className="containerAdm">
            <Header />
            <div className="adm-content">
                <div className="container_nav_control">
                    <ul >
                        <li onClick={() => navAbas(0)}>Dashboard</li>
                        <li onClick={() => navAbas(1)}>add filme</li>
                        <li onClick={() => navAbas(2)}>Filmes cadastrados</li>
                        <li onClick={() => navAbas(3)}>add podcast</li>
                    </ul>
                </div>
                <div className="content-adm_abas">
                    <ul id="contral_abas" style={{fontFamily: "'Inter', sans-serif"}}>
                        {/*Daschbord */}
                        <li className="adm-active" style={{ color: '#ccc' }}>
                            <h2>Dashboard</h2>
                            {/* Categorias */}
                            <div className="content_add-movie">
                            {user.permission == 'root' && (
                                <div style={{display: 'flex', flexWrap: 'wrap', width: '100%', justifyContent: 'center'}}>
                                    <div style={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap', width: 'calc(50% - 2rem)', minWidth: 'calc(360px - 2rem)', maxWidth: 'calc(100% - 2rem) !important', border: '1px solid #999', padding: '.4rem'}}>
                                        <strong style={{marginBottom: '.4rem'}}>Gêneros</strong>

                                        {genres.map(item =>{ return (
                                        <div style={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingTop: '.2rem', paddingBottom: '.2rem', borderBottom: '1px solid #333',}} key={item.id}>
                                            <span>{item.name}</span>
                                            <BtnRed type="button" name="Excluir" style={{paddingTop: '.3rem', paddingBottom: '.3rem', fontSize: '80%'}} onClick={()=> deleteGenre(item.id)}/>
                                        </div>
                                        );})}
                                        <div style={{display: 'flex',  width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingTop: '.2rem', paddingBottom: '.2rem'}}>
                                            <div className="adm_agroup-input">
                                                <input type="text" name="new-genre" id="new-genre" style={{border: 'none', background: 'transparent', color: '#ccc'}} placeholder="Novo Gênero"  value={newGenre} onChange={event => setNewGenre(event.target.value)}/>
                                            </div>
                                            <BtnBorderBlue type="button" name="Novo" style={{padding: '.3rem', fontSize: '80%'}} onClick={()=> storeGenre()}/>
                                        </div>
                                    </div>
                                    <div style={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap', width: 'calc(50% - 2rem)', minWidth: 'calc(360px - 2rem)', maxWidth: 'calc(100% - 2rem) !important', border: '1px solid #999', padding: '.4rem'}}>
                                        <strong style={{marginBottom: '.4rem'}}>Categorias</strong>
                                       
                                        {categories.map(item =>{ return (
                                        <div style={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingTop: '.2rem', paddingBottom: '.2rem', borderBottom: '1px solid #333',}} key={item.id}>
                                            <span>{item.name}</span>
                                            <BtnRed type="button" name="Excluir" style={{paddingTop: '.3rem', paddingBottom: '.3rem', fontSize: '80%'}} onClick={()=> deleteCategory(item.id)}/>
                                        </div>
                                        );})}

                                        <div style={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center', paddingTop: '.2rem', paddingBottom: '.2rem'}}>
                                            <div className="adm_agroup-input">
                                                <input type="text" name="new-category" id="new-category" style={{border: 'none', background: 'transparent', color: '#ccc'}} placeholder="Nova Categoria" value={newCategory} onChange={event => setNewCategory(event.target.value)}/>
                                            </div>
                                            <BtnBorderBlue type="button" name="Novo" style={{padding: '.3rem', fontSize: '80%'}} onClick={()=> storeCategory()}/>
                                        </div>
                                    </div>
                                </div>
                            )}
                            </div>
                        </li>
                        {/*ADD filme */}
                        <li>
                            <div className="content_add-movie">
                                <form>
                                    <h5 style={{ width: '100%', marginBottom: '5px' }}>DADOS DO FILME</h5>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="id-vimeo">Id filme vimeo</label>
                                        <input type="number" name="id-vimeo" id="id-vimeo"
                                            value={url} onChange={event => setUrl(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="MovieTitle">Titulo do filme</label>
                                        <input type="text" name="MovieTitle" id="MovieTitle"
                                            value={name} onChange={event => setName(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="indicatedAge">Classificação indicativa</label>
                                        <input type="text" name="indicatedAge" id="indicatedAge"
                                            value={parentalRating} onChange={event => setParentalRating(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input input-movieTime">
                                        <label htmlFor="movieTime">tempo de duração do filme</label>
                                        <input type="time" step='1' name="movieTime" id="movieTime"
                                            value={duration} onChange={event => setDuration(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="release">Ano de lançamento</label>
                                        <input type="date" name="release" id="release"
                                            value={release} onChange={event => setRelease(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="production">Pais</label>
                                        <input type="text" name="country" id="country"
                                            value={country} onChange={event => setCountry(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="direction">Direção</label>
                                        <input type="text" name="direction" id="direction"
                                            value={direction} onChange={event => setDirection(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="production">Produção</label>
                                        <input type="text" name="production" id="production"
                                            value={production} onChange={event => setProduction(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="Script">Roteiro</label>
                                        <input type="text" name="Script" id="Script"
                                            value={script} onChange={event => setScript(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="composition">Composição musical</label>
                                        <input type="text" name="composition" id="composition"
                                            value={composition} onChange={event => setComposition(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="cast">Elenco</label>
                                        <input type="text" name="cast" id="cast"
                                            value={cast} onChange={event => setCast(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="cost">Preço</label>
                                        <input type="number" name="cost" id="cost"
                                            value={cost} onChange={event => setCost(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="expiration">Data de Expiração</label>
                                        <input type="date" name="expiration" id="expiration"
                                            value={expiration} onChange={event => setExpiration(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-txt-area">
                                        <label htmlFor="shortSynopsis">Descrição Resumida</label>
                                        <textarea name="shortSynopsis" id="shortSynopsis"
                                            value={description} onChange={event => setDescription(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-txt-area">
                                        <label htmlFor="FSynopsis">Sinopse</label>
                                        <textarea name="FSynopsis" id="FSynopsis" rows={6}
                                            value={sinopse} onChange={event => setSinopse(event.target.value)} />
                                    </div>
                                    <div style={{display: 'flex', width: '100%', flexWrap: 'wrap'}}>
                                        <div style={{width: '50%', minWidth: '360px', color: '#ccc', marginTop: '.3rem'}}>
                                            <label>Categorias</label>
                                            <div style={{display: 'flex', flexWrap: 'wrap', marginTop: '.3rem'}}>
                                            {categories.map((option) => {
                                                return (
                                                    <div style={{width: '50%', margin: '0', alignItems: 'center'}} key={option.id}>
                                                        <input type="checkbox" name='checkCategories' value={option.id} style={{ marginRight: '.3rem'}}/>{option.name}
                                                    </div>
                                                );
                                            })}
                                            </div>
                                        </div>
                                        <div style={{width: '50%', minWidth: '360px', color: '#ccc', marginTop: '.3rem'}}>
                                            <label>Gêneros</label>
                                            <div style={{display: 'flex', flexWrap: 'wrap', marginTop: '.3rem'}}>
                                            {genres.map((option) => {
                                                return (
                                                    <div style={{width: '50%', margin: '0', alignItems: 'center'}} key={option.id}>
                                                        <input type="checkbox" name='checkGenres' value={option.id} style={{ marginRight: '.3rem'}}/>{option.name}
                                                    </div>
                                                );
                                            })}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="thumbnail">URL da Thumbnail</label>
                                        <input type="text" name="thumbnail" id="thumbnail" placeholder="Proporção: 640x935"
                                            value={thumbnail} onChange={event => setThumbnail(event.target.value)} />
                                        <button type="button" onClick={()=> openModalGallery('thumbnail')}>Galeria</button>
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="wallpaper">URL da Capa</label>
                                        <input type="text" name="wallpaper" id="wallpaper" placeholder="Proporção: 1920x1080"
                                            value={wallpaper} onChange={event => setWallpaper(event.target.value)} />
                                        <button type="button" onClick={()=> openModalGallery('wallpaper')}>Galeria</button>
                                    </div>
                                    <h5 style={{ width: '100%', marginBottom: '5px' }}>DADOS DO TRAILER</h5>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="id-vimeo-trailer">Id trailer vimeo</label>
                                        <input type="number" name="id-vimeo-trailer" id="id-vimeo-trailer"
                                            value={trailersUrl} onChange={event => setTrailersUrl(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="trailer-name">Título do trailer</label>
                                        <input type="text" name="trailer-name" id="trailer-name"
                                            value={trailersName} onChange={event => setTrailersName(event.target.value)} />
                                    </div>
                                    <div className="adm_agroup-input">
                                        <label htmlFor="thumbnail">URL da Thumbnail do Trailer</label>
                                        <input type="text" name="trailersThumbnail" id="trailersThumbnail" placeholder="Proporção: 310x174"
                                            value={trailersThumbnail} onChange={event => setTrailersThumbnail(event.target.value)} />
                                        <button style={{padding: '5px'}} type="button" onClick={()=> openModalGallery('horizontal-thumbnail')}>Galeria</button>
                                    </div>
                                    {/* <h5 style={{ width: '100%', marginBottom: '5px' }}>EXTRAS <button type="button" style={{borderRadius: '50%',cursor: 'pointer'}} onClick={addExtra}><span style={{ verticalAlign: '2px'}}>+</span></button></h5>
                                    <div style={{width: '100%',display: 'flex', gap: '.8rem'}}>
                                        <div className="adm_agroup-input">
                                            <label htmlFor="id-vimeo-extra">Id Extra Vimeo</label>
                                            <input type="number" name="id-vimeo-extra"/>
                                        </div>
                                        <div className="adm_agroup-input">
                                            <label htmlFor="trailer-name">Título do trailer</label>
                                            <input type="text" name="extra-name"/>
                                        </div>
                                        <div className="adm_agroup-input">
                                            <label htmlFor="thumbnail">URL da Thumbnail do Trailer</label>
                                            <input type="text" name="trailersThumbnail" id="trailersThumbnail" placeholder="Proporção: 310x174"/>
                                            <button type="button" onClick={()=> openModalGallery('horizontal-thumbnail')}>Galeria</button>
                                        </div>
                                        <div style={{display: 'flex', alignItems: 'center'}}>
                                            <button type="button" style={{borderRadius: '50%',cursor: 'pointer', color: '#fff', background: '#d33'}}><span style={{ verticalAlign: '3px'}}>x</span></button>
                                        </div>
                                    </div> */}
                                </form>
                                <div className="container-btn__add-muvie">
                                    <button type="button" className="btn__add-muvie">Cancelar </button>
                                    <button type="button" className="btn__add-muvie" onClick={handleCheckCategoryAndGenre}>Cadastrar filme</button>
                                </div>
                            </div>
                        </li>
                        <li style={{ color: '#ccc' }}>
                            <h4>AGUARDANDO APROVAÇÃO</h4>
                            <div className="container-muvie-aprove">
                            {approveMovies.map(movie => { return (
                                <div key={movie.id}>
                                    <div className="card-muvie-aprove" style={{ backgroundImage: `url(${movie.thumbnail})`, backgroundSize: 'cover'}}>
                                        <div className="modal-muvie-title">
                                            {movie.name}
                                        </div>
                                        <div className="modal-muvie-info">
                                            <div className="modal-indicate">{movie.parental_rating}</div>
                                            <div className="muvie-time">{movie.duration}</div>

                                        </div>
                                    </div>
                                    <div className="indicate-aprove">
                                        <p style={{textAlign: 'center'}}>
                                        {movie.approval == 'approved' && (<span>APROVADO</span>)}
                                        {movie.approval == 'disapproved' && (<span>REPROVADO</span>)}
                                        {movie.approval == 'pending' && (<span>PENDENTE</span>)}
                                        </p>
                                    </div>
                                        {/*Botao */}
                                    {user.permission == 'root' && (
                                    <div className="aprove-btn-group" style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                                        <BtnBorderBlue type="button" name="Aprovar"
                                            onClick={() => approveMovie(movie.id)}/>
                                        <BtnRed type="button" name="Reprovar" style={{paddingTop: '.4rem', paddingBottom: '.4rem'}}
                                            onClick={() => disapproveMovie(movie.id)} />
                                        <BtnRed type="button" name="Excluir" style={{paddingTop: '.4rem', paddingBottom: '.4rem'}}
                                            onClick={() => deleteApproveMovie(movie.id)} />
                                    </div>  
                                    )}
                                </div>
                            );})}
                            </div>
                            {approveMovies.length == 0 && (<p>Você não tem nenhum filme aguardando aprovação</p>)}
                            <hr />
                            <h4>APROVADOS</h4>
                            <div className="container-muvie-aprove">
                            {movies.map(movie => { return (
                            <div key={movie.id}>
                                <div className="card-muvie-aprove" style={{ backgroundImage: `url(${movie.thumbnail})`, backgroundSize: 'cover'}}>
                                    <div className="modal-muvie-title">
                                        {movie.name}
                                    </div>
                                    <div className="modal-muvie-info">
                                        <div className="modal-indicate">{movie.parental_rating}</div>                                        <div className="muvie-time">{movie.duration}</div>
                                    </div>
                                </div>
                                {user.permission == 'root' && (
                                <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: '.4rem'}}>
                                    <BtnRed type="button" name="Excluir" style={{paddingTop: '.4rem', paddingBottom: '.4rem'}}
                                        onClick={() => deleteMovie(movie.id)} />
                                </div>  
                                )}
                            </div>
                            );})}
                            </div>
                            {movies.length == 0 && (<p>Você não tem nenhum filme aprovado</p>)}
                        </li>
                        <li style={{ color: '#ccc' }}>
                            <h2>PODCASTS</h2>
                        </li>
                    </ul>
                </div>
            </div>

            <div id="modal-gallery" className="change-payment__content-modal" style={{background: '#242326',fontFamily: "'Inter', sans-serif"}}>
                <div onClick={closeModalGallery} className="btn-close-modal" style={{fontSize: '1rem'}}> X </div> 
                <div style={{ padding: '.2rem 1.2rem'}}>
                    <h4 style={{color: '#ccc', textTransform: 'uppercase'}}>
                        {galleryType == 'thumbnail' && (<>Thumbnail <small>640x935</small></>)}
                        {galleryType == 'wallpaper' && (<>Capa <small>1920x1080</small></>)}
                        {galleryType == 'horizontal-thumbnail' && (<>Thumbnail de Trailer <small>310x174</small></>)}
                    </h4>                                
                    <input multiple type="file" id="image[]" onChange={handleSelectImages} 
                        style={{color: '#ccc', marginBottom: '.6rem'}}/>
                    <BtnBorderBlue type="button" name="Subir Imagens" onClick={submitUploadImages}/>
                </div>

                <div className="image-galery" style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    gap: '.4rem',
                    marginTop: '3rem',
                    justifyContent: 'center',
                    
                }}>
                    {gallery.map((image,index)=>{ return (
                        <div key={index} style={{width: '6rem', cursor: 'pointer'}}
                            onClick={()=> selectedImage(image)}>
                            <img src={image} style={{width: '100%'}}></img>
                        </div>
                    );})}
                    {gallery.length == 0 && (<p style={{color: '#ccc' }}>Sua galeria está vazia</p>) }
                </div>
            </div>
        </div>

    );
}

export default DashboardAdm;
import react from 'react';
import { Link } from 'react-router-dom';

function PageEndev(){
    const goBack = () =>{
        window.history.back();
    }
    return(
        <>
         <div id="pagina-em-dev">
             <h1>Página em desenvolvimento</h1>
             <button onClick={goBack}>Clique aqui para voltar</button>
         </div>
        </>

    );
}

export default PageEndev;
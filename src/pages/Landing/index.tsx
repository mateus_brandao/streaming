import React, { useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Quest from '../../components/Question';
import logowite from '../../assets/imagens/Logo/SVG/branco.svg';
import api from '../../services/api';
import Slider from "react-slick";

//inport img
import Imgdisp from '../../assets/imagens/disp-01.jpg';
import imgCard1 from '../../assets/imagens/capa/capa3.jpg';
import imgCard2 from '../../assets/imagens/capa/capa1.jpg';
import imgCard3 from '../../assets/imagens/capa/capa2.jpg';
import imgCard4 from '../../assets/imagens/capa/capa4.jpg';
import { GeneralContext } from '../../contexts/GeneralContext';
import logoblue from '../../assets/imagens/Logo/SVG/logomagnificat.svg';

interface SubscriptionPlanData {
    id: number;
    name: string;
    price: number;
    resolution: string;
    screens: string;
    profiles: string;
}


function Landing() {
    const history = useHistory();
    const { setOpen, setMessage } = useContext(GeneralContext);
    const [dataSubscriptionPlan, setDataSubscriptionPlan] = useState<SubscriptionPlanData[]>(
        [{ 'id': 0, 'name': '', 'price': 0.00, 'resolution': '', 'screens': '', 'profiles': '', }]);
    const [email, setEmail] = useState('');
    const [openModalPlan, setOpenModalPlan] = useState(false);
    if(openModalPlan == true){
        let janela: any;
       janela = document.querySelector('body');
       janela.style.overflow = 'hidden';
    }else{
        let janela: any;
       janela = document.querySelector('body');
        janela.style.overflow = 'auto';
    }
    const questions = [
        {
            id: '1',
            title: 'O que é Magnificat?',
            description: 'A Magnificat é um serviço de transmissão online que oferece uma ampla variedade de shows, podcasts, séries, filmes e documentários premiados em milhares de aparelhos conectados à internet.'
        },
        {
            id: '2',
            title: 'Quanto custa a Magnificat?',
            description: `Assista à Magnificat no seu celular, tablet, Smart TV, notebook ou aparelho de streaming por uma taxa mensal única. \n\
             Os planos variam de R$14,90 a R$39,90 por mês. Sem contrato nem taxas extras.`
        },
        {
            id: '3',
            title: 'Onde posso assistir?',
            description: 'Assista onde quiser, o quanto quiser e em um número ilimitado de aparelhos. Faça login com sua conta Magnificat em Magnificat.video'
        },
        {
            id: '4',
            title: 'Como faço para cancelar?',
            description: `O Amor não prende. Magnificat é livre e simples. Não há contratos nem compromissos.
            Aqui, se você quiser, poderá cancelar a sua conta com apenas dois cliques.
            Não há taxa de cancelamento – você pode começar ou encerrar a sua assinatura
            a qualquer momento.
             `
        },
        {
            id: '5',
            title: 'O que posso assistir na Magnificat?',
            description: `A Magnificat tem um grande catálogo podcasts, musicais, filmes, documentários, séries, originais Magnificat e muito mais. Assista o quanto quiser, quando quiser.`
        },
        {
            id: '6',
            title: 'Esqueci meu e-mail ou senha da Magnificat',
            description: `Se você não consegue entrar na Magnificat, você pode redefinir a senha por E-mail ou mensagem de texto caso tenha adicionado seu número de telefone à conta. Se tiver esquecido o E-mail ou o número de telefone informados na inscrição, você poderá fornecer informações adicionais online para recuperar sua conta.`
        },
        {
            id: '7',
            title: 'Como pagar a assinatura Magnificat? ',
            description: ` Escolha um plano  adequado às suas necessidades e ao orçamento. Como assinante da Magnificat, você é cobrado uma vez por mês no dia da sua assinatura. Existem várias maneiras de pagar a Magnificat.
            Cartões de crédito:  Aceitamos os seguintes cartões de crédito, que devem estar habilitados para transações recorrentes de comércio eletrônico.
            Visa
            
            MasterCard
            
            American Express
            
            Diners
            
            Elo
            
            Hipercard
            
            Cartões de débito: Todos 
            Cartões virtuais
            
            Aceitamos cartões virtuais em alguns mercados. Se o seu cartão virtual for recusado, escolha outra forma de pagamento.
            Cartões pré-pagos
            
            Aceitamos cartões pré-pagos das seguintes bandeiras:
            
            Visa
            
            MasterCard
            
            American Express`
        },
    ];

    function toRegister() {
        if (email.length > 5 && email.indexOf('@') != -1 && email.indexOf('.') != -1) {
            sessionStorage.setItem('mag_email', email);
            history.push('/cadastre-se');
        }
        else message('Insira um email válido antes de se cadastrar.');
    }
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    // carrosel
    var settings2 = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 872,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 680,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },
            {
                breakpoint: 478,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    dots: false
                }
            },

        ]
    }
    useEffect(() => {
        getPlan()

    }, []);
    async function getPlan() {
        api.get('indexSubscriptionPlan').then(response => {
            if (response.data.result) {
                setDataSubscriptionPlan(response.data.response)
            } else console.log(response.data.response);
        })
    }


    return (
        <div id="container_landing">
            <div className="header_container">
                <div className="logo">

                    <img src={logowite} alt="" />


                    <Link to="/conecte-se" className="btn_entrar">Entrar</Link>
                </div>
                <div className="efect__top">

                </div>
                <div className="efect__bottom">

                </div>
                <div className="aling-action2">

                    <div className="call-to-action">
                        <h1>Pronto para assistir?</h1>
                        <p>Informe seu E-mail para criar ou reiniciar sua assinatura</p>
                        <div className="call-input-group">
                            <input type="text" name="register" id="register" placeholder="Seu E-mail"
                                value={email} onChange={event => setEmail(event.target.value)} />
                            <button type="button" onClick={toRegister} className="btn"><span>assinar</span></button>
                        </div>
                    </div>

                </div>

            </div>



            <main>
                <div className="main-cardss">
                    <div className="container__carrossel-landing">
                        <Slider {...settings2}>
                            <div className="intem-carrosel__landing">
                                <div className="title-landing-card">
                                    <span>
                                        Shows
                                </span>
                                </div>
                                <img src={imgCard1} alt="" />

                            </div>
                            <div className="intem-carrosel__landing">
                                <div className="title-landing-card">
                                    <span>
                                        Vida dos <br />Santos
                                </span>
                                </div>
                                <img src={imgCard4} alt="" />
                            </div>
                            <div className="intem-carrosel__landing">
                                <div className="title-landing-card">
                                    <span>
                                        Lives
                                </span>
                                </div>
                                <img src={imgCard3} alt="" />
                            </div>
                            <div className="intem-carrosel__landing">
                                <div className="title-landing-card">
                                    <span>
                                        Filmes <br /> Clássicos
                                </span>
                                </div>
                                <img src={imgCard2} alt="" />
                            </div>


                        </Slider >

                    </div>


                    <div className="txt__landing">
                        <h4>
                            Filmes, <br /> séries, shows, <br /> lives, músicas <br /> e podcasts
                        </h4>
                        {dataSubscriptionPlan.map(values => {
                            if (values.name === 'Básico') {
                                return (
                                    <p>Com planos a partir de R$ {values.price} </p>
                                )
                            }
                        })}
                        <button onClick={() => setOpenModalPlan(!openModalPlan)} className="btn btn2"> <span>Conheça os planos</span></button>
                    </div>

                </div>
                {/* aqui */}
                <div className="main_card2">
                    <div className="card2_txt">
                        <div>
                            <h4>
                                Títulos <br />
                                exclusivos <br />
                                ao seu alcance
                            </h4>
                            <p> Assista aos melhores conteúdos <br />e maratone quando <br /> e onde quiser. </p>
                        </div>
                    </div>

                    <img src={Imgdisp} alt="" />

                </div>

                <div className="container_question">
                    <h2>Dúvidas Frequentes</h2>
                    <div className="question_content">
                        {questions.map(question => {
                            return (
                                <Quest key={question.id} title={question.title} description={question.description} />
                            )
                        })}
                    </div>
                </div>
                <div className="aling-action">
                    <div className="call-to-action2">
                        <h4>Pronto para assistir?</h4>
                        <p>Informe seu E-mail para criar ou reiniciar sua assinatura</p>
                        <div className="call-input-group">
                            <input type="text" name="register" id="register" placeholder="Seu E-mail"
                                value={email} onChange={event => setEmail(event.target.value)} />
                            <button type="button" onClick={toRegister} className="btn"><span>assinar</span></button>
                        </div>
                    </div>
                </div>
            </main>
            <Footer />
            <div className="modal-plan"
                style={{
                    display: `${openModalPlan ? 'flex' : 'none'} `
                }}
            >
                <div onClick={() => setOpenModalPlan(!openModalPlan)} className="close-modal-plan">
                    <i className="material-icons">highlight_off</i>
                </div>
                <div className="container-plans">
                    {dataSubscriptionPlan.map(plan => {
                        if (plan.id == 4) return null;
                        return (
                            <div key={plan.id} className="plan">
                                <h3>{plan.name}</h3>
                                <div className="img-plan">
                                    <img src={logoblue} alt="" />
                                </div>
                                <span className="plan-price">R$ {plan.price}</span>
                                <span>Resolução: {plan.resolution}</span>
                                <span>Telas simultâneas: {plan.screens}</span>
                            </div>
                        )
                    })}
                </div>


                <div className="call-to-action-plan">
                    <input type="text" name="register" id="register" placeholder=" Seu E-mail"
                        value={email} onChange={event => setEmail(event.target.value)} />
                    <button type="button" onClick={toRegister} className="btn"><span>assinar</span></button>
                </div>


            </div>
        </div>
    );
}

export default Landing;


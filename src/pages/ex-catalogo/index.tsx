import React, { useEffect, useRef, useState } from 'react';
import Header from '../../components/Header';
import defaltBanner from '../../assets/imagens/magnificatbase-02.jpg';
import Footer from '../../components/Footer';
import { Category, Genre, Trailer, Extra } from '../../types/typesdef';
import api from '../../services/api';
import Vimeo from '@u-wave/react-vimeo';
import LoadPage from '../../components/Load';
import { Link } from 'react-router-dom';

interface UserMovie {
    id: number;
    user_id: number;
    movie_id: number;
    current_time: string;
    viewed: boolean;
    favorite: boolean;
    rating: string;
    movie?: Movie;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    categories: Category[];
    genres: Genre[];
    url: string;
    trailers: Trailer[];
    extras?: Extra[];
    users?: UserMovie[];
    duration: string;
    cost: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface CategoryMovie {
    id: number;
    name: string;
    movies: Movie[];
}

function Catalogo() {
    const [access_token, setAccessToken] = useState("");
    const [movies, setMovies] = useState<Movie[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [genreSelect, setGenreSelect] = useState('todos')

    useEffect(() => {
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
        getMovies()
        getGenres()
    }, [access_token]);
    useEffect(() => {
        loadPage()
    }, []);
    function loadPage() {
        let spiner: any;
        spiner = document.querySelector('.on-load-page')
        spiner.style.display = 'none'
    }

    function getMovies() {
        api.get('auth/indexMovie', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setMovies(response.data.response)
                    console.log('teste', response.data.response)
                }
                else message(response.data.response);
            });
    }
    function getGenres() {
        api.get('auth/indexGenre', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setGenres(response.data.response);
                // console.log('teste y', response.data.response)
            }
            else message(response.data.response);
        });
    }
    function message(text: string) {
        // if (modalMessageOpen) { setModalMessageOpen(false); }
        // setModalMessageText(text);
        // setModalMessageOpen(true);
        console.log(text);
    }
    function checkSelect() {
        getMovies()
    }
    function goBack(){
        window.history.back();
    }

    return (
        <>
            <div className="catalogContainer">
                <Header />
                <div className="container_page_catalogo">
                    <div className="select-category">
                        <div className="go-back-catalogo">
                            <i onClick={goBack} className="material-icons">chevron_left</i>
                            <h3 className="catalogo">Catálogo</h3>

                        </div>
                        {/* <select onClick={dd} onChange={event => setValuegeSelect(event.target.value)} name="" id="">
                                    <option></option>
                        </select> */}
                        <select onClick={checkSelect} onChange={event => setGenreSelect(event.target.value)} name="" id="">
                                        <option>Todos</option>
                            {genres.map((option) => {
                                return (
                                        <option>{option.name}</option>
                                    
                                );
                            })}
                        </select>
                    </div>
                    <div className="catalog_main">
                        <div className="movie-list-ex">
                            {movies.map(movie => {
                                if (movie.genres[0].name == genreSelect) {
                                    return (
                                        <Link to={`/descricao-do-filme/${movie.id}`} key={movie.id} className="movie-list-ex_card">
                                            
                                            <img src={movie.thumbnail} alt="" />
                                        </Link>
                                    );
                                } else if (genreSelect == 'todos') {
                                    return (
                                        <Link to={`/descricao-do-filme/${movie.id}`} key={movie.id} className="movie-list-ex_card">
                                            
                                            <img src={movie.thumbnail} alt="" />
                                        </Link>
                                    );

                                }
                            })}
                        </div>

                    </div>
                </div>
            </div>
            <div className="on-load-page">
                <LoadPage />
            </div>
            <Footer />
        </>
    );
}
export default Catalogo;
import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import Vimeo from '@u-wave/react-vimeo';
import api from '../../services/api';
import Slider from "react-slick";
import LoadPage from '../../components/Load';
import MySlider from '../../components/Slider';
import DefaltTubnal from '../../assets/imagens/magnificatbase-01.jpg';
import Carrosel from '../../components/carousel';

// import img
import imgCard1 from '../../assets/imagens/capa/capa3.jpg';
import imgCard2 from '../../assets/imagens/capa/capa1.jpg';
import imgCard3 from '../../assets/imagens/capa/capa2.jpg';
import imgCard4 from '../../assets/imagens/capa/capa4.jpg';
//////////////

interface Category {
    id: number;
    name: string;
}
interface Genre {
    id: number;
    name: string;
}
interface Trailer {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}
interface Extra {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}
interface UserMovie {
    id: number;
    user_id: number;
    movie_id: number;
    current_time: string;
    viewed: boolean;
    favorite: boolean;
    rating: string;
    movie?: Movie;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    categories: Category[];
    genres: Genre[];
    url: string;
    trailers: Trailer[];
    extras?: Extra[];
    users?: UserMovie[];
    duration: string;
    cost: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface CategoryMovie {
    id: number;
    name: string;
    movies: Movie[];
}

interface User {
    first_name: string;
    last_name: string;
    email: string;
    permission: string;
    payment_status: string;
}

function Home() {
    //CARROSEL
    var settings2 = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false

                },
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false

                },
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false

                },
            },
        ]
    };
    var settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 620,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const containerCategory = [
        { 'name': 'Shows', 'img': imgCard1 },
        { 'name': 'Vida dos Santos', 'img': imgCard4 },
        { 'name': 'Lives', 'img': imgCard3 },
        { 'name': 'Filmes Clássicos', 'img': imgCard2 },
    ];

    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'user', payment_status: 'paid' });

    const [watchingMovies, setWatchingMovies] = useState<UserMovie[]>([handleDefaultUserMovie()]);
    const [favoriteMovies, setFavoriteMovies] = useState<UserMovie[]>([handleDefaultUserMovie()]);
    const [categoryMovies, setCategoryMovies] = useState<CategoryMovie[]>([]);

    // BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState("");

    useEffect(() => {
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
        if (sessionStorage.getItem('magnificat_popup') == null) openPopUp();
        
    }, []);

    useEffect(() => {
        if (access_token != "") {
            getUser();
            getFavoriteMovies(access_token);
            getWatchingMovies(access_token);
            getMovies(access_token);
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    useEffect(() => {
        if (user.first_name.length > 0) {
            loadPage();
            console.log(user);
        }
    }, [user]);

    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission,
                    'payment_status': response.data.payment_status
                });
            }
        });
    }
    // liste de filmes
    const [scrollX, setScrollX] = useState(0)
    let items: any
    items = document.querySelectorAll('.conteudo')
    const handleLeftArrow = () => {
        let x = scrollX + Math.round(window.innerWidth / 2);
        if (x > 0) { x = 0 }
        setScrollX(x)
    }
    const handleRigthArrow = () => {
        let x = scrollX - Math.round(window.innerWidth / 2);
        let listW = items.length * 150;
        if ((window.innerWidth - listW) > x) {
            x = (window.innerWidth - listW) - 60;
        }
        setScrollX(x);
    }
    // get movie random
    // function getMovieRandom(){
    //    if( categoryMovie) 
    //     let random = 0 a categoryMovies.length
    //     let random2 = 0  categoryMovies[random].movies.lenght;
    //     categoryMovies[random].movies[random2].trailers[0].url
    // }
    // fazer logica para possibilidade de gategoryMovie.length == 0 && categoryMovie.movie.length == 0
    function openPopUp() {
        let popUp: any;
        let apliBluer: any;
        apliBluer = document.querySelector('.banner_home_txt')
        popUp = document.querySelector('.pop-up-video')
        apliBluer.style.filter = 'blur(3px)'
        popUp.style.display = "flex";
    }
    function closePopUp() {
        let popUp: any;
        let apliBluer: any;
        apliBluer = document.querySelector('.banner_home_txt')
        popUp = document.querySelector('.pop-up-video')
        popUp.style.display = "none";
        apliBluer.style.filter = 'blur(0px)'
        sessionStorage.setItem('magnificat_popup', 'true');
    }
    // voltar aqui
    function loadPage() {
        let spiner: any;
        spiner = document.querySelector('.on-load-page')
        spiner.style.display = 'none'
    }

    let slider: number;
    slider = 210;
    let x: number;


    const [scrolX, setScrolX] = useState(0)
    const Scroll = useRef<HTMLDivElement>(null);

    const leftSlider = () => {
        if (Scroll.current) {
            x = scrolX + slider;
            Scroll.current.scrollTo(x, 0);
            setScrolX(x)
        }
    }

    const rigthSlider = () => {
        if (Scroll.current) {
            if (scrolX > 0) {
                x = scrolX - slider;
                Scroll.current.scrollTo(x, 0);
                setScrolX(x)
            }
        }

    }

    function getFavoriteMovies(token?: string) {
        if (token && !access_token) setAccessToken(token);
        api.get('auth/getFavoriteMovies', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setFavoriteMovies([...response.data.response]);
                }
                else message(response.data.response);
            });
    }
    function getWatchingMovies(token?: string) {
        if (token && !access_token) setAccessToken(token);
        api.get('auth/watchingMovies', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setWatchingMovies([...response.data.response]);
                }
                else message(response.data.response);
            });
    }
    function getMovies(token?: string) {
        console.log('entrou na função')
        if (token && !access_token) setAccessToken(token);
        api.get('auth/getMoviesCatalog', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setCategoryMovies([...response.data.response]);
                    console.log(response.data.response)
                }
                else message(response.data.response);
            });
    }
    function message(text: string) {
        // if (modalMessageOpen) { setModalMessageOpen(false); }
        // setModalMessageText(text);
        // setModalMessageOpen(true);
        console.log(text);
    }
    function filterCategory(name: string) {
        if (categoryMovies) {
            let index = categoryMovies.findIndex(category => category.name == name);
            if (index != -1) { console.log(categoryMovies[index].id); }
        }
    }
    function toggleRating(movie: Movie, rating: number) {
        const data = new FormData();

        data.append('rating', String(rating));
        data.append('movie_id', String(movie.id));

        api.post('auth/toggleRating', data, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                movie.users = [];
                movie.users[0] = response.data.response;
                updateOneMovie(movie);
            }
            else message(response.data.response);
        });
    }
    function toggleFavorite(movie: Movie) {
        const data = new FormData();
        data.append('movie_id', String(movie.id));
        api.post('auth/toggleFavorite', data, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                getFavoriteMovies();
                movie.users = [];
                movie.users[0] = response.data.response;
                console.log(response.data.response);
                updateOneMovie(movie);
            }
            else message(response.data.response);
        });
    }
    function updateOneMovie(movie: Movie) {
        let temp: any;
        let index: any;

        if (watchingMovies) {
            temp = watchingMovies;
            index = watchingMovies.findIndex(watching => watching.movie?.id == movie.id);
            if (index != -1) {
                temp[index].movie = movie;
                setWatchingMovies([...temp]);
            }
        }

        temp = categoryMovies;
        index = -2;
        categoryMovies.map((category, i) => {
            index = category.movies.findIndex(category => category.id == movie.id);
            if (index != -1) temp[i].movies[index] = movie;
        });
        if (index != -2) setCategoryMovies([...temp]);

        if (favoriteMovies) {
            temp = favoriteMovies;
            index = favoriteMovies.findIndex(favorite => favorite.movie?.id == movie.id);
            if (index != -1) {
                temp[index].movie = movie;
                setFavoriteMovies([...temp]);
            }
        }
    }
    function handleDefaultUserMovie() {
        return {
            id: 0,
            user_id: 0,
            movie_id: 0,
            current_time: '0',
            viewed: false,
            favorite: false,
            rating: "1",
        };
    }
   
   
   
    return (
        <div id="container-home">
            <Header />
            {user && (user.payment_status == 'paid' || user.permission != 'user' || true) ? (
                <>
                    <div className="container_banner_home">
                        <div className="content_baner_home">
                            <div className="banner_home_txt">
                                <p><span className="userName">{user.first_name},</span><br /> O que você quer <br /> assistir hoje?</p>
                                <Link to="/explorar-catalogo" className="btn-wite">Explorar catálogo</Link>
                            </div>
                        </div>
                        <div className="efect"></div>
                    </div>


                    <section className="container-category">
                        <h4>Categorias</h4>
                        <div className="container-my-carousel">
                            <div ref={Scroll} className="my-carousel-sroll ">
                                <div className="my-carousel container-category__content">
                                    {categoryMovies.map(category => {
                                        if (category.movies.length == 0) return (<></>);
                                        return (
                                            <Link to={`/categoria/${category.id}`} key={category.id} className="my-carousel-item container-category__item">
                                                <span>{category.name}</span>
                                                <img src={DefaltTubnal} alt="" />
                                                <div className="efect-home"></div>
                                            </Link>

                                        )
                                    })}
                                </div>
                            </div>
                            <div onClick={rigthSlider} className="btn-my-carrosel-left">
                                <button><i className="material-icons">chevron_left</i></button>
                            </div>

                            <div onClick={leftSlider} className="btn-my-carrosel-rigth">
                                <button><i className="material-icons">navigate_next</i></button>
                            </div>
                        </div>
                    </section>
                    <div>
                        <h1>Teste</h1>
                        <Carrosel />
                    </div>

                    {watchingMovies && watchingMovies.length > 0 && (
                        <div className="container_home">
                            <p>CONTINUE DE ONDE PAROU</p>
                            {watchingMovies.map(watching => {
                                if (!watching.movie) return (<></>);
                                let movie = watching.movie;
                                let date = new Date(`Thu, 01 Jan 1970 ${movie.duration}`);
                                let hour = `${date.getHours()}h ${date.getMinutes() == 0 ? '' : date.getMinutes() + 'm'}`;
                                let firstDate = new Date(movie.release);
                                let like = (movie.users && movie.users.length > 0 && movie.users[0].rating == '1') ? true : false;
                                let deslike = (movie.users && movie.users.length > 0 && movie.users[0].rating == '-1') ? true : false;
                                return (
                                    <div className="container_continue" key={movie.id}>
                                        <Link to={`/reproduzir/${movie.id}`} className="div_left">
                                            <i className="far fa-play-circle"></i>
                                            <img src={movie.wallpaper} alt="" />
                                        </Link>
                                        <div className="div_rigth">
                                            <div className="div_rigth1">
                                                <h4>{movie.name}</h4>
                                                <span>{firstDate.getFullYear()}.
                                                {movie.genres.map((genre => (<span key={genre.id}>{genre.name},</span>)))}
                                                | {hour}.
                                                 </span>
                                                {/*  */}
                                            </div>

                                            <div className="didv_rigth2">
                                                <span>VOCÊ GOSTOU DE {movie.name}?</span>
                                            </div>

                                            <div className="container_like">
                                                <i className={`far fa-thumbs-up like ${like && 'active'}`} onClick={() => toggleRating(movie, 1)}></i>
                                                <i className={`far fa-thumbs-down like ${deslike && 'active'}`} onClick={() => toggleRating(movie, -1)}></i>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    )}
                    {/* FAVORITOS */}

                    {
                        favoriteMovies && favoriteMovies.length > 0 && (
                            <section className="container-carrosel_slik">
                                <h4 style={{ textTransform: 'uppercase', fontWeight: 'normal' }}>Minha lista de filmes</h4>
                                <div className="container-my-carousel">
                                    <div ref={Scroll} className="my-carousel-sroll ">
                                        <div className="my-carousel container-category__content">
                                            {favoriteMovies.map(favoriteMovie => {
                                                if (!favoriteMovie.movie) return (<></>);
                                                let movie = favoriteMovie.movie;
                                                let date = new Date(`Thu, 01 Jan 1970 ${movie.duration}`);
                                                let hour = `${date.getHours()}h ${date.getMinutes() == 0 ? '' : date.getMinutes() + 'm'}`;
                                                let firstDate = new Date(movie.release)
                                                let favorite = (movie.users && movie.users.length > 0 && movie.users[0].favorite == true) ? true : false;
                                                let like = (movie.users && movie.users.length > 0 && movie.users[0].rating == '1') ? true : false;
                                                let deslike = (movie.users && movie.users.length > 0 && movie.users[0].rating == '-1') ? true : false;
                                                return (

                                                    <div key={movie.id} className="my-carousel-item conteudo_slik">
                                                        <div className="modal-card-traler_slik" >
                                                            <div className="aling__traler_slik">
                                                                {/* <iframe src={`https://player.vimeo.com/video/${movie.trailers.length > 0 ? movie.trailers[0].url : 0}?byline=0&portrait=0"`}  allow="autoplay; fullscreen; picture-in-picture"></iframe>  */}
                                                                <Vimeo
                                                                    video={movie.trailers.length > 0 ? movie.trailers[0].url : 0}
                                                                    // background={true}
                                                                    responsive={true}
                                                                    loop={true}
                                                                    muted={true}
                                                                    autoplay={false}
                                                                />
                                                            </div>
                                                            <Link to={`/descricao-do-filme/${movie.id}`} className="testez"></Link>
                                                            <div className="info_card_muvie_slik">
                                                                <div className="info_card_muvie__control_slik">
                                                                    <Link to={`/reproduzir/${movie.id}`}>
                                                                        <i className="fas fa-play"></i>
                                                                    </Link>
                                                                    <i className={`fas fa-${favorite ? 'check' : 'plus'}`} onClick={() => toggleFavorite(movie)}></i>
                                                                    <i className={`far fa-thumbs-up like ${like ? 'activeLike' : ''}`} onClick={() => toggleRating(movie, 1)}></i>
                                                                    <i className={`far fa-thumbs-down like ${deslike ? 'activeLike' : ''}`} onClick={() => toggleRating(movie, -1)}></i>
                                                                    <Link to={`/descricao-do-filme/${movie.id}`}>
                                                                        <i className="fas fa-chevron-down"></i>
                                                                    </Link>
                                                                </div>

                                                                <div className="info_card_muvie__bootom_slik" >
                                                                    <div className="info_card_muvie__itens_slik">
                                                                        {/* <div className="info_card_muvie__aproval">96% relevante</div>*/}
                                                                        <h4 style={{ color: '#1C1E29', margin: 0, textTransform: 'uppercase' }}>{movie.name}</h4>
                                                                        <div id="idicative-age" className="info_card_muvie__idicative-class_slik">
                                                                            {movie.parental_rating}
                                                                        </div>
                                                                        <div className="info_card_muvie_ano-lancamento_slik">
                                                                            {firstDate.getFullYear()}.
                                    </div>
                                                                        {movie.genres.map((genre => {
                                                                            return (
                                                                                <div className="genre-card_slik" key={genre.id}>{genre.name},</div>
                                                                            );
                                                                        }))}
                                                                        <div className="minutes-card_slik">| {hour}</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <img src={movie.thumbnail} alt="" />

                                                    </div>
                                                );
                                            })}


                                        </div>
                                    </div>
                                    <div onClick={rigthSlider} className="btn-my-carrosel-left">
                                        <button><i className="material-icons">chevron_left</i></button>
                                    </div>

                                    <div onClick={leftSlider} className="btn-my-carrosel-rigth">
                                        <button><i className="material-icons">navigate_next</i></button>
                                    </div>
                                </div>
                            </section>

                        )
                    }


                    {/* CATALOGO DE FILMES */}
                    {
                        categoryMovies.map(category => {
                            if (category.movies.length == 0) return (<></>);
                            return (
                                <section className="container-carrosel_slik" key={category.id}>
                                    <h4 style={{ textTransform: 'uppercase', fontWeight: 'normal' }}>{category.name}</h4>
                                    <MySlider>
                                        <>
                                            <div className="my-carousel">
                                                {category.movies.map(movie => {
                                                    let date = new Date(`Thu, 01 Jan 1970 ${movie.duration}`);
                                                    let hour = `${date.getHours()}h ${date.getMinutes() == 0 ? '' : date.getMinutes() + 'm'}`;
                                                    let firstDate = new Date(movie.release)
                                                    let favorite = (movie.users && movie.users.length > 0 && movie.users[0].favorite == true) ? true : false;
                                                    let like = (movie.users && movie.users.length > 0 && movie.users[0].rating == '1') ? true : false;
                                                    let deslike = (movie.users && movie.users.length > 0 && movie.users[0].rating == '-1') ? true : false;
                                                    return (
                                                        <div key={movie.id} className="my-carousel-item">
                                                            <div className="container-video-card">
                                                                <div className="aling-video-card">
                                                                    <Vimeo
                                                                        video={movie.trailers.length > 0 ? movie.trailers[0].url : 0}
                                                                        // background={true}
                                                                        responsive={true}
                                                                        loop={true}
                                                                        muted={true}
                                                                        autoplay={false}
                                                                    />
                                                                </div>

                                                                <div className="card-info-n">
                                                                    <div className="control-card-movie">
                                                                        <Link to={`/reproduzir/${movie.id}`}>
                                                                            <i className="fas fa-play"></i>
                                                                        </Link>
                                                                        <i className={`fas fa-${favorite ? 'check' : 'plus'}`} onClick={() => toggleFavorite(movie)}></i>
                                                                        <i className={`far fa-thumbs-up like ${like ? 'activeLike' : ''}`} onClick={() => toggleRating(movie, 1)}></i>
                                                                        <i className={`far fa-thumbs-down like ${deslike ? 'activeLike' : ''}`} onClick={() => toggleRating(movie, -1)}></i>
                                                                        <Link to={`/descricao-do-filme/${movie.id}`}>
                                                                            <i className="fas fa-chevron-down"></i>
                                                                        </Link>
                                                                    </div>
                                                                    <div className="info_card_muvie__bootom_slik" >
                                                                        <div className="info_card_muvie__itens_slik">
                                                                            {/* <div className="info_card_muvie__aproval">96% relevante</div>*/}
                                                                            <h4 style={{ color: '#1C1E29', margin: 0, textTransform: 'uppercase' }}>{movie.name}</h4>
                                                                            <div id="idicative-age" className="info_card_muvie__idicative-class_slik">
                                                                                {movie.parental_rating}
                                                                            </div>
                                                                            <div className="info_card_muvie_ano-lancamento_slik">
                                                                                {firstDate.getFullYear()}
                                                                            </div>
                                                                            {movie.genres.map((genre => {
                                                                                return (
                                                                                    <div className="genre-card_slik" key={genre.id}>{genre.name},</div>
                                                                                );
                                                                            }))}
                                                                            <div className="minutes-card_slik">| {hour}</div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <img src={movie.thumbnail} alt="" />
                                                        </div>
                                                    );
                                                })}
                                            </div>
                                            <div className="fim-carrosel"></div>
                                        </>
                                    </MySlider>
                                </section>
                            )
                        })
                    }
                    {/* testes */}
                </>
            ) : (<p>Aguardando pagamento</p>)}
            <div className="espaco"></div>
            <div className="pop-up-video">
                <div onClick={closePopUp} className="close-pop-up">X Clique aqui para sair.</div>
                <div className="pop-up-video__content">
                    <Vimeo
                        video={530765573}
                        responsive={true}
                        loop={false}
                        muted={false}
                        autoplay={false}
                    />
                </div>
            </div>
            {/* LOAD PAGE */}
            <div className="on-load-page">
                <LoadPage />
            </div>
            <Footer />
        </div>
    );
}
export default Home;
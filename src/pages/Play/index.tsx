import React, { useContext, useEffect, useState }  from 'react';
import { Link, useHistory, useParams } from 'react-router-dom';
import api from '../../services/api';
import Vimeo from '@u-wave/react-vimeo';
import { GeneralContext } from '../../contexts/GeneralContext';
interface PlayParams{ id?: string; }
interface Movie{
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    category?: string;
    url: string;
    trailer_url?: string;
    duration: string;
    approval: string;
    cost?: string;
    thumbnail: string;
    wallpaper: string;
}
function Play() {
    const params = useParams<PlayParams>();
    const [movie,setMovie] = useState<Movie>();
    const history = useHistory();
    const {setOpen, setMessage} = useContext(GeneralContext);
    const [idVideo,setIdVideo] = useState(0);

    const [access_token,setAccessToken] = useState("");
    useEffect(() => {
        if (sessionStorage.getItem('magnificat_access') == null) history.push('/');
        else setAccessToken(sessionStorage.getItem('magnificat_access') || "");

        window.scrollTo(0, 0);

       
    }, []);


    useEffect(() => {
        if(access_token != "" && params.id){
            getMovie();
        }
    },[params.id,access_token]);
    useEffect(() => {
        if(movie) setIdVideo(Number(movie.url));
    },[movie]);

   
    function goBack() {
        history.push(`/descricao-do-filme/${params.id}`);
    }
    function getMovie(){
        api.get(`auth/getMovieById/${params.id}`,{ headers: {"Authorization" : `Bearer ${access_token}`} })
           .then(response => {
            if(response.data.result){
                setMovie(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function message(text: string){
        setMessage(text);
        setOpen(true);
    }
    return (
        <div id="ContainerPlay">
            <div className="top-card">
            <button className="btn-voltar" type="button" onClick={goBack}><i className="fas fa-arrow-left"></i>Voltar</button>
            </div>
            
            <div className="containerVideo">
                {(movie && idVideo!=0) && (
                <iframe src={`https://player.vimeo.com/video/${idVideo}`}   allow="autoplay; fullscreen; picture-in-picture"></iframe>
               
                    // <Vimeo
                    //     video= {idVideo}
                    //     autoplay={true}
                    //     responsive = {true}
                    // />
               
                )}
            </div>
        </div>
    );
}

export default Play;
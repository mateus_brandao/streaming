import React, { useEffect } from "react";
import { Link } from 'react-router-dom';

function ChamgePlan() {
    return (
        <>
            <div className="change-plan">
                <h3>Altere seu plano atual</h3>
                <p>
                    Selecione a melho opção a baixo.
                </p>

                <div id="table_grid">
                    <div className="table_grid-th border-table-rigth table-c1">
                        <div className="table_grid-td"></div>
                        <div className="table_grid-td border-table-bottom">Preço por mês</div>
                        <div className="table_grid-td border-table-bottom">Qualidade do video</div>
                        <div className="table_grid-td border-table-bottom">Resolução</div>
                        <div className="table_grid-td border-table-bottom">Telas simultâneas</div>
                        <div className="table_grid-td border-table-bottom">Assistir em PC, <br />celular, tablet e TV </div>

                        <div className="table_grid-td border-table-bottom"> Conteúdos simultâneos</div>
                        <div className="table_grid-td">Cancele quando quiser</div>
                    </div>
                    <div className="table_grid-th border-table-rigth">
                        <div className="table_grid-td th-destaque">Básico</div>
                        <div className="table_grid-td border-table-bottom">R$xx,xx</div>
                        <div className="table_grid-td border-table-bottom">Boa</div>
                        <div className="table_grid-td border-table-bottom">480p</div>
                        <div className="table_grid-td border-table-bottom">1</div>
                        <div className="table_grid-td border-table-bottom">sim</div>
                        <div className="table_grid-td border-table-bottom">Sim</div>
                        <div className="table_grid-td">sim</div>
                    </div>
                    <div className="table_grid-th border-table-rigth">
                        <div className="table_grid-td th-destaque"> Padrão</div>
                        <div className="table_grid-td border-table-bottom">
                            <span className="th_mobile">Preço por mês</span> R$xx,xx
                                        </div>
                        <div className="table_grid-td border-table-bottom">
                            <span className="th_mobile">Qualidade do video</span> Melhor
                                         </div>
                        <div className="table_grid-td border-table-bottom">
                            <span className="th_mobile">Resolução</span> 1080p
                                          </div>
                        <div className="table_grid-td border-table-bottom">
                            <span className="th_mobile">Telas simultâneas</span> 2
                                         </div>
                        <div className="table_grid-td border-table-bottom">
                            <span className="th_mobile">Assistir em PC, celular, tablet e TV</span> Sim
                                         </div>
                        <div className="table_grid-td border-table-bottom">
                            <span className="th_mobile">Conteúdos simultâneos</span> Sim
                                         </div>
                        <div className="table_grid-td">
                            <span className="th_mobile">Cancele quando quiser</span> Sim
                                         </div>
                    </div>
                    <div className="table_grid-th">
                        <div className="table_grid-td th-destaque"> Premium</div>
                        <div className="table_grid-td border-table-bottom">R$xx,xx</div>
                        <div className="table_grid-td border-table-bottom">Superior</div>
                        <div className="table_grid-td border-table-bottom">4K + HD</div>
                        <div className="table_grid-td border-table-bottom">4</div>
                        <div className="table_grid-td border-table-bottom">Sim</div>
                        <div className="table_grid-td border-table-bottom">Sim</div>
                        <div className="table_grid-td">Sim</div>
                    </div>
                    <div className="table-c1"></div>
                    <div className="btn-p">
                        <button type="button"
                            className="btn-p2">
                            <span>Escolha</span>
                        </button>
                    </div>
                    <div className="btn-p">
                        <button type="button"
                            className="btn-p2">
                            <span>Escolha</span>
                        </button>
                    </div>
                    <div className="btn-p">
                        <button type="button"
                            className="btn-p2">
                            <span>Escolha</span>
                        </button>
                    </div>


                </div>
                <div className="alignRecordButton">

                    <div className="container_btn">
                        <Link to="/painel-de-controle" className="btn-v">
                            <span>Anterior</span>
                        </Link>
                        <button type="button" className="btn">
                            <span>Alterar</span>
                        </button>

                    </div>
                </div>
            </div>


        </>
    );
}

export default ChamgePlan;
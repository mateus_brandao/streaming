import React, { ChangeEvent, useContext, useEffect, useState } from 'react';
import api from "../../services/api";
import BtnBorderBlue from '../../components/Buttons/Button-border-blue';
import ButtonIcon from '../../components/Buttons/Button-icon';
import BtnRed from '../../components/Buttons/Button-red';
import Header from '../../components/Dash/Header';
import { Link, useHistory } from 'react-router-dom';
import { GeneralContext } from '../../contexts/GeneralContext';


interface User {
    first_name: string;
    last_name: string;
    email: string;
    permission: string;
}
interface Category {
    id: number;
    name: string;
}
interface Genre {
    id: number;
    name: string;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    category?: string;
    genre?: string;
    url: string;
    trailer_url?: string;
    trailer_name?: string;
    trailer_thumbnail?: string;
    duration: string;
    approval: string;
    cost?: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface Extra {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}


function RegisteredMovies() {
    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'provider' });
    const [approveMovies, setApproveMovies] = useState<Movie[]>([]);
    const [listMoviesBlock, setListMoviesBlock] = useState<Movie[]>([]);
    const [movies, setMovies] = useState<Movie[]>([]);
    const { setOpen, setMessage } = useContext(GeneralContext);
    // BEGIN:: REGISTER MOVIE
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [sinopse, setSinopse] = useState('');
    const [parentalRating, setParentalRating] = useState('');
    const [release, setRelease] = useState('');
    const [country, setCountry] = useState('');
    const [direction, setDirection] = useState('');
    const [production, setProduction] = useState('');
    const [script, setScript] = useState('');
    const [composition, setComposition] = useState('');
    const [cast, setCast] = useState('');
    const [category, setCategory] = useState<String[]>([]);
    const [genre, setGenre] = useState<String[]>([]);
    const [url, setUrl] = useState('');
    const [duration, setDuration] = useState('');
    const [cost, setCost] = useState('');
    const [expiration, setExpiration] = useState('');
    const [thumbnail, setThumbnail] = useState('');
    const [wallpaper, setWallpaper] = useState('');
    const [trailersUrl, setTrailersUrl] = useState('');
    const [trailersName, setTrailersName] = useState('');
    const [trailersThumbnail, setTrailersThumbnail] = useState('');
    const [extras, setExtras] = useState<Extra[]>([]);
    // END:: REGISTER MOVIE
    const [categories, setCategories] = useState<Category[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [newCategory, setNewCategory] = useState('');
    const [newGenre, setNewGenre] = useState('');
    const [validate, setValidate] = useState(false);
    // BEGIN:: GALLERY
    const [gallery, setGallery] = useState([]);
    const [galleryType, setGalleryType] = useState('thumbnail');
    const [uploadImages, setUploadImages] = useState<File[]>([]);
    // END:: GALLERY | BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    const history = useHistory();
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");

    }, []);
    useEffect(() => {
        if (access_token != "") {
            getCategories();
            getGenres();
            getApproveMovies();
            getMovies();
            getUser();
            getBlockedMovies();
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    useEffect(() => {
        if (validate) {
            storeMovie();
            setValidate(false);
        }
    }, [validate]);

    function navAbas(n: number) {
        let aba: any
        let qtdAba: number
        let current: number
        aba = document.querySelectorAll('#contral_abas li')
        current = n;
        qtdAba = aba.length;
        if (current === 0) {
            aba[0].classList.add("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 1) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.add("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 2) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.add("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 3) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.add("adm-active");
        }

    }
    // BEGIN:: HANDLE EXTRA
    function addExtra() {
        let lastId = 1;
        if (extras.length > 0) {
            lastId = extras[extras.length - 1].id + 1;
        }
        setExtras([...extras, {
            id: lastId,
            url: '',
            name: '',
            thumbnail: '',
        }]);
    }
    function removeExtra(id: number) {
        let tempExtras = extras;
        setExtras([]);
        tempExtras.splice(extras.findIndex(extra => extra.id === id), 1);
        setExtras(tempExtras);
    }
    function handleExtra(extraParams: Extra) {
        let tempExtras = extras;
        let index = extras.findIndex(extra => extra.id === extraParams.id);

        tempExtras[index] = extraParams;

        setExtras(tempExtras);
        // console.log(extras);
    }
    // ADD:: HANDLE EXTRA
    function resetAddMovie() {
        setName('');
        setDescription('');
        setSinopse('');
        setParentalRating('');
        setRelease('');
        setCountry('');
        setDirection('');
        setProduction('');
        setScript('');
        setComposition('');
        setCast('');
        setCategory([]);
        setGenre([]);
        setUrl('');
        setTrailersUrl('');
        setTrailersName('');
        setTrailersThumbnail('');
        setDuration('');
        setCost('');
        setExpiration('');
        setThumbnail('');
        setWallpaper('');
    }
    function validateMovie() {
        // console.log(genre, category);

        let error = [] as String[]

        if (name.length == 0) error.push("O título do filme é obrigatório");
        if (description.length == 0) error.push("A descrição do filme é obrigatória");
        if (sinopse.length == 0) error.push("A sinopse é obrigatória");
        if (parentalRating.length == 0) error.push("A classificação indicativa é obrigatória");
        if (release.length == 0) error.push("O ano de lançamento é obrigatório");
        if (country.length == 0) error.push("O país é obrigatório");
        if (direction.length == 0) error.push("O campo direção é obrigatório");
        if (production.length == 0) error.push("O campo produção é obrigatório");
        if (script.length == 0) error.push("O roteiro é obrigatório");
        if (composition.length == 0) error.push("A composição musical é obrigatória");
        if (cast.length == 0) error.push("O elenco é obrigatório");
        if (category.length == 0) error.push("A categoria é obrigatória");
        if (genre.length == 0) error.push("O gênero é obrigatório");
        if (url.length == 0) error.push("O campo 'Id filme vimeo' é obrigatório");
        if (thumbnail.length == 0) error.push("A thumbnail é o  brigatória");
        if (wallpaper.length == 0) error.push("A capa do filme é obrigatória");
        if (trailersUrl.length == 0) error.push("O campo 'Id treiler vimeo' é obrigatório");
        if (trailersName.length == 0) error.push("O título do trailer é obrigatório");
        if (trailersThumbnail.length == 0) error.push("A thumbnail do trailer é obrigatória");
        if (duration.length == 0) error.push("A duração do filme é obrigatória");
        if (expiration.length == 0) error.push("A data de expiração do filme é obrigatória");
        if (cost.length == 0) error.push("O preço é obrigatório");

        if (error.length > 0) {
            let text = "";
            error.forEach(msg => text += msg + "\n");
            message(text);
        }
        return error.length > 0 ? false : true;
    }
    function handleCheckCategoryAndGenre() {
        let checkCategories = document.getElementsByName('checkCategories') as NodeListOf<HTMLInputElement>;
        let arrCategories = [] as String[];
        Array.from(checkCategories).forEach(category => {
            if (category.checked) {
                arrCategories.push(String(category.getAttribute('value')));
                category.checked = false;
            }
        });
        setCategory(arrCategories);

        let checkGenres = document.getElementsByName('checkGenres') as NodeListOf<HTMLInputElement>;
        let arrGenres = [] as String[];
        Array.from(checkGenres).forEach(genre => {
            if (genre.checked) {
                arrGenres.push(String(genre.getAttribute('value')));
                genre.checked = false;
            }
        });
        setGenre(arrGenres);

        setValidate(true);
    }
    async function storeMovie() {
        if (validateMovie()) {
            const data = new FormData();

            data.append('name', name);
            data.append('description', description);
            data.append('sinopse', sinopse);
            data.append('parental_rating', parentalRating);
            data.append('release', release);
            data.append('country', country);
            data.append('direction', direction);
            data.append('production', production);
            data.append('script', script);
            data.append('composition', composition);
            data.append('cast', cast);
            data.append('cost', cost);
            category.forEach((item, index) => { data.append(`category[${index}]`, String(item)); });
            genre.forEach((item, index) => { data.append(`genre[${index}]`, String(item)); })
            data.append('url', url);
            data.append('trailer_url', trailersUrl);
            data.append('trailer_name', trailersName);
            data.append('trailer_thumbnail', trailersThumbnail);
            data.append('duration', duration);
            data.append('expiration', expiration);
            data.append('thumbnail', thumbnail);
            data.append('wallpaper', wallpaper);

            api.post('auth/registerMovie', data, { headers: { "Authorization": `Bearer ${access_token}` } })
                .then(response => {
                    if (response.data.result) {
                        setApproveMovies([...approveMovies, response.data.response]);
                        message('Filme cadastrado com sucesso');
                        resetAddMovie();
                    }
                    else message(response.data.response);
                });
        }
    }
    function storeCategory() {
        if (newCategory.length == 0) {
            message('Digite o nome da nova categoria');
            return;
        }
        const data = new FormData();

        data.append('name', newCategory);

        api.post('auth/registerCategory', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setCategories([...categories, response.data.response]);
                    message('Categoria cadastrada com sucesso');
                    setNewCategory('');
                }
                else message(response.data.response);
            });
    }
    function storeGenre() {
        if (newGenre.length == 0) {
            message('Digite o nome do novo gênero');
            return;
        }
        const data = new FormData();

        data.append('name', newGenre);

        api.post('auth/registerGenre', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setGenres([...genres, response.data.response]);
                    message('Gênero cadastrado com sucesso');
                    setNewGenre('');
                }
                else message(response.data.response);
            });
    }
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    // BEGIN: APPROVE MOVIE
    function approveMovie(id: number) {
        const data = new FormData();

        data.append('movie_id', String(id));
        api.post('auth/approveMovie', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('Filme Aprovado!');
                    getMovies();
                    getApproveMovies();
                }
                else message(response.data.response);
            });
    }
    function disapproveMovie(id: number) {
        const data = new FormData();

        data.append('movie_id', String(id));
        api.post('auth/disapproveMovie', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('Filme Reprovado!');
                    getMovies();
                    getApproveMovies();
                }
                else message(response.data.response);
            });
    }
    // END: APPROVE MOVIE | BEGIN:: DELETE MOVIE / CATEGORY / GENRES
    function deleteMovie(id: number) {
        api.get(`auth/deleteMovie/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Filme Excluído!');
                getMovies();
            }
            else message(response.data.response);
        });
    }
    // BEGGIN:: BLOCK MOVIE
    async function blockMovie(id: number) {
        api.get(`auth/blockMovie/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('filme bloqueado com sucesso');
                    getMovies();
                }
                else message(response.data.response);
            })
    }
    // END:: BLOCK MOVIE
    // BEGGIN:: list blocked movies
    async function getBlockedMovies() {
        api.get('auth/indexMovieBlocked', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setListMoviesBlock(response.data.response)
                    console.log('teste', response.data.response)
                }
                else message(response.data.response)
            })
    }
    // END:: list blocked movies
    // BEGGIN:: unlock movie
    async function unlockMovie(id: number) {
        api.get(`auth/getRestoreMovieById/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } } )
        .then(response => {
            if (response.data.result){
                message('filme desbloqueado com sucesso');
                getMovies();
                
            }
            else message(response.data.response);
        })
    }
    // END:: unlock movie

    function deleteApproveMovie(id: number) {
        api.get(`auth/deleteApproveMovie/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Filme Excluído!');
                getApproveMovies();
            }
            else message(response.data.response);
        });
    }
    function deleteCategory(id: number) {
        api.get(`auth/deleteCategory/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Categoria Excluída!');
                getCategories();
            }
            else message(response.data.response);
        });
    }
    function deleteGenre(id: number) {
        api.get(`auth/deleteGenre/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Categoria Excluída!');
                getGenres();
            }
            else message(response.data.response);
        });
    }
    // BEGGIN:: GETTERS API
    function getCategories() {
        api.get('auth/indexCategory', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setCategories(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getGenres() {
        api.get('auth/indexGenre', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setGenres(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getApproveMovies() {
        api.get('auth/movieWaitingApproval', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { setApproveMovies(response.data.response) }
                else message(response.data.response);
            });
    }
    function getMovies() {
        api.get('auth/indexMovie', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setMovies(response.data.response)
                    console.log('teste', response.data.response )
                }
                else message(response.data.response);
            });
    }
    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
            }
        });
    }
    function getGallery(type: string) {
        api.get(`auth/getGallery/${type}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) setGallery(response.data.response);
        });
    }
    // END:: GETTERS API
    // BEGGIN: GALLERY 
    let modalPayment: any;
    function openModalGallery(type: string) {
        getGallery(type);
        setGalleryType(type);
        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "100%";
        modalPayment.style.opacity = "1";
        modalPayment.style.display = "block";
    }
    function closeModalGallery() {
        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "0";
        modalPayment.style.opacity = "0";
        modalPayment.style.display = "none";
        setGallery([]);
    }
    function selectedImage(image_url: string) {
        switch (galleryType) {
            case 'thumbnail': setThumbnail(image_url); break;
            case 'wallpaper': setWallpaper(image_url); break;
            case 'horizontal-thumbnail': setTrailersThumbnail(image_url); break;
            default: message('Galeria inválida'); break;
        }
        closeModalGallery();
    }
    function handleSelectImages(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) return;
        const selectedImages = Array.from(event.target.files);
        setUploadImages(selectedImages);
    }
    function submitUploadImages() {
        const data = new FormData();

        data.append('type', galleryType);
        uploadImages.forEach((image, index) => {
            data.append(`images[${index}]`, image);
        });

        api.post('auth/uploadImages', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message(response.data.response);
                    closeModalGallery();
                    openModalGallery(galleryType);
                }
                else message(response.data.response);
            });
    }
    return (
        <div id="container__movie-aprove">
            < Header />

            <div className="salutation__catalogo">
                <span className="blue-left-border">Filmes cadastrados</span>
                <Link className="salutation__catalogo__go-back" to="/painel-de-controle-adm">Voltar</Link>
            </div>
            <div className="approval-condition">
                <h4>AGUARDANDO APROVAÇÃO</h4>
                {approveMovies.length == 0 && (<p>Você não tem nenhum filme aguardando aprovação</p>)}
            </div>

            <div className="container-muvie-aprove">
                {approveMovies.map(movie => {
                    return (
                        <div key={movie.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${movie.thumbnail})`, backgroundSize: 'cover' }} to={`/adicionar-filme/${movie.id}`}>
                                <div className="modal-muvie-title">
                                    {movie.name}

                                </div>
                                <div className="modal-muvie-info">
                                    <div className="modal-indicate">{movie.parental_rating}</div>
                                    <div className="muvie-time">{movie.duration}</div>

                                </div>
                            </Link>
                            <div className="indicate-aprove">
                                <p style={{ textAlign: 'center', marginTop: '.4rem' }}>
                                    {movie.approval == 'approved' && (<span>APROVADO</span>)}
                                    {movie.approval == 'disapproved' && (<span>REPROVADO</span>)}
                                    {movie.approval == 'pending' && (<span>PENDENTE</span>)}
                                </p>
                            </div>
                            {/*Botao */}
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    {/* <ButtonIcon type="button" icon="fas fa-film"
                                        onClick={() => history.push(`/descricao-do-filme/${movie.id}`)} /> */}
                                    <ButtonIcon type="button" icon="far fa-thumbs-up"
                                        onClick={() => approveMovie(movie.id)} />
                                    <ButtonIcon type="button" icon="far fa-thumbs-down" danger
                                        onClick={() => disapproveMovie(movie.id)} />
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deleteApproveMovie(movie.id)} />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>
            <div className="approval-condition" style={{ marginTop: '1.4rem', paddingTop: '.4rem', borderTop: '1px solid #dcdcdc' }}>
                <h4>APROVADOS</h4>
                {movies.length == 0 && (<p>Você não tem nenhum filme aprovado</p>)}
            </div>
            <div className="container-muvie-aprove">
                {movies.map(movie => {
                    return (
                        <div key={movie.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${movie.thumbnail})`, backgroundSize: 'cover' }} to={`/adicionar-filme/-${movie.id}`}>
                                <div className="modal-muvie-title">
                                    {movie.name}
                                </div>
                                <div className="modal-muvie-info">
                                    <div className="modal-indicate">{movie.parental_rating}</div>                                        <div className="muvie-time">{movie.duration}</div>
                                </div>
                            </Link>
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    <ButtonIcon type="button" icon="fas fa-film"
                                        onClick={() => history.push(`/descricao-do-filme/${movie.id}`)} />
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deleteMovie(movie.id)} />
                                    <button className="btn-block-movie" onClick={() => blockMovie(movie.id)}>
                                        <i className="material-icons">block</i>
                                    </button>
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>


            <div className="approval-condition" style={{ marginTop: '1.4rem', paddingTop: '.4rem', borderTop: '1px solid #dcdcdc' }}>
                <h4>Filmes bloqueados</h4>
            </div>
            <div className="container-muvie-aprove">
                {/* copia */}
                {listMoviesBlock.map(movie => {
                    return (
                        <div key={movie.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${movie.thumbnail})`, backgroundSize: 'cover' }} to={`/adicionar-filme/-${movie.id}`}>
                                <div className="modal-muvie-title">
                                    {movie.name}
                                </div>
                                <div className="modal-muvie-info">
                                    <div className="modal-indicate">{movie.parental_rating}</div>                                        <div className="muvie-time">{movie.duration}</div>
                                </div>
                            </Link>
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    <ButtonIcon type="button" icon="fas fa-film"
                                        onClick={() => history.push(`/descricao-do-filme/${movie.id}`)} />
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deleteMovie(movie.id)} />
                                    <button className="unlock-movie" onClick={() => unlockMovie(movie.id) }>
                                        <i className="material-icons">reply</i>
                                    </button>
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>

        </div>



    );
}

export default RegisteredMovies;
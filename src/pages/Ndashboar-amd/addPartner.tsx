import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/Dash/Header';


function addPartner() {
    let Password: string
    function generatePassword  ()  {

        Password = 'Kjkszpj'
    }
    return (
        <div id="add-partner">
            <Header />
            <div className="contente-partner">
                <span>Adicionar parceiro.</span> <br />

                    <select className="select-user-level">
                        <option value="">Selecione nível de usuário.</option>
                        <option value="Parceiro">Parceiro/ produtora.</option>
                        <option value="Adm">Administrador</option>
                    </select>
                <section className="container-user-data">
                    <div className="user-data">
                        <div>
                            <label htmlFor="userNamePartiner">Nome do usuário</label>
                            <input type="text" name="userNamePartiner" id="userNamePartiner" />
                        </div>
                        <div>
                            <label htmlFor="userMailPartiner">Email</label>
                            <input type="email" name="userMailPartiner" id="userMailPartiner" />
                        </div>
                        <div>
                            <label htmlFor="passwordUserPartiner">Senha</label>
                            <input  type="password" name="passwordUserPartiner" id="passwordUserPartiner" />
                        </div>
                    </div>
                </section>
                <div className="group-btn-user-partiner">
                    <div className="border-gradient">
                    <Link to="/painel-de-controle-adm" className="btn-partiner-wite">Cancelar</Link>
                    </div>
                    <button className="btn-partiner-blue" type="button">Adicionar parceiro</button>
                </div>
            </div>
        </div>
    );
}

export default addPartner;
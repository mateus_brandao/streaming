import React, { ChangeEvent, useContext, useEffect, useState } from 'react';
import Header from '../../components/Dash/Header';
import BtnBorderBlue from '../../components/Buttons/Button-border-blue';
import BtnRed from '../../components/Buttons/Button-red';
import api from "../../services/api";
import { Link } from 'react-router-dom';
import { GeneralContext } from '../../contexts/GeneralContext';


interface User {
    first_name: string;
    last_name: string;
    email: string;
    permission: string;
}
interface Category {
    id: number;
    name: string;
}
interface Genre {
    id: number;
    name: string;
}
interface RecordCompanies{
    id: number;
    name: string;
}
interface Artists{
    id: number;
    name: string;
}
interface Albums{
    id: number;
    name: string;
    type: 'single' | 'album' | 'serie' | '';
}

function ConfigCatalogo() {
    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'provider' });
    const [sectionActive,setSectionActive] = useState<'movie'|'podcast'|'ebook'|'serie'|'music'>('movie');
    // BEGIN:: MOVIE
    const { setOpen, setMessage } = useContext(GeneralContext);
    const [categories, setCategories] = useState<Category[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [newCategory, setNewCategory] = useState('');
    const [newGenre, setNewGenre] = useState('');
    // END:: MOVIE | BEGIN:: PODCAST
    const [podcastCategories, setPodcastCategories] = useState<Category[]>([]);
    const [podcastGenres, setPodcastGenres] = useState<Genre[]>([]);
    const [recordCompanies, setRecordCompanies] = useState<RecordCompanies[]>([]);
    const [artists, setArtists] = useState<Artists[]>([]);
    const [albums, setAlbums] = useState<Albums[]>([]);
    const [newRecordCompany, setNewRecordCompany] = useState('');
    const [newArtist, setNewArtist] = useState('');
    const [newAlbum, setNewAlbum] = useState('');
    const [newAlbumType, setNewAlbumType] = useState('');
    // END:: PODCAST | BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");

    }, []);
    useEffect(() => {
        if (access_token != "") {
            getCategories();
            getGenres();
            getCategories('podcast');
            getGenres('podcast');
            getRecordCompanies();
            getArtists();
            getAlbums();
            getUser();
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN

    // BEGIN:: FUNCTIONS FOR MOVIES
    function storeCategory(type = 'movie') {
        if (newCategory.length == 0) {
            message('Digite o nome da nova categoria');
            return;
        }
        const data = new FormData();

        data.append('name', newCategory);
        data.append('type', type);

        api.post('auth/registerCategory', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    if(type == 'movie') setCategories([...categories, response.data.response]);
                    else if(type == 'podcast')
                        setPodcastCategories([...podcastCategories, response.data.response]);

                    message('Categoria cadastrada com sucesso');
                    setNewCategory('');
                }
                else message(response.data.response);
            });
    }
    function storeGenre(type = 'movie') {
        if (newGenre.length == 0) {
            message('Digite o nome do novo gênero');
            return;
        }
        const data = new FormData();

        data.append('name', newGenre);
        data.append('type', type);

        api.post('auth/registerGenre', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    if(type == 'movie') setGenres([...genres, response.data.response]);
                    else if(type == 'podcast') 
                        setPodcastGenres([...podcastGenres, response.data.response]);

                    message('Gênero cadastrado com sucesso');
                    setNewGenre('');
                }
                else message(response.data.response);
            });
    }
    function deleteCategory(id: number) {
        api.get(`auth/deleteCategory/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Categoria Excluída!');
                getCategories();
            }
            else message(response.data.response);
        });
    }
    function deleteGenre(id: number) {
        api.get(`auth/deleteGenre/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Gênero Excluído!');
                getGenres();
            }
            else message(response.data.response);
        });
    }
    // END:: FUNCTIONS FOR PODCAST | BEGIN:: FUNCTIONS FOR PODCAST
    function storeRecordCompany() {
        if (newRecordCompany.length == 0) {
            message('Digite o nome da nova gravador');
            return;
        }
        const data = new FormData();

        data.append('name', newRecordCompany);

        api.post('auth/registerRecordCompany', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setRecordCompanies([...recordCompanies, response.data.response]);
                    message('Categoria cadastrada com sucesso');
                    setNewRecordCompany('');
                }
                else message(response.data.response);
            });
    }
    function storeArtist() {
        if (newArtist.length == 0) {
            message('Digite o nome do novo gênero');
            return;
        }
        const data = new FormData();

        data.append('name', newArtist);

        api.post('auth/registerArtist', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setArtists([...artists, response.data.response]);
                    message('Artista cadastrado com sucesso');
                    setNewArtist('');
                }
                else message(response.data.response);
            });
    }
    function storeAlbum() {
        if (newAlbum.length == 0) {
            message('Digite o nome do novo Álbum/Série');
            return;
        }
        const data = new FormData();

        data.append('name', newAlbum);
        data.append('type', newAlbumType);

        api.post('auth/registerAlbum', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setAlbums([...albums, response.data.response]);
                    message('Álbum cadastrado com sucesso');
                    setNewAlbum('');
                    setNewAlbumType('');
                }
                else message(response.data.response);
            });
    }
    // END:: FUNCTIONS FOR PODCAST | BEGGIN:: GETTERS API
    function getCategories(type = 'movie') {
        api.get(`auth/indexCategory/${type}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                if(type == 'movie') setCategories(response.data.response);
                else if(type == 'podcast') setPodcastCategories(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getGenres(type = 'movie') {
        api.get(`auth/indexGenre/${type}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                if(type == 'movie') setGenres(response.data.response);
                else if(type == 'podcast') setPodcastGenres(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getRecordCompanies() {
        api.get('auth/indexRecordCompany', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setRecordCompanies(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getArtists() {
        api.get('auth/indexArtist', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setArtists(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getAlbums() {
        api.get('auth/indexAlbum', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setAlbums(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
            }
        });
    }
    // END:: GETTERS API
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    return (
        <div id="conatainer-congfig-catalog">
            <Header />
            <div className="nav-config-catalogo">
                <ul>
                    <li style={sectionActive == 'movie' ? { 
                        backgroundColor: 'rgba(0,0,0,.2)',
                        borderRadius: '8px',
                        padding: '.2rem'
                    } : {padding: '.2rem'}}>
                        <a href="javascript:;" onClick={()=> setSectionActive('movie')}>Filmes</a>
                    </li>
                    <li style={sectionActive == 'podcast' ? { 
                        backgroundColor: 'rgba(0,0,0,.2)',
                        borderRadius: '8px',
                        padding: '.2rem'
                    } : {padding: '.2rem'}}>
                        <a href="javascript:;" onClick={()=> setSectionActive('podcast')}>PodCast</a>
                    </li>
                    <li style={sectionActive == 'ebook' ? { 
                        backgroundColor: 'rgba(0,0,0,.2)',
                        borderRadius: '8px',
                        padding: '.2rem'
                    } : {padding: '.2rem'}}>
                        <a href="javascript:;" onClick={()=> setSectionActive('ebook')}>Ebook</a>
                    </li>
                    <li style={sectionActive == 'serie' ? { 
                        backgroundColor: 'rgba(0,0,0,.2)',
                        borderRadius: '8px',
                        padding: '.2rem'
                    } : {padding: '.2rem'}}>
                        <a href="javascript:;" onClick={()=> setSectionActive('serie')}>Séries</a>
                    </li>
                    <li style={sectionActive == 'music' ? { 
                        backgroundColor: 'rgba(0,0,0,.2)',
                        borderRadius: '8px',
                        padding: '.2rem'
                    } : {padding: '.2rem'}}>
                        <a href="javascript:;" onClick={()=> setSectionActive('music')}>Músicas</a>
                    </li>
                </ul>
            </div>
            <div>
                <div className="saudation__config">
                    <div>
                        <span className="blue-left-border">Oi {user.first_name}</span><br />
                        {sectionActive == 'movie' ? (
                            <p>Aqui você pode adicionar gênero e categoria em seus filmes</p>
                        ):sectionActive == 'podcast'?(
                            <p>Aqui você pode adicionar gênero, categoria, artista, album e gravadora em seus podcasts</p>
                        ):sectionActive == 'ebook'?(
                            <p>Ebook está em desenvolvimento</p>
                        ):sectionActive == 'serie'?(
                            <p>Séries ainda estão em desenvolvimento</p>
                        ):sectionActive == 'music' && (
                            <p>Músicas ainda estão em desenvolvimento</p>
                        )}
                        
                    </div>
                    <Link to="/painel-de-controle-adm">Voltar</Link>
                </div>
                {user.permission == 'root' && sectionActive == 'movie' ? (
                    <div className='config-catalogo'>
                        <div className="config__genero-categoraia">
                            <strong>Gêneros</strong>
                            <div className="config__input-add">
                                <input type="text" name="new-genre" id="new-genre" placeholder="Adicionar Gênero" value={newGenre} onChange={event => setNewGenre(event.target.value)} />
                                <button type="button" name="Novo" onClick={() => storeGenre()}> Adicionar </button>
                            </div>

                            {genres.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name}</span>
                                        <button type="button" onClick={() => deleteGenre(item.id)}>Excluir</button>
                                    </div>
                                );
                            })}

                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Categorias</strong>

                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" value={newCategory} onChange={event => setNewCategory(event.target.value)} />
                                <button type="button" name="Novo" onClick={() => storeCategory()}>Adicionar</button>
                            </div>
                            {categories.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name}</span>
                                        <button type="button" onClick={() => deleteCategory(item.id)}>Excluir</button>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Direção</strong>
                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" />
                                <button type="button" name="Novo">Adicionar</button>
                            </div>
                            <div className="config__item" key=''>
                                <span>Item</span>
                                <button type="button" >Excluir</button>
                            </div>
                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Roteiro</strong>
                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" />
                                <button type="button" name="Novo">Adicionar</button>
                            </div>
                            <div className="config__item" key=''>
                                <span>Item</span>
                                <button type="button" >Excluir</button>
                            </div>
                        </div>
                    </div>
                ):sectionActive == 'podcast'?(
                    <div className='config-catalogo'>
                        <div className="config__genero-categoraia">
                            <strong>Gêneros</strong>
                            <div className="config__input-add">
                                <input type="text" name="new-genre" id="new-genre" placeholder="Adicionar Gênero" value={newGenre} onChange={event => setNewGenre(event.target.value)} />
                                <button type="button" name="Novo" onClick={() => storeGenre('podcast')}> Adicionar </button>
                            </div>

                            {podcastGenres.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name}</span>
                                        <button type="button" onClick={() => deleteGenre(item.id)}>Excluir</button>
                                    </div>
                                );
                            })}

                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Categorias</strong>

                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" value={newCategory} onChange={event => setNewCategory(event.target.value)} />
                                <button type="button" name="Novo" onClick={() => storeCategory('podcast')}>Adicionar</button>
                            </div>
                            {podcastCategories.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name}</span>
                                        <button type="button" onClick={() => deleteCategory(item.id)}>Excluir</button>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Gravadora</strong>

                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" value={newRecordCompany} onChange={event => setNewRecordCompany(event.target.value)} />
                                <button type="button" name="Novo" onClick={storeRecordCompany}>Adicionar</button>
                            </div>
                            {recordCompanies.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name}</span>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Artista</strong>

                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" value={newArtist} onChange={event => setNewArtist(event.target.value)} />
                                <button type="button" name="Novo" onClick={storeArtist}>Adicionar</button>
                            </div>
                            {artists.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name}</span>
                                    </div>
                                );
                            })}
                        </div>
                        <div className="config__genero-categoraia">
                            <strong>Álbum</strong>

                            <div className="config__input-add">
                                <input type="text" name="new-category" id="new-category" value={newAlbum} onChange={event => setNewAlbum(event.target.value)} />
                                <select name="album-type" id="album-type" 
                                    onChange={event => setNewAlbumType(event.target.value)} 
                                    value={newAlbumType}>
                                    <option value='serie'>Série</option>
                                    <option value='single'>Single</option>
                                    <option value='album'>Álbum</option>
                                </select>
                                <button type="button" name="Novo" onClick={storeAlbum}>Adicionar</button>
                            </div>
                            {albums.map(item => {
                                return (
                                    <div className="config__item" key={item.id}>
                                        <span>{item.name} | 
                                            <span style={{color: '#999', marginLeft: '.4rem'}}>
                                                {item.type == 'serie'? 'Série':
                                                item.type == 'single'? 'Single':
                                                item.type == 'album' && 'Álbum'}
                                            </span>
                                        </span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                ):sectionActive == 'ebook'?(
                    <p>Ebook está em desenvolvimento</p>
                ):sectionActive == 'serie'?(
                    <p>Séries ainda estão em desenvolvimento</p>
                ):sectionActive == 'music' && (
                    <p>Músicas ainda estão em desenvolvimento</p>
                )}
            </div>

        </div>
    );
}

export default ConfigCatalogo;
import React, { ChangeEvent, useContext, useEffect, useRef, useState } from 'react';
import api from "../../services/api";
import Header from '../../components/Dash/Header'
import { Link, useParams } from 'react-router-dom';
import { GeneralContext } from '../../contexts/GeneralContext';
import LoadSpin from '../../components/Load';
import ButtonIcon from '../../components/Buttons/Button-icon';
import { Category, Genre, Movie } from '../../types/typesdef';

interface Trailer {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}
interface Extra {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}
interface TrailerExtra {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
    file_thumbnail?: File | null;
    isFile?: boolean;
}
interface InputFromGallery {
    id: number;
    type: string;
}
interface MovieParams {
    id?: string;
}

function AddMovie() {
    const params = useParams<MovieParams>();

    const { setOpen, setMessage } = useContext(GeneralContext);
    // BEGIN:: REGISTER MOVIE
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [sinopse, setSinopse] = useState('');
    const [parentalRating, setParentalRating] = useState('');
    const [release, setRelease] = useState('');
    const [country, setCountry] = useState('');
    const [direction, setDirection] = useState('');
    const [production, setProduction] = useState('');
    const [script, setScript] = useState('');
    const [composition, setComposition] = useState('');
    const [cast, setCast] = useState('');
    const [category, setCategory] = useState<String[]>([]);
    const [genre, setGenre] = useState<String[]>([]);
    const [url, setUrl] = useState('');
    const [duration, setDuration] = useState('');
    const [cost, setCost] = useState('');
    const [expiration, setExpiration] = useState('');
    const [freeable, setFreeable] = useState(false);
    const [trailers, setTrailers] = useState<TrailerExtra[]>([getTrailerDefaut(true)]);
    const [extras, setExtras] = useState<TrailerExtra[]>();
    
    const [thumbnail, setThumbnail] = useState('');
    const inputThumbnail = useRef<HTMLInputElement>(null);
    const [activeFileThumbnail, setActiveFileThumbnail] = useState(true);
    const [fileThumbnail, setFileThumbnail] = useState<File | null>(null);

    const [wallpaper, setWallpaper] = useState('');
    const inputWallpaper = useRef<HTMLInputElement>(null);
    const [activeFileWallpaper, setActiveFileWallpaper] = useState(true);
    const [fileWallpaper, setFileWallpaper] = useState<File | null>(null);
    // END:: REGISTER MOVIE
    const [categories, setCategories] = useState<Category[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [validate, setValidate] = useState(false);
    // BEGIN:: GALLERY
    const [gallery, setGallery] = useState([]);
    const [galleryType, setGalleryType] = useState('thumbnail');
    const [inputFromGallery, setInputFromGallery] = useState<InputFromGallery | null>(null);
    const [uploadImages, setUploadImages] = useState<File[]>([]);
    // END:: GALLERY | BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getCategories();
            getGenres();
            if (params.id) {
                if (Number(params.id) > 0) getApproveMovieById(params.id);
                else getMovieById(String(Number(params.id) * -1));
            }
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    useEffect(() => {
        if (validate) {
            storeMovie();
            setValidate(false);
        }
    }, [validate]);

    // BEGIN:: HANDLE INPUT FILE
    function handleToggleInputFile(param: string){
        switch(param){
            case 'thumbnail':
                setActiveFileThumbnail(!activeFileThumbnail);
                setFileThumbnail(null);
                setThumbnail('');
                break;
            case 'wallpaper':
                setActiveFileWallpaper(!activeFileWallpaper);
                setFileWallpaper(null);
                setWallpaper('');
                break;
        }
    }
    // END:: HANDLE INPUT FILE | BEGIN:: HANDLE TRAILER
    function getTrailerDefaut(init = false) {
        let id = 0;

        if (init) id = 1;
        else id = trailers.length + 1;

        return {
            id: id,
            url: '',
            name: '',
            thumbnail: '',
            file_thumbnail: null,
            isFile: true,
        };
    }
    function handleSetTrailer(value: string, column: string, id: number, file?: File) {
        let temp = trailers;
        let index = trailers.findIndex(trailer => trailer.id == id);
        switch (column) {
            case 'url': temp[index].url = value; break;
            case 'name': temp[index].name = value; break;
            case 'thumbnail': temp[index].thumbnail = value; break;
            case 'file_thumbnail': temp[index].file_thumbnail = file; break;
            case 'isFile': temp[index].isFile = !temp[index].isFile; break;
        }
        setTrailers([...temp]);
    }
    function AddTrailer() {
        setTrailers([...trailers, getTrailerDefaut()]);
    }
    function RemoveTrailer() {
        if (trailers.length > 1) {
            let temp = trailers;
            temp.pop();
            setTrailers([...temp]);
        } else {
            message('Obrigatório o cadastro de, no minimo, 1 trailer')
        }
    }
    // END:: TRAILER | BEGIN:: HANDLE EXTRAS
    function getExtraDefaut(init = false) {
        let id = 0;

        if (init || !extras) id = 1;
        else id = extras.length + 1;

        return {
            id: id,
            url: '',
            name: '',
            thumbnail: '',
            file_thumbnail: null,
            isFile: true,
        };
    }
    function handleSetExtra(value: string, column: string, id: number, file?: File) {
        if (extras) {
            let temp = extras;
            let index = extras.findIndex(extra => extra.id == id);
            switch (column) {
                case 'url': temp[index].url = value; break;
                case 'name': temp[index].name = value; break;
                case 'thumbnail': temp[index].thumbnail = value; break;
                case 'file_thumbnail': temp[index].file_thumbnail = file; break;
                case 'isFile': temp[index].isFile = !temp[index].isFile; break;
            }
            setExtras([...temp]);
        }
    }
    function AddExtra() {
        if (extras) setExtras([...extras, getExtraDefaut()]);
        else setExtras([...[getExtraDefaut()]]);
    }
    function RemoveExtra() {
        if (extras && extras.length > 0) {
            let temp = extras;
            temp.pop();
            setExtras([...temp]);
        }
    }
    // END:: HANDLE EXTRAS | BEGIN:: SEND FORM
    function resetAddMovie() {
        setName('');
        setDescription('');
        setSinopse('');
        setParentalRating('');
        setRelease('');
        setCountry('');
        setDirection('');
        setProduction('');
        setScript('');
        setComposition('');
        setCast('');

        setCategory([]);
        let checkCategories = document.getElementsByName('checkCategories') as NodeListOf<HTMLInputElement>;
        Array.from(checkCategories).forEach(category => category.checked = false);

        setGenre([]);
        let checkGenres = document.getElementsByName('checkGenres') as NodeListOf<HTMLInputElement>;
        Array.from(checkGenres).forEach(genre => genre.checked = false);

        setUrl('');
        setTrailers([getTrailerDefaut(true)]);
        setExtras([]);
        setDuration('');
        setCost('');
        setExpiration('');
        setFreeable(false);

        setThumbnail('');
        setActiveFileThumbnail(true);
        setFileThumbnail(null);
        if(inputThumbnail.current) inputThumbnail.current.value = '';
        setWallpaper('');
        setActiveFileWallpaper(true);
        setFileWallpaper(null);
        if(inputWallpaper.current) inputWallpaper.current.value = '';
    }
    function fillAddMovie(movie: Movie) {
        if (!movie || !movie.name) {
            message('Houve um erro inesperado ao selecionar o filme');
            return false;
        }
        setName(movie.name);
        setDescription(movie.description);
        setSinopse(movie.sinopse);
        setParentalRating(movie.parental_rating);
        setRelease(movie.release);
        setCountry(movie.country);
        setDirection(movie.direction);
        setProduction(movie.production);
        setScript(movie.script);
        setComposition(movie.composition);
        setCast(movie.cast);
        setUrl(movie.url);

        movie.categories.forEach(category => {
            let element = document.getElementById(`checkCategory-${category.id}`) as any;
            if (element) element.checked = true;
        });
        movie.genres.forEach(genre => {
            let element = document.getElementById(`checkGenre-${genre.id}`) as any;
            if (element) element.checked = true;
        });

        if (Number(params.id) > 0) {
            if (movie.approve_trailers) setTrailers(movie.approve_trailers);
            if (movie.approve_extras) setExtras(movie.approve_extras);
        } else {
            if (movie.trailers) setTrailers(movie.trailers);
            if (movie.extras) setExtras(movie.approve_extras);
        }

        setDuration(movie.duration);
        setCost(movie.cost || '0');
        setExpiration(movie.expiration);
        setFreeable(movie.freeable);
        
        setThumbnail(movie.thumbnail);
        setActiveFileThumbnail(false);
        setWallpaper(movie.wallpaper);
        setActiveFileWallpaper(false);
    }
    function validateMovie() {
        let error = [] as String[]

        if (name.length == 0) error.push("O título do filme é obrigatório");
        if (description.length == 0) error.push("A descrição do filme é obrigatória");
        if (sinopse.length == 0) error.push("A sinopse é obrigatória");
        if (parentalRating.length == 0) error.push("A classificação indicativa é obrigatória");
        if (release.length == 0) error.push("O ano de lançamento é obrigatório");
        if (country.length == 0) error.push("O país é obrigatório");
        if (direction.length == 0) error.push("O campo direção é obrigatório");
        if (production.length == 0) error.push("O campo produção é obrigatório");
        if (script.length == 0) error.push("O roteiro é obrigatório");
        if (composition.length == 0) error.push("A composição musical é obrigatória");
        if (cast.length == 0) error.push("O elenco é obrigatório");
        if (category.length == 0) error.push("A categoria é obrigatória");
        if (genre.length == 0) error.push("O gênero é obrigatório");
        if (url.length == 0) error.push("O campo 'Id filme vimeo' é obrigatório");

        if(activeFileThumbnail){
            if(!(inputThumbnail.current && inputThumbnail.current.value.length > 0)) error.push("A thumbnail é obrigatório");
        }
        else if (thumbnail.length == 0) error.push("A thumbnail é obrigatória");
        if(activeFileWallpaper){
            if(!(inputWallpaper.current && inputWallpaper.current.value.length > 0)) error.push("A capa do filme é obrigatória");
        }
        else if (wallpaper.length == 0) error.push("A capa do filme é obrigatória");

        trailers.forEach((trailer, index) => {
            if (trailer.url.length == 0) error.push(`O campo 'Id trailer vimeo' é obrigatório (${index}º Trailer)`);
            if (trailer.name.length == 0) error.push(`O título do trailer é obrigatório (${index}º Trailer)`);
            if (trailer.isFile){ 
                if (!trailer.file_thumbnail) error.push(`A thumbnail do trailer é obrigatória (${index}º Trailer)`);
            }
            else if (trailer.thumbnail.length == 0) error.push(`A thumbnail do trailer é obrigatória (${index}º Trailer)`);
        });
        if (extras) extras.forEach((extra, index) => {
            if (extra.url.length == 0) error.push(`O campo 'Id Extra vimeo' é obrigatório (${index}º Extra)`);
            if (extra.name.length == 0) error.push(`O título do extra é obrigatório (${index}º Extra)`);
            if (extra.isFile){
                if (!extra.file_thumbnail) error.push(`A thumbnail do extra é obrigatória (${index}º Extra)`);
            }
            else if (extra.thumbnail.length == 0) error.push(`A thumbnail do extra é obrigatória (${index}º Extra)`);
        })

        if (duration.length == 0) error.push("A duração do filme é obrigatória");
        if (expiration.length == 0) error.push("A data de expiração do filme é obrigatória");
        if (cost.length == 0) error.push("O preço é obrigatório");

        if (error.length > 0) {
            let text = "";
            error.forEach(msg => text += msg + "\n");
            message(text);
        }
        return error.length > 0 ? false : true;
    }
    function handleCheckCategoryAndGenre() {
        let checkCategories = document.getElementsByName('checkCategories') as NodeListOf<HTMLInputElement>;
        let arrCategories = [] as String[];
        Array.from(checkCategories).forEach(category => {
            if (category.checked) arrCategories.push(String(category.getAttribute('value')));
        });
        setCategory(arrCategories);

        let checkGenres = document.getElementsByName('checkGenres') as NodeListOf<HTMLInputElement>;
        let arrGenres = [] as String[];
        Array.from(checkGenres).forEach(genre => {
            if (genre.checked) arrGenres.push(String(genre.getAttribute('value')));
        });
        setGenre(arrGenres);

        setValidate(true);
    }
    async function storeMovie() {
        if (validateMovie()) {
            const data = new FormData();

            data.append('name', name);
            data.append('description', description);
            data.append('sinopse', sinopse);
            data.append('parental_rating', parentalRating);
            data.append('release', release);
            data.append('country', country);
            data.append('direction', direction);
            data.append('production', production);
            data.append('script', script);
            data.append('composition', composition);
            data.append('cast', cast);
            data.append('cost', cost);
            category.forEach((item, index) => { data.append(`category[${index}]`, String(item)); });
            genre.forEach((item, index) => { data.append(`genre[${index}]`, String(item)); })
            data.append('url', url);

            trailers.forEach((trailer, index) => {
                data.append(`trailer_url[${index}]`, trailer.url);
                data.append(`trailer_name[${index}]`, trailer.name);

                if(trailer.isFile && trailer.file_thumbnail) data.append(`trailer_file_thumbnail[${index}]`, trailer.file_thumbnail);
                else data.append(`trailer_thumbnail[${index}]`, trailer.thumbnail);
            });
            if (extras) extras.forEach((extra, index) => {
                data.append(`extra_url[${index}]`, extra.url);
                data.append(`extra_name[${index}]`, extra.name);

                if(extra.isFile && extra.file_thumbnail) data.append(`extra_file_thumbnail[${index}]`, extra.file_thumbnail);
                else data.append(`extra_thumbnail[${index}]`, extra.thumbnail);
            });

            data.append('duration', duration);
            data.append('expiration', expiration);
            data.append('freeable', (freeable ? '1': '0'));
            
            if(activeFileThumbnail && fileThumbnail) data.append('file_thumbnail', fileThumbnail);
            else data.append('thumbnail', thumbnail);

            if(activeFileWallpaper && fileWallpaper) data.append('file_wallpaper', fileWallpaper);
            else data.append('wallpaper', wallpaper);
            
            if (params.id) {
                if (Number(params.id) > 0) {
                    data.append('id', params.id);
                    api.post('auth/updateApproveMovie', data,
                        { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                            if (response.data.result) {
                                message('Filme atualizado com sucesso');
                                window.scrollTo(0, 0);
                            }
                            else message(response.data.response);
                        });
                } else {
                    data.append('id', String(Number(params.id) * -1));
                    api.post('auth/updateMovie', data,
                        { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                            if (response.data.result) {
                                message('Filme atualizado com sucesso');
                                window.scrollTo(0, 0);
                            }
                            else message(response.data.response);
                        });
                }
            } else {
                api.post('auth/registerMovie', data,
                    { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                        if (response.data.result) {
                            message('Filme cadastrado com sucesso');
                            resetAddMovie();
                            window.scrollTo(0, 0);
                        }
                        else message(response.data.response);
                    });
            }
        }
    }
    // BEGGIN:: GETTERS API
    function getCategories() {
        api.get('auth/indexCategory', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setCategories(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getGenres() {
        api.get('auth/indexGenre', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setGenres(response.data.response);
                console.log('teste y', response.data.response)
            }
            else message(response.data.response);
        });
    }
    function getGallery(type: string) {
        api.get(`auth/getGallery/${type}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) setGallery(response.data.response);
        });
    }
    function getMovieById(id: string) {
        api.get(`auth/getMovieById/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { fillAddMovie(response.data.response) }
                else message(response.data.response);
            });
    }
    function getApproveMovieById(id: string) {
        api.get(`auth/getApproveMovieById/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { fillAddMovie(response.data.response) }
                else message(response.data.response);
            });
    }
    // END:: GETTERS API | BEGGIN: GALLERY 
    let modalPayment: any;
    function openModalGallery(type: string, inputFromG?: InputFromGallery) {
        getGallery(type);
        setGalleryType(type);

        if (inputFromG) setInputFromGallery(inputFromG);
        else setInputFromGallery(null);

        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "100%";
        modalPayment.style.opacity = "1";
        modalPayment.style.display = "block";
    }
    function closeModalGallery() {
        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "0";
        modalPayment.style.opacity = "0";
        modalPayment.style.display = "none";
        setGallery([]);
    }
    function selectedImage(image_url: string) {
        if (inputFromGallery == null) {
            switch (galleryType) {
                case 'thumbnail': setThumbnail(image_url); break;
                case 'wallpaper': setWallpaper(image_url); break;
                default: message('Galeria inválida'); break;
            }
        } else {
            if (inputFromGallery.type == 'trailer') handleSetTrailer(image_url, 'thumbnail', inputFromGallery.id)
            else
                if (inputFromGallery.type == 'extra') handleSetExtra(image_url, 'thumbnail', inputFromGallery.id)
        }
        closeModalGallery();

    }
    function handleSelectImages(event: ChangeEvent<HTMLInputElement>) {
        if (!event.target.files) return;
        const selectedImages = Array.from(event.target.files);
        setUploadImages(selectedImages);
    }
    function submitUploadImages() {
        const data = new FormData();

        data.append('type', galleryType);
        uploadImages.forEach((image, index) => {
            data.append(`images[${index}]`, image);
        });

        api.post('auth/uploadImages', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message(response.data.response);
                    getGallery(galleryType);
                }
                else message(response.data.response);
            });
    }
    function deleteFromGallery(image_url: string) {
        let arr_image = image_url.split('/');
        if(arr_image.length >= 2){
            let type = arr_image[arr_image.length - 2];
            let filename = arr_image[arr_image.length - 1];

            api.get(`auth/deleteFromGallery/${type}&${filename}`,
            { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                if (response.data.result) {
                    message(response.data.response);
                    getGallery(galleryType);
                }
                else message(response.data.response);
            });
        }
        else message('Endereço de imagem inválido!');
    }
    // END:: GALLERY
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    return (
        <div id="container__add-movie-ndash">
            <Header />
            <div className="content_add-movie">
                <div className="add-movie__salutation">
                    <span className="blue-left-border">{params.id ? 'Atualizar' : 'Adicionar'} Filme</span>
                    <Link to="/filmes-cadastrados" style={{ color: '#000', borderBottom: '2px solid #dcdcdc', paddingBottom: '.15rem' }}>Filmes Cadastrados</Link>
                </div>
                <form>
                    <h5 style={{ width: '100%', marginBottom: '5px' }}>DADOS DO FILME</h5>
                    <div className="adm_agroup-input">
                        <label htmlFor="id-vimeo">Id filme vimeo</label>
                        <input type="number" name="id-vimeo" id="id-vimeo"
                            value={url} onChange={event => setUrl(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="MovieTitle">Titulo do filme</label>
                        <input type="text" name="MovieTitle" id="MovieTitle"
                            value={name} onChange={event => setName(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="indicatedAge">Classificação indicativa</label>
                        <input type="text" name="indicatedAge" id="indicatedAge"
                            value={parentalRating} onChange={event => setParentalRating(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input input-movieTime">
                        <label htmlFor="movieTime">tempo de duração do filme</label>
                        <input type="time" step='1' name="movieTime" id="movieTime"
                            value={duration} onChange={event => setDuration(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="release">Ano de lançamento</label>
                        <input type="date" name="release" id="release"
                            value={release} onChange={event => setRelease(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="production">Pais</label>
                        <input type="text" name="country" id="country"
                            value={country} onChange={event => setCountry(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="direction">Direção</label>
                        <input type="text" name="direction" id="direction"
                            value={direction} onChange={event => setDirection(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="production">Produção</label>
                        <input type="text" name="production" id="production"
                            value={production} onChange={event => setProduction(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="Script">Roteiro</label>
                        <input type="text" name="Script" id="Script"
                            value={script} onChange={event => setScript(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="composition">Composição musical</label>
                        <input type="text" name="composition" id="composition"
                            value={composition} onChange={event => setComposition(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="cast">Elenco</label>
                        <input type="text" name="cast" id="cast"
                            value={cast} onChange={event => setCast(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="cost">Preço</label>
                        <input type="number" name="cost" id="cost"
                            value={cost} onChange={event => setCost(event.target.value)} />
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="expiration">Data de Expiração</label>
                        <input type="date" name="expiration" id="expiration"
                            value={expiration} onChange={event => setExpiration(event.target.value)} />
                    </div>
                    <div className="adm_agroup-txt-area">
                        <label htmlFor="shortSynopsis">Descrição Resumida</label>
                        <textarea name="shortSynopsis" id="shortSynopsis"
                            value={description} onChange={event => setDescription(event.target.value)} />
                    </div>
                    <div className="adm_agroup-txt-area">
                        <label htmlFor="FSynopsis">Sinopse</label>
                        <textarea name="FSynopsis" id="FSynopsis" rows={6}
                            value={sinopse} onChange={event => setSinopse(event.target.value)} />
                    </div>
                    <div style={{ display: 'flex', width: '100%', flexWrap: 'wrap' }}>
                        <div style={{ width: '50%', minWidth: '360px', marginTop: '.3rem' }}>
                            <label className="input-checkbox" style={{ display: 'flex', alignItems: 'center' }}>
                                Categorias
                                <Link to="/configurar-catalogo" style={{ fontSize: '.7rem', marginLeft: '.4rem', paddingTop: '3px', color: '#1C1E29' }}><i className="fas fa-plus"></i></Link>
                            </label>
                            <div style={{ display: 'flex', flexWrap: 'wrap', marginTop: '.3rem' }}>
                                {categories.map((option) => {
                                    return (
                                        <div className="input-checkbox" style={{ width: '50%', margin: '0', alignItems: 'center' }} key={option.id}>
                                            <input type="checkbox" name='checkCategories' value={option.id} style={{ marginRight: '.3rem' }} id={`checkCategory-${option.id}`}
                                            />{option.name}
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                        <div style={{ width: '50%', minWidth: '360px', marginTop: '.3rem' }}>
                            <label className="input-checkbox" style={{ display: 'flex', alignItems: 'center' }}>
                                Gêneros
                                <Link to="/configurar-catalogo" style={{ fontSize: '.7rem', marginLeft: '.4rem', paddingTop: '3px', color: '#1C1E29' }}><i className="fas fa-plus"></i></Link>
                            </label>
                            <div style={{ display: 'flex', flexWrap: 'wrap', marginTop: '.3rem' }}>
                                {genres.map((option) => {
                                    return (
                                        <div className="input-checkbox" style={{ width: '50%', margin: '0', alignItems: 'center' }} key={option.id}>
                                            <input type="checkbox" name='checkGenres' value={option.id}
                                                style={{ marginRight: '.3rem' }} id={`checkGenre-${option.id}`} />{option.name}
                                            
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="thumbnail" style={{ 
                            display: 'flex', justifyContent: 'space-between', alignItems: 'center'
                        }}>
                            {!activeFileThumbnail && 'URL da '}Thumbnail
                            <button onClick={() => handleToggleInputFile('thumbnail')} type="button" 
                                style={{
                                    padding: '.4rem', backgroundColor: '#333', color: '#eee', width: 'auto', cursor: 'pointer'
                                }}
                            >
                                {!activeFileThumbnail ? 'UPLOAD' : 'URL'}
                            </button>
                        </label>

                        {activeFileThumbnail ? (<>
                            <input type="file" ref={inputThumbnail} onChange={event => {
                                if(event.target.files) setFileThumbnail(event.target.files[0])
                            }}/>
                        </>):(<>
                            <input type="text" name="thumbnail" id="thumbnail" placeholder="Proporção: 640x935"
                                value={thumbnail} onChange={event => setThumbnail(event.target.value)} />
                            <button type="button" onClick={() => openModalGallery('thumbnail')}>Galeria</button>
                        </>)}
                        {thumbnail.length > 0 && ( <img src={thumbnail} style={{ width: '64px', height: '93.5px' }} /> )}
                    </div>
                    <div className="adm_agroup-input">
                        <label htmlFor="wallpaper" style={{ 
                            display: 'flex', justifyContent: 'space-between', alignItems: 'center'
                        }}>
                            {!activeFileWallpaper && 'URL da '}Capa
                            <button onClick={() => handleToggleInputFile('wallpaper')} type="button" 
                                style={{
                                    padding: '.4rem', backgroundColor: '#333', color: '#eee', width: 'auto', cursor: 'pointer'
                                }}
                            >
                                {!activeFileWallpaper ? 'UPLOAD' : 'URL'}
                            </button>
                        </label>

                        {activeFileWallpaper ? (<>
                            <input type="file" ref={inputWallpaper} onChange={event => {
                                if(event.target.files) setFileWallpaper(event.target.files[0])
                            }}/>
                        </>):(<>
                            <input type="text" name="wallpaper" id="wallpaper" placeholder="Proporção: 1920x1080"
                                value={wallpaper} onChange={event => setWallpaper(event.target.value)} />
                            <button type="button" onClick={() => openModalGallery('wallpaper')}>Galeria</button>
                        </>)}
                        {wallpaper.length > 0 && ( <img src={wallpaper} style={{ width: '166.22px', height: '93.5px' }} /> )}
                    </div>
                    <div className="input-checkbox" style={{ margin: '0', alignItems: 'center' }}>
                        <label style={{ display: 'flex', alignItems: 'center', }}>
                            <input type="checkbox" onClick={()=> setFreeable(!freeable)} 
                                checked={freeable} style={{ marginRight: '.3rem' }}/>
                            Disponível Gratuitamente
                        </label>
                    </div>
                    {/* add trailer */}
                    <div className="add__traler" style={{ marginTop: '1.4rem', paddingTop: '.4rem', borderTop: '1px solid #dcdcdc' }}>
                        <h5>DADOS DO TRAILER</h5>
                        <div style={{ display: 'flex', gap: '.5rem' }}>
                            <div onClick={AddTrailer} className="btn__add__traler">+ Trailer</div>
                            {trailers.length > 1 && (<div onClick={RemoveTrailer} className="btn__add__traler">
                                - Trailer
                            </div>)}
                        </div>
                        <div className="container__list-trailer">
                            {trailers.map((trailer) => {
                                return (
                                    <div id={`trailer_item_${trailer.id}`} key={trailer.id} className="list-trailer__item">
                                        <div>
                                            <label htmlFor="id-vimeo-trailer">Id trailer vimeo</label>
                                            <input type="text" name="id-vimeo-trailer" value={trailer.url} onChange={event => handleSetTrailer(event.target.value, 'url', trailer.id)} />
                                        </div>
                                        <div>
                                            <label htmlFor="trailer-name">Título do trailer</label>
                                            <input type="text" name="trailer-name" value={trailer.name} onChange={event => handleSetTrailer(event.target.value, 'name', trailer.id)} />
                                        </div>
                                        <div>
                                            <label htmlFor={`trailersThumbnail${trailer.id}`}  style={{ 
                                                display: 'flex', justifyContent: 'space-between', alignItems: 'center'
                                            }}>
                                                {!trailer.isFile && 'URL da '}Thumbnail do Trailer
                                                <button onClick={() => handleSetTrailer('','isFile', trailer.id)}
                                                    type="button" style={{
                                                        padding: '.4rem', backgroundColor: '#333', color: '#eee', width: 'auto', cursor: 'pointer'
                                                    }}
                                                >
                                                    {!trailer.isFile ? 'UPLOAD' : 'URL'}
                                                </button>
                                            </label>
                                            {trailer.isFile ? (<>
                                                <input type="file" onChange={event => {
                                                    if(event.target.files) handleSetTrailer('','file_thumbnail', trailer.id,event.target.files[0])
                                                }}/>
                                            </>):(<>
                                                <input type="text" name="trailersThumbnail" id={`trailersThumbnail${trailer.id}`} placeholder="Proporção: 310x174" value={trailer.thumbnail} onChange={event => handleSetTrailer(event.target.value, 'thumbnail', trailer.id)} />
                                                <button type="button" onClick={() => openModalGallery('horizontal-thumbnail', { id: trailer.id, type: 'trailer' })}>Galeria</button>
                                            </>)}
                                        </div>
                                        {trailer.thumbnail.length > 0 && (
                                            <img src={trailer.thumbnail} style={{ width: '166.22px', height: '93.5px' }} />
                                        )}
                                    </div>
                                )
                            })}
                        </div>
                    </div>

                    <div className="add__traler" style={{ marginTop: '1.4rem', paddingTop: '.4rem', borderTop: '1px solid #dcdcdc' }}>
                        <h5>EXTRAS</h5>
                        <div style={{ display: 'flex', gap: '.5rem' }}>
                            <div onClick={AddExtra} className="btn__add__traler">+ Extras</div>
                            {extras && extras.length > 0 && (<div onClick={RemoveExtra} className="btn__add__traler">
                                - Extras
                            </div>)}
                        </div>
                        <div className="container__list-trailer">
                            {extras?.map((extra) => {
                                return (
                                    <div id={`extra_item_${extra.id}`} key={extra.id} className="list-trailer__item">
                                        <div>
                                            <label>Id Extra vimeo</label>
                                            <input type="text" name="id-vimeo-extra" value={extra.url} onChange={event => handleSetExtra(event.target.value, 'url', extra.id)} />
                                        </div>
                                        <div>
                                            <label>Título do Extra</label>
                                            <input type="text" name="extra-name" value={extra.name} onChange={event => handleSetExtra(event.target.value, 'name', extra.id)} />
                                        </div>
                                        <div>
                                            <label htmlFor={`extraThumbnail${extra.id}`}  style={{ 
                                                display: 'flex', justifyContent: 'space-between', alignItems: 'center'
                                            }}>
                                                {!extra.isFile && 'URL da '}Thumbnail do Extra
                                                <button onClick={() => handleSetExtra('','isFile', extra.id)}
                                                    type="button" style={{
                                                        padding: '.4rem', backgroundColor: '#333', color: '#eee', width: 'auto', cursor: 'pointer'
                                                    }}
                                                >
                                                    {!extra.isFile ? 'UPLOAD' : 'URL'}
                                                </button>
                                            </label>
                                            {extra.isFile ? (<>
                                                <input type="file" onChange={event => {
                                                    if(event.target.files) handleSetExtra('','file_thumbnail', extra.id,event.target.files[0])
                                                }}/>
                                            </>):(<>
                                                <input type="text" name="extrasThumbnail" id={`extraThumbnail${extra.id}`} placeholder="Proporção: 310x174" value={extra.thumbnail} onChange={event => handleSetExtra(event.target.value, 'thumbnail', extra.id)} />
                                                <button type="button" onClick={() => openModalGallery('horizontal-thumbnail', { id: extra.id, type: 'extra' })}>Galeria</button>
                                            </>)}
                                        </div>
                                        {extra.thumbnail.length > 0 && (
                                            <img src={extra.thumbnail} style={{ width: '166.22px', height: '93.5px' }} />
                                        )}
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </form>
                <div className="container-btn__add-muvie">
                    <Link to="/inserir-midia" type="button" className="btn__add-muvie">Cancelar </Link>
                    <button type="button" className="btn__add-muvie" onClick={handleCheckCategoryAndGenre}>{params.id ? 'Atualizar' : 'Cadastrar'} filme</button>
                </div>
            </div>
            <div id="modal-gallery" className="change-payment__content-modal">
                <div onClick={closeModalGallery} className="btn-close-modal"> X </div>
                <div className="content-modal">
                    <h4>
                        {galleryType == 'thumbnail' && (<>Thumbnail <small>640x935</small></>)}
                        {galleryType == 'wallpaper' && (<>Capa <small>1920x1080</small></>)}
                        {galleryType == 'horizontal-thumbnail' && (<>Thumbnail de Trailer <small>310x174</small></>)}
                    </h4>
                    <input multiple type="file" id="image[]" onChange={handleSelectImages} />
                    <button type="button" onClick={submitUploadImages}>Upload de imagens</button>
                </div>

                <div className="container-modal-imgs">
                    {gallery.map((image, index) => {
                        return (
                           
                            <div className="container-img-galery" key={index} style={{ maxWidth: '15rem'}}>
                                <img src={image} style={{ width: '100%' }}></img>
                                <div>
                                    <div  style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                    }}>
                                        <button onClick={() => selectedImage(image)}
                                            style={{
                                                flex: '1',
                                                width: '100%',
                                                background: 'transparent',
                                            }}
                                        >
                                            <i className="fa fa-check"></i>
                                        </button>
                                        <button  onClick={() => deleteFromGallery(image)}
                                            style={{
                                                background: 'rgba(200,43,35,.6)',
                                                width: '100%',
                                                padding: '.4rem',
                                            }}
                                        >
                                            <i className="far fa-trash-alt" style={{color: 'white'}}></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                    {gallery.length == 0 && (<p style={{ color: '#ccc' }}>Sua galeria está vazia</p>)}
                   {/* <div>
                        <LoadSpin />
                   </div> */}
                   
                </div>
            </div>
        </div>
    );
}

export default AddMovie;
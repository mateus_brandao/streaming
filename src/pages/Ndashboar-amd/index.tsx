import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/Dash/Header'
import ImgInsertmedia from '../../assets/ico/inserirmidia.svg'
import ImgMovie from '../../assets/ico/filmes.svg';
import ImgPodcast from '../../assets/ico/podcast.svg';
import AddParceiro from '../../assets/ico/adicionarparceiro.svg';
import icoengrenagem from '../../assets/ico/configuracoes.svg';
import IcoParceiro from '../../assets/ico/parceiro.svg';
import { GeneralContext } from '../../contexts/GeneralContext';

function NdashboardAdm() {
    const [userError, setuserError] = useState(true)
    const {user, setLoadOpen, openLoad, loadPage} = useContext (GeneralContext);
    useEffect(() =>{setLoadOpen(true)},[]);
    useEffect(() =>{
        if(openLoad &&  user && user.first_name.length > 0){
            // loadPage()
            console.log('Key')
        }
    },[user, openLoad]);
    return (
        <div className="adm-home-screen">
            <Header />
            <div className="adm-home-screen__container">
            <div className="indicate-area">Área da equipe</div>
                <div className="adm-home-screen__container-flex">
                    
                    <div className="adm-home-screen__container-flex-item">
                        <p className="title-1 color-txt">
                            <span className="blue-left-border">Oi {user.first_name}</span><br />
                            Boas vindas ao <br />
                            Painel Magnificat.
                        </p>
                        <span className="txt-1 color-txt"> O que você quer fazer agora?</span>
                    </div>
                    <div className="adm-home-screen__container-flex-item aline-flexend">
                        <Link to="/inicio" className="btn-adm-hom-scren"><span>Voltar para <strong>Magnificat</strong></span></Link>
                    </div>
                </div>
            </div>
            <div className="adm-home-screen__container  margin-top-40">
                <div className="adm-home-screen__container-flex">
                    <div className="adm-home-screen__container-flex-item flex-row gap-10">
                        <Link to="/painel-de-controle" className="nav-dash-adm color-txt"><i className="far fa-user-circle"></i><span> Conta</span></Link>
                        <Link to="/sua-conta" className="nav-dash-adm color-txt"><img src={icoengrenagem} alt=""/><span>Configuração</span></Link>
                    </div>
                    <div className="adm-home-screen__container-flex-item flex-colum gap-10">
                        <div className="txt-1 color-txt aline-flexend txt-aling-rigth">
                            Ei {user.first_name}, está tudo certo? <br />
                            Não encontrou o que precisava? <br />
                            Nós te ajudamos!
                        </div>

                        <div className="adm-home-screen__container-flex-item aline-flexend">
                            <Link to="#" className=" btn-adm-hom-scren">Abrir Chamado</Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container__status">

                 <div className="container__status-content ">
                    <div className="container__status-content__title">
                        <h3>Magnificat</h3>
                        <div>
                            {userError ? 'tá ON' : 'TÁ OF'}
                        </div>
                        <span>
                            No momento, Magnificat está
                            funcionando perfeitamente.
                            Nenhum erro foi reportado.
                        </span>
                    </div>
                </div>

            </div>

          
           
            <div className="container-tools__adm">
                <h5>CATÁLOGO</h5>
                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <Link to="/inserir-midia" className="nav-dash-adm color-txt"> <img src={ImgInsertmedia} alt="" /> <span>Inserir midía</span></Link>
                    <Link to="/filmes-cadastrados" className="nav-dash-adm color-txt"><img src={ImgMovie} alt="" /><span>Filmes</span></Link>
                    {/* <Link to="/series-cadastradas" className="nav-dash-adm color-txt"><img src={ImgMovie} alt="" /><span>Séries</span></Link> */}
                    <Link to="/podcasts-cadastrados" className="nav-dash-adm color-txt"><img src={ImgPodcast} alt="" /><span>Podcast</span></Link>
                    {/* 
                        <div className="nav-dash-adm color-txt">4</div>
                        <div className="nav-dash-adm color-txt">5</div>
                        <div className="nav-dash-adm color-txt">6</div>
                        <div className="nav-dash-adm color-txt">7</div>
                    <div className="nav-dash-adm color-txt">8</div>*/}
                    <Link to="/configurar-catalogo" className="nav-dash-adm color-txt"><img src={icoengrenagem} alt="" /> <span>configurações de catálogo</span></Link>
                </div>
            </div>
            <div className="container-tools__adm">
                <h5>PARCEIROS</h5>
                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <Link to="/add-parceiro" className="nav-dash-adm color-txt"> <img src={AddParceiro} alt="" /> <span>Inserir parceiro</span> </Link>
                    <Link to="/lista-parceiro" className="nav-dash-adm color-txt"><img src={IcoParceiro} alt="" /><span>Parceiro</span></Link>
                    {/* <div className="nav-dash-adm color-txt">3</div>
                    <div className="nav-dash-adm color-txt">4</div>
                    <div className="nav-dash-adm color-txt">5</div> */}
                </div>
            </div>
            {/* <div className="container-tools__adm">
                <h5>ASSINANTES</h5>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <div className="nav-dash-adm color-txt">1</div>
                    <div className="nav-dash-adm color-txt">2</div>
                    <div className="nav-dash-adm color-txt">3</div>
                </div>
            </div>
            <div className="container-tools__adm">
                <h5>FINANCEIRO</h5>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <div className="nav-dash-adm color-txt">1</div>
                    <div className="nav-dash-adm color-txt">2</div>
                    <div className="nav-dash-adm color-txt">3</div>
                    <div className="nav-dash-adm color-txt">4</div>
                </div>
            </div>
            <div className="container-tools__adm">
                <h5>CENTRAL DE AJUDA</h5>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <div className="nav-dash-adm color-txt">1</div>
                    <div className="nav-dash-adm color-txt">2</div>
                    <div className="nav-dash-adm color-txt">3</div>
                    <div className="nav-dash-adm color-txt">4</div>
                </div>
            </div>
            <div className="container-tools__adm">
                <h5>ATUALIZAÇÕES</h5>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <div className="nav-dash-adm color-txt">1</div>
                    <div className="nav-dash-adm color-txt">2</div>
                </div>
            </div>
            <div className="container-tools__adm">
                <h5>STATUS MAGNIFICAT - CHAMADOS</h5>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <div className="nav-dash-adm color-txt">1</div>
                    <div className="nav-dash-adm color-txt">2</div>
                    <div className="nav-dash-adm color-txt">3</div>

                </div>
            </div>
            <div className="container-tools__adm">
                <h5>COMUNICAÇÃO & PUBLICIDADE</h5>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <div className="nav-dash-adm color-txt">1</div>
                    <div className="nav-dash-adm color-txt">2</div>
                    <div className="nav-dash-adm color-txt">3</div>
                    <div className="nav-dash-adm color-txt">4</div>
                </div>
            </div>
 */}

        </div>
    );
}
export default NdashboardAdm;
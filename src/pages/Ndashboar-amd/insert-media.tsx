import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/Dash/Header';
import icoMovie from '../../assets/ico/filmes.svg';
import icoPodCast from '../../assets/ico/podcast.svg';

function InsertMedia() {
    return (
        <div id="container__insert-media">
            <Header />
            <div className="insert-media__salutation">
                <span className="blue-left-border">Adicionar Mídia</span>

                <div className="insert-media__btn-group">
                    <Link className="btn__go-back-midias" to="/painel-de-controle-adm">Voltar</Link>
                </div>
            </div>
            <div className="option__insert-media">
                <p>Que mídia você deseja adicionar?</p>

                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <Link to="/adicionar-filme" className="nav-dash-adm color-txt"><img src={icoMovie} alt="" /><span>Inserir Filme</span></Link>

                </div>
                
                {/* <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                <Link to="#" className="nav-dash-adm color-txt"> <span>inserir midia</span></Link>
                <Link to="#" className="nav-dash-adm color-txt"><span>inserir midia</span></Link>
                <Link to="#" className="nav-dash-adm color-txt"><span>inserir midia</span></Link>
                </div> */}
                {/* <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <Link to="#" className="nav-dash-adm color-txt"><span>inserir midia</span></Link>
                    <Link to="#" className="nav-dash-adm color-txt"><span>inserir midia</span></Link>
                    <Link to="#" className="nav-dash-adm color-txt"><span>inserir midia</span></Link>
                </div> */}
                <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <Link to="/inserir-podcast" className="nav-dash-adm color-txt"> <img src={icoPodCast}  alt=""/> <span>podcast</span></Link>
                    {/* <Link to="#" className="nav-dash-adm color-txt"> <img src={icoPodCast}  alt=""/> <span>Novo epsodio</span></Link> */}
                </div>
                {/* <div className="adm-home-screen__container-flex-item flex-row gap-10 tools__adm">
                    <Link to="#" className="nav-dash-adm color-txt"><span>inserir midia</span></Link>
                </div> */}
            </div>

        </div>
    );
}
export default InsertMedia;
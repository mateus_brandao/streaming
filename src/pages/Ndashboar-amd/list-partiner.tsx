import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/Dash/Header';

function ListPartiner() {
    const Partiner = [
        {
            id: '01',
            nome: 'nome',
            email: 'email@gmail.com'
        },
       

    ]


    return (
        <div id="container-list-partiner">
            <Header />

            <div className="content-list-partiner">
                <div >
                    <span>Parceiros cadastrados</span>
                    <Link to="/add-parceiro">
                        <div>
                            <p>Add novo Parceiro</p>
                            <div className="btn-add-new-partiner">
                                <div>
                                    +
                                </div>
                            </div>
                        </div>
                    </Link>
                </div>

                <table className="partiner-table">
                    <tr>
                        <th>
                            Nome
                           </th>
                        <th>
                            Email
                           </th>
                        <th>
                            excluir
                           </th>
                    </tr>
                    {Partiner.map((item, index) => (
                        <tr key={index}>
                            <td>
                                {item.nome}
                            </td>
                            <td>
                                {item.email}
                            </td>
                            <td className="delite-partiner">
                                <i className="material-icons">
                                    delete
                                </i>
                            </td>
                        </tr>
                    ))}
                </table>
                <div className="btn-list-partiner">
                    <Link to="/painel-de-controle-adm">Voltar</Link>
                </div>
            </div>

        </div>
    );
}

export default ListPartiner;
import React, { ChangeEvent, useContext, useEffect, useState } from 'react';
import api from "../../services/api";
import ButtonIcon from '../../components/Buttons/Button-icon';
import Header from '../../components/Dash/Header';
import { Link, useHistory } from 'react-router-dom';
import { GeneralContext } from '../../contexts/GeneralContext';
import { Season, User } from '../../types/typesdef';

function RegisteredSeries() {
    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'provider' });
    const [approveSeasons, setApproveSeasons] = useState<Season[]>([]);
    const [seasons, setSeasons] = useState<Season[]>([]);
    const { setOpen, setMessage } = useContext(GeneralContext);
    // BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    const history = useHistory();
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getApproveSeason();
            getSeason();
            getUser();
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    // BEGIN: APPROVE MOVIE
    function approveSeason(id: number) {
        const data = new FormData();

        data.append('season_id', String(id));
        api.post('auth/approveSeason', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('Temporada Aprovada!');
                    getSeason();
                    getApproveSeason();
                }
                else message(response.data.response);
            });
    }
    function disapproveSeason(id: number) {
        const data = new FormData();

        data.append('season_id', String(id));
        api.post('auth/disapproveSeason', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('Temporada Reprovada!');
                    getSeason();
                    getApproveSeason();
                }
                else message(response.data.response);
            });
    }
    // END: APPROVE MOVIE | BEGIN:: DELETE MOVIE / CATEGORY / GENRES
    function deleteSeason(id: number) {
        api.get(`auth/deleteSeason/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Temporada Excluída!');
                getSeason();
            }
            else message(response.data.response);
        });
    }
    function deleteApproveSeason(id: number) {
        api.get(`auth/deleteApproveSeason/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Temporada Excluída!');
                getApproveSeason();
            }
            else message(response.data.response);
        });
    }
    // BEGGIN:: GETTERS API
    function getApproveSeason() {
        api.get('auth/seasonWaitingApproval', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { setApproveSeasons(response.data.response) }
                else message(response.data.response);
            });
    }
    function getSeason() {
        api.get('auth/indexSeason', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setSeasons(response.data.response)
                }
                else message(response.data.response);
            });
    }
    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
            }
        });
    }
    // END:: GETTERS API
    return (
        <div id="container__movie-aprove">
            < Header />

            <div className="salutation__catalogo">
                <span className="blue-left-border">Temporadas cadastradas</span>
                <Link className="salutation__catalogo__go-back" to="/painel-de-controle-adm">Voltar</Link>
            </div>
            <div className="approval-condition">
                <h4>AGUARDANDO APROVAÇÃO</h4>
                {approveSeasons.length == 0 && (<p>Você não tem nenhuma temporada aguardando aprovação</p>)}
            </div>

            <div className="container-muvie-aprove">
                {approveSeasons.map(season => {
                    return (
                        <div key={season.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${season.thumbnail})`, backgroundSize: 'cover' }} to={`/add-season/${season.id}`}>
                                <div className="modal-muvie-title">
                                    {season.title}
                                </div>
                                <div className="modal-muvie-info">
                                    <div className="muvie-time">{season.indice} ª Temporada</div>
                                </div>
                            </Link>
                            <div className="indicate-aprove">
                                <p style={{ textAlign: 'center', marginTop: '.4rem' }}>
                                    {season.approval == 'approved' && (<span>APROVADO</span>)}
                                    {season.approval == 'disapproved' && (<span>REPROVADO</span>)}
                                    {season.approval == 'pending' && (<span>PENDENTE</span>)}
                                </p>
                            </div>
                            {/*Botao */}
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    {/* <ButtonIcon type="button" icon="fas fa-film"
                                        onClick={() => history.push(`/descricao-do-filme/${season.id}`)} /> */}
                                    <ButtonIcon type="button" icon="far fa-thumbs-up"
                                        onClick={() => approveSeason(season.id)} />
                                    <ButtonIcon type="button" icon="far fa-thumbs-down" danger
                                        onClick={() => disapproveSeason(season.id)} />
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deleteApproveSeason(season.id)} />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>
            <div className="approval-condition" style={{ marginTop: '1.4rem', paddingTop: '.4rem', borderTop: '1px solid #dcdcdc' }}>
                <h4>APROVADOS</h4>
                {seasons.length == 0 && (<p>Você não tem nenhuma temporada aprovada</p>)}
            </div>

            <div className="container-muvie-aprove">
                {seasons.map(season => {
                    return (
                        <div key={season.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${season.thumbnail})`, backgroundSize: 'cover' }} to={`/add-season/-${season.id}`}>
                                <div className="modal-muvie-title">
                                    {season.title}
                                </div>
                                <div className="modal-muvie-info">
                                    <div className="muvie-time">{season.indice} ª Temporada</div>
                                </div>
                            </Link>
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    <ButtonIcon type="button" icon="fas fa-film"
                                        onClick={() => history.push(`/descricao-do-filme/${season.id}`)} />
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deleteApproveSeason(season.id)} />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default RegisteredSeries;
import React, { useContext, useEffect, useState } from 'react';
import api from "../../services/api";
import ButtonIcon from '../../components/Buttons/Button-icon';
import Header from '../../components/Dash/Header';
import { Link, useHistory } from 'react-router-dom';
import { User, Podcast } from '../../types/typesdef';
import { GeneralContext } from '../../contexts/GeneralContext';
function RegisteredPodcasts() {
    const [user, setUser] = useState<User>(
        {first_name: '', last_name: '', email: '', permission: 'provider' }
    );
    const [approvePodcasts, setApprovePodcasts] = useState<Podcast[]>([]);
    const [podcasts, setPodcasts] = useState<Podcast[]>([]);
    // BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    const { setOpen, setMessage } = useContext(GeneralContext);
    const history = useHistory();
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getApprovePodcasts();
            getPodcasts();
            getUser();
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    // BEGIN: APPROVE MOVIE
    function approvePodcast(id: number) {
        const data = new FormData();

        data.append('podcast_id', String(id));
        api.post('auth/approvePodcast', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('Podcast Aprovado!');
                    getPodcasts();
                    getApprovePodcasts();
                }
                else message(response.data.response);
            });
    }
    function disapprovePodcast(id: number) {
        const data = new FormData();

        data.append('podcast_id', String(id));
        api.post('auth/disapprovePodcast', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message('Podcast Reprovado!');
                    getPodcasts();
                    getApprovePodcasts();
                }
                else message(response.data.response);
            });
    }
    // END: APPROVE MOVIE | BEGIN:: DELETE MOVIE / CATEGORY / GENRES
    function deletePodcast(id: number) {
        api.get(`auth/deletePodcast/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Podcast Excluído!');
                getPodcasts();
            }
            else message(response.data.response);
        });
    }
    function deleteApprovePodcast(id: number) {
        api.get(`auth/deleteApprovePodcast/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                message('Podcast Excluído!');
                getApprovePodcasts();
            }
            else message(response.data.response);
        });
    }
    // BEGGIN:: GETTERS API
    function getApprovePodcasts() {
        api.get('auth/podcastWaitingApproval', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { setApprovePodcasts(response.data.response) }
                else message(response.data.response);
            });
    }
    function getPodcasts() {
        api.get('auth/indexPodcast', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setPodcasts(response.data.response)
                }
                else message(response.data.response);
            });
    }
    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
            }
        });
    }
    // END:: GETTERS API
    return (
        <div id="container__movie-aprove">
            < Header />

            <div className="salutation__catalogo">
                <span className="blue-left-border">Podcasts cadastrados</span>
                <Link className="salutation__catalogo__go-back" to="/painel-de-controle-adm">Voltar</Link>
            </div>
            <div className="approval-condition">
                <h4>AGUARDANDO APROVAÇÃO</h4>
                {approvePodcasts.length == 0 && (<p>Você não tem nenhum podcast aguardando aprovação</p>)}
            </div>

            <div className="container-muvie-aprove">
                {approvePodcasts.map(podcast => {
                    return (
                        <div key={podcast.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${podcast.thumbnail})`, backgroundSize: 'cover' }} to={`/inserir-podcast/${podcast.id}`}>
                                <div className="modal-muvie-title">
                                    {podcast.name}
                                </div>
                                <div className="modal-muvie-info">
                                    {/* <div className="modal-indicate">{podcast.parental_rating}</div> */}
                                    <div className="muvie-time">{podcast.duration}</div>

                                </div>
                            </Link>
                            <div className="indicate-aprove">
                                <p style={{ textAlign: 'center', marginTop: '.4rem' }}>
                                    {podcast.approval == 'approved' && (<span>APROVADO</span>)}
                                    {podcast.approval == 'disapproved' && (<span style={{ color: '#b20000', fontWeight: 'bold' }}>REPROVADO</span>)}
                                    {podcast.approval == 'pending' && (<span>PENDENTE</span>)}
                                </p>
                            </div>
                            {/*Botao */}
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    {/* <ButtonIcon type="button" icon="fas fa-film"
                                        onClick={() => history.push(`/descricao-do-filme/${podcast.id}`)} /> */}
                                    <ButtonIcon type="button" icon="far fa-thumbs-up"
                                        onClick={() => approvePodcast(podcast.id)} />
                                    <ButtonIcon type="button" icon="far fa-thumbs-down" danger
                                        onClick={() => disapprovePodcast(podcast.id)} />
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deleteApprovePodcast(podcast.id)} />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>
            <div className="approval-condition" style={{ marginTop: '1.4rem', paddingTop: '.4rem', borderTop: '1px solid #dcdcdc' }}>
                <h4>APROVADOS</h4>
                {podcasts.length == 0 && (<p>Você não tem nenhum podcast aprovado</p>)}
            </div>

            <div className="container-muvie-aprove">
                {podcasts.map(podcast => {
                    return (
                        <div key={podcast.id}>
                            <Link className="card-muvie-aprove" style={{ backgroundImage: `url(${podcast.thumbnail})`, backgroundSize: 'cover' }} to={`/inserir-podcast/-${podcast.id}`}>
                                <div className="modal-muvie-title">
                                    {podcast.name}
                                </div>
                                <div className="modal-muvie-info">
                                    {/* <div className="modal-indicate">{podcast.parental_rating}</div> */}
                                    <div className="muvie-time">{podcast.duration}</div>
                                </div>
                            </Link>
                            {user.permission == 'root' && (
                                <div className="aprove-btn-group" style={{ marginTop: '.4rem', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                    {/* history.push(`/descricao-do-filme/${podcast.id}`) */}
                                    <ButtonIcon type="button" icon="fas fa-microphone-alt"
                                        onClick={() => message('Em Desenvolvimento') }/>
                                    <ButtonIcon type="button" icon="far fa-trash-alt" danger
                                        onClick={() => deletePodcast(podcast.id)} />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>

        </div>

       
        
    );
}

export default RegisteredPodcasts;
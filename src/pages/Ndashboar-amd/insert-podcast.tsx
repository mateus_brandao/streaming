import React, { ChangeEvent, useContext, useEffect, useRef, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Header from '../../components/Dash/Header';
import { GeneralContext } from '../../contexts/GeneralContext';
import api from '../../services/api';
import { Category, Genre, RecordCompanies, Artists, Albums, Podcast } from '../../types/typesdef';

interface InputFromGallery {
    id: number;
    type: string;
}
interface PodcastParams {
    id?: string;
}

function InsertPodcast() {
    const params = useParams<PodcastParams>();
    const { setOpen, setMessage, user } = useContext(GeneralContext);
    const inputAudio = useRef<HTMLInputElement>(null);
    // BEGIN:: REGISTER PODCAST
    const [url,setUrl] = useState<File | null>(null);
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [clip_url, setClipUrl] = useState('');
    const [duration, setDuration] = useState('');
    const [thumbnail, setThumbnail] = useState('');
    const [wallpaper, setWallpaper] = useState('');
    const [release, setRelease] = useState('');
    const [country, setCountry] = useState('');
    const [production, setProduction] = useState('');
    const [edition, setEdition] = useState('');
    const [episode_index, setEpisodeIndex] = useState(0);
    const [expiration, setExpiration] = useState('');
    const [awards, setAwards] = useState('');
    
    const [record_company_id, setRecordCompanyId] = useState<number>(0);
    const [artist_id, setArtistId] = useState<number>(0);
    const [album_id, setAlbumId] = useState<number>(0);
    const [category, setCategory] = useState<String[]>([]);
    const [genre, setGenre] = useState<String[]>([]);
    // END:: REGISTER MOVIE
    const [record_companies, setRecordCompanies] = useState<RecordCompanies[]>([]);
    const [artists, setArtists] = useState<Artists[]>([]);
    const [albums, setAlbums] = useState<Albums[]>([]);

    const [categories, setCategories] = useState<Category[]>([]);
    const [genres, setGenres] = useState<Genre[]>([]);
    const [validate, setValidate] = useState(false);

    // BEGIN:: GALLERY
    const [gallery, setGallery] = useState([]);
    const [galleryType, setGalleryType] = useState('thumbnail');
    const [inputFromGallery, setInputFromGallery] = useState<InputFromGallery | null>(null);
    const [uploadImages, setUploadImages] = useState<File[]>([]);
    // END:: GALLERY | BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getCategories();
            getGenres();
            getRecordCompanies();
            getArtists();
            getAlbums();
            if (params.id) {
                if (Number(params.id) > 0) getApprovePodcastById(params.id);
                else getPodcastById(String(Number(params.id) * -1));
            }
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN
    useEffect(() => {
        if (validate) {
            storePodcast();
            setValidate(false);
        }
    }, [validate]);

    // BEGIN:: SEND FORM
    function resetAddPodcast() {
        if(inputAudio.current) inputAudio.current.value = '';
        setUrl(null);
        setName('');
        setDescription('');
        setClipUrl('');
        setDuration('');
        setThumbnail('');
        setWallpaper('');
        setRelease('');
        setCountry('');
        setProduction('');
        setEdition('');
        setEpisodeIndex(0);
        setExpiration('');
        setRecordCompanyId(0);
        setArtistId(0);
        setAlbumId(0);
        
        setCategory([]);
        let checkCategories = document.getElementsByName('checkCategories') as NodeListOf<HTMLInputElement>;
        Array.from(checkCategories).forEach(category => category.checked = false);

        setGenre([]);
        let checkGenres = document.getElementsByName('checkGenres') as NodeListOf<HTMLInputElement>;
        Array.from(checkGenres).forEach(genre => genre.checked = false);    
    }
    function fillAddPodcast(podcast: Podcast) {
        if (!podcast || !podcast.name) {
            message('Houve um erro inesperado ao selecionar o podcast');
            return false;
        }
        setName(podcast.name);
        setDescription(podcast.description);
        setClipUrl(podcast.clip_url);
        setDuration(podcast.duration);
        setThumbnail(podcast.thumbnail);
        setWallpaper(podcast.wallpaper);
        setRelease(podcast.release);
        setCountry(podcast.country);
        setProduction(podcast.production);
        setEdition(podcast.edition);
        setEpisodeIndex(podcast.episode_index)
        setAwards(podcast.awards);
        setExpiration(podcast.expiration);
        setRecordCompanyId(podcast.record_company.id);
        setArtistId(podcast.artist.id);
        setAlbumId(podcast.album.id);
        
        podcast.categories.forEach(category => {
            let element = document.getElementById(`checkCategory-${category.id}`) as any;
            if (element) element.checked = true;
        });
        podcast.genres.forEach(genre => {
            let element = document.getElementById(`checkGenre-${genre.id}`) as any;
            if (element) element.checked = true;
        });
    }
    function validatePodcast() {
        let error = [] as String[]
        if (!params.id && !(inputAudio.current && inputAudio.current.value.length > 0)) error.push("O campo áudio é obrigatório");
        if (name.length == 0) error.push("O título do podcast é obrigatório");
        if (description.length == 0) error.push("A descrição do podcast é obrigatória");
        if (duration.length == 0) error.push("A duração do podcast é obrigatória");        
        if (thumbnail.length == 0) error.push("A thumbnail é obrigatória");
        if (wallpaper.length == 0) error.push("A capa do podcast é obrigatória");
        if (release.length == 0) error.push("O ano de lançamento é obrigatório");
        if (country.length == 0) error.push("O país é obrigatório");
        if (production.length == 0) error.push("O campo produção é obrigatório");
        if (edition.length == 0) error.push("O campo edição é obrigatório");
        if (episode_index < 1) error.push("Adicione um indice de episódio maior do que 0");
        if (expiration.length == 0) error.push("A data de expiração do podcast é obrigatória");
        // award *optional
        if (category.length == 0) error.push("A categoria é obrigatória");
        if (genre.length == 0) error.push("O gênero é obrigatório");
        if (record_company_id < 1) error.push("Selecione uma Gravador");
        if (artist_id < 1) error.push("Selecione um artista");
        if (album_id < 1) error.push("Selecione um album");

        if (error.length > 0) {
            let text = "";
            error.forEach(msg => text += msg + "\n");
            message(text);
        }
        return error.length > 0 ? false : true;
    }
    function handleCheckCategoryAndGenre() {
        let checkCategories = document.getElementsByName('checkCategories') as NodeListOf<HTMLInputElement>;
        let arrCategories = [] as String[];
        Array.from(checkCategories).forEach(category => {
            if (category.checked) arrCategories.push(String(category.getAttribute('value')));
        });
        setCategory(arrCategories);

        let checkGenres = document.getElementsByName('checkGenres') as NodeListOf<HTMLInputElement>;
        let arrGenres = [] as String[];
        Array.from(checkGenres).forEach(genre => {
            if (genre.checked) arrGenres.push(String(genre.getAttribute('value')));
        });
        setGenre(arrGenres);

        setValidate(true);
    }
    async function storePodcast() {
        if (validatePodcast()) {
            const data = new FormData();
            if(url) data.append('url', url);
            data.append('name', name);
            data.append('description', description);
            data.append('clip_url', clip_url);
            data.append('duration', duration);
            data.append('thumbnail', thumbnail);
            data.append('wallpaper', wallpaper);
            data.append('release', release);
            data.append('country', country);
            data.append('production', production);
            data.append('edition', edition);
            data.append('episode_index', String(episode_index));
            data.append('expiration', expiration);
            data.append('awards', awards);
            
            category.forEach((item, index) => { data.append(`category[${index}]`, String(item)); });
            genre.forEach((item, index) => { data.append(`genre[${index}]`, String(item)); })
            
            data.append('record_company_id', String(record_company_id));
            data.append('artist_id', String(artist_id));
            data.append('album_id', String(album_id));

            if (params.id) {
                if (Number(params.id) > 0) {
                    data.append('id', params.id);
                    api.post('auth/updateApprovePodcast', data,
                        { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                            if (response.data.result) {
                                message('Podcast atualizado com sucesso');
                                window.scrollTo(0, 0);
                            }
                            else message(response.data.response);
                        });
                } else {
                    data.append('id', String(Number(params.id) * -1));
                    api.post('auth/updatePodcast', data,
                        { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                            if (response.data.result) {
                                message('Podcast atualizado com sucesso');
                                window.scrollTo(0, 0);
                            }
                            else message(response.data.response);
                        });
                }
            } else {
                api.post('auth/registerPodcast', data,
                    { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                        if (response.data.result) {
                            message('Podcast cadastrado com sucesso');
                            resetAddPodcast();
                            window.scrollTo(0, 0);
                        }
                        else message(response.data.response);
                    });
            }
        }
    }
    // BEGGIN:: GETTERS API
    function getCategories() {
        api.get('auth/indexCategory/podcast', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setCategories(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getGenres() {
        api.get('auth/indexGenre/podcast', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setGenres(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getRecordCompanies() {
        api.get('auth/indexRecordCompany', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setRecordCompanies(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getArtists() {
        api.get('auth/indexArtist', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setArtists(response.data.response);
            }
            else message(response.data.response);
        });
    }
    function getAlbums() {
        api.get('auth/indexAlbum', { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                setAlbums(response.data.response);
            }
            else message(response.data.response);
        });
    }

    function getGallery(type: string) {
        api.get(`auth/getGallery/${type}`, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) setGallery(response.data.response);
        });
    }
    function getPodcastById(id: string) {
        api.get(`auth/getPodcastById/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { fillAddPodcast(response.data.response) }
                else message(response.data.response);
            });
    }
    function getApprovePodcastById(id: string) {
        api.get(`auth/getApprovePodcastById/${id}`, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { fillAddPodcast(response.data.response) }
                else message(response.data.response);
            }); 
    }
    // END:: GETTERS API | BEGGIN: GALLERY 
    let modalPayment: any;
    function openModalGallery(type: string, inputFromG?: InputFromGallery) {
        getGallery(type);
        setGalleryType(type);

        if (inputFromG) setInputFromGallery(inputFromG);
        else setInputFromGallery(null);

        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "100%";
        modalPayment.style.opacity = "1";
        modalPayment.style.display = "block";
    }
    function closeModalGallery() {
        modalPayment = document.getElementById('modal-gallery')
        modalPayment.style.width = "0";
        modalPayment.style.opacity = "0";
        modalPayment.style.display = "none";
        setGallery([]);
    }
    function selectedImage(image_url: string) {
        switch (galleryType) {
            case 'thumbnail': setThumbnail(image_url); break;
            case 'wallpaper': setWallpaper(image_url); break;
            default: message('Galeria inválida'); break;
        }
        closeModalGallery();
    }
    function submitUploadImages() {
        const data = new FormData();

        data.append('type', galleryType);
        uploadImages.forEach((image, index) => {
            data.append(`images[${index}]`, image);
        });

        api.post('auth/uploadImages', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    message(response.data.response);
                    getGallery(galleryType);
                }
                else message(response.data.response);
            });
    }
    function deleteFromGallery(image_url: string) {
        let arr_image = image_url.split('/');
        if(arr_image.length >= 2){
            let type = arr_image[arr_image.length - 2];
            let filename = arr_image[arr_image.length - 1];

            api.get(`auth/deleteFromGallery/${type}&${filename}`,
            { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                if (response.data.result) {
                    message(response.data.response);
                    getGallery(galleryType);
                }
                else message(response.data.response);
            });
        }
        else message('Endereço de imagem inválido!');
    }
    // END:: GALLERY
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }

    return (
        <div id="container__insertPodcast">
            <Header />
            <div className="insert-podcast__content">
                <div className="podcast__content__salutation">
                    <div>
                        <span className="blue-left-border">Oi {user.first_name}</span>
                        <p>Complete os campos a baixo para {params.id ? 'atualizar' : 'adicionar'} um podcast.</p>
                    </div>

                    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-end'}}>
                        <Link to="/inserir-midia">Voltar</Link>
                        <Link to="/podcasts-cadastrados" style={{ color: '#000', borderBottom: '2px solid #dcdcdc', paddingBottom: '.15rem', marginTop: '.4rem' }}>Podcasts Cadastrados</Link>
                    </div>
                </div>
            </div>

            <div className="container_input-inser-podcast">
                <h4>
                    DADOS DO PODCAST
                </h4>
                <div className="group-input-label">
                    <label htmlFor="Purl">Áudio</label>
                    <input type="file" name="Purl" id="Purl" ref={inputAudio} onChange={event => {
                        if(event.target.files) setUrl(event.target.files[0])
                    }}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pgravadora">Gravadora</label>
                    <select name="gravadora" id="gravadora" onChange={event => setRecordCompanyId(Number(event.target.value))} 
                        value={record_company_id}>
                        <option value={0}>Selecione a Gravadora</option>
                        {record_companies.map((company) => { return (
                            <option value={company.id} key={company.id}>{company.name}</option>
                        );})}
                    </select>
                </div>
                <div className="group-input-label">
                    <label htmlFor="artista1">Artista</label>
                    <select name="artista1" id="artista1" onChange={event => setArtistId(Number(event.target.value))}
                        value={artist_id}>
                        <option value={0}>Selecione o Artista</option>
                        {artists.map((artist) => { return (
                            <option value={artist.id} key={artist.id}>{artist.name}</option>
                        );})}
                    </select>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pserie-album">Serie-Álbum</label>
                    <select onChange={event => setAlbumId(Number(event.target.value))} value={album_id}>
                        <option value={0}>Selecionar Série/Álbum</option>
                        {albums.map((album) => { return (
                            <option value={album.id} key={album.id}>{album.name}</option>
                        );})}
                    </select>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pepisodio">Episódio</label>
                    <input type="number" name="Pepisodio" id="Pepisodio"
                        value={episode_index} onChange={event => setEpisodeIndex(Number(event.target.value))}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Ptitulo">Título original</label>
                    <input type="text" name="Ptitulo" id="Ptitulo"
                        value={name} onChange={ event => setName(event.target.value) }/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pduracao">Duração</label>
                    <input type="time" name="Pduracao" id="Pduracao"
                        value={duration} onChange={event => setDuration(event.target.value)}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Placamento">Ano de lançamento</label>
                    <input type="date" name="Placamento" id="Placamento"
                        value={release} onChange={event => setRelease(event.target.value)}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Ppais">País</label>
                    <input type="text" name="Ppais" id="Ppais"
                        value={country} onChange={event => setCountry(event.target.value)}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pproducao">Produção</label>
                    <input type="text" name="Pproducao" id="Pproducao"
                        value={production} onChange={event => setProduction(event.target.value)}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pedicao">Edição</label>
                    <input type="text" name="Pedicao" id="Pedicao"
                        value={edition} onChange={event => setEdition(event.target.value)}/>
                </div>
                <div className="podcast-txt-area">
                    <label htmlFor="txtarea">Sobre o episódio</label>
                    <textarea value={description} onChange={event => setDescription(event.target.value)}>
                    </textarea>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Ppremios">Prêmios/Indicações</label>
                    <input type="text" name="Ppremios" id="Ppremios"
                        value={awards} onChange={event => setAwards(event.target.value)}/>
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pencerramento">Data de encerramento de contrato</label>
                    <input type="date" name="Pencerramento" id="Pencerramento" 
                        value={expiration} onChange={event => setExpiration(event.target.value)}/>
                </div>
                <div className="podcast__categoria-check-box">
                    <h4>Gênero</h4>
                    {genres.map((option) => {
                        return (
                            <div className="group-checkbox-podcast" key={option.id}>
                                <input type="checkbox" name='checkGenres'
                                    value={option.id} id={`checkGenre-${option.id}`}/>
                                <label>{option.name}</label>
                            </div>
                        );
                    })}    
                </div>
                <div className="podcast__categoria-check-box">
                    <h4>Categorias</h4>
                    {categories.map((option) => {
                        return (
                            <div className="group-checkbox-podcast" key={option.id}>
                                <input type="checkbox" name='checkCategories'
                                    value={option.id} id={`checkCategory-${option.id}`}/>
                                <label>{option.name}</label>
                            </div>
                        );
                    })}    
                </div>
                <div className="group-input-label">
                    <label htmlFor="Pclip">Vídeo Clipe (id vimeo)</label>
                    <input type="text" name="Pclip" id="Pclip"
                        value={clip_url} onChange={event => setClipUrl(event.target.value)}/>
                </div>
            </div>
            <div className="podcas-image">
                <h4>Imagens</h4>
                <div className="img-podcast-type">
                    <span>Capa do álbum</span>
                    <input placeholder="link da imagem" type="text" name="link-img" id="link-img"
                        value={thumbnail} onChange={event => setThumbnail(event.target.value)}/>
                    <button>acessar galeria</button>
                </div>
                <div className="img-podcast-type">
                    <span>Capa (paisagem)</span>
                    <input placeholder="link da imagem" type="text" name="link-img"
                        value={wallpaper} onChange={event => setWallpaper(event.target.value)}/>
                    <button>acessar galeria</button>
                </div>
                {/*
            <div className="img-podcast-type">
                <span>Capa extra com artista principais</span>
                <input placeholder="link da imagem" type="text" name="link-img" id="link-img" />
                <button>acessar galeria</button>
            </div>
            <div className="img-podcast-type">
                <span>Imagem destaque para Carrossel</span>
                <input placeholder="link da imagem" type="text" name="link-img" id="link-img" />
                <button>acessar galeria</button>
            </div>
            <div className="img-podcast-type">

                <span> Imagem de anuncio (banner - paisagem)</span>
                <input placeholder="link da imagem" type="text" name="link-img" id="link-img" />
                <button>acessar galeria</button>
            </div>
            <div className="img-podcast-type">
                <span>Imagem de anuncio (banner – vertical)</span>
                <input placeholder="link da imagem" type="text" name="link-img" id="link-img" />
                <button>acessar galeria</button>
            </div>
            <div className="img-podcast-type">
                <span>Canva</span>
                <input placeholder="link da imagem" type="text" name="link-img" id="link-img" />
                <button>acessar galeria</button>
            </div> */}

            </div>
            <div className="group__btn-save-podcast">
                <button> Cancelar</button>
                <button className="btn btn-save-podcast" onClick={handleCheckCategoryAndGenre}>
                    <span>{params.id ? 'Atualizar' : 'Salvar'}</span>
                </button>
            </div>
        </div>
    );


}

export default InsertPodcast;
import React from 'react';
import { Link } from 'react-router-dom';


function Checkout(){
  return(
    <div id="checkout-container2">
        <div className="content-checkout">
            <h1>insira seus detalhes de pagamento</h1>
            <p>Lorem Ipsum é simplesmente um texto fictício da indústria de impressão e composição.</p>
            <br/>
            <div className="secure-payment">Pagina potegida  <i className="fas fa-lock"></i></div>
            <div className="option-payment">Pagamento com cartão de credito <i className="fas fa-caret-right"></i> </div>
            <div className="option-payment">Boleto Bancario <i className="fas fa-caret-right"></i></div>
            <div className="option-payment">Pix <i className="fas fa-caret-right"></i></div>
        </div>
    </div>
  );
}

export default Checkout;
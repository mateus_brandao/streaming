import React, { ReactNode, useRef, useState } from 'react';

interface SliderProps {
 children: ReactNode;
}

function Slider({children}:SliderProps) {
    let slider: number;
    slider = 210;
    let x: number;


    const [scrolX, setScrolX] = useState(0)
    const Scroll = useRef<HTMLDivElement>(null);
    const leftSlider = () => {

        if (Scroll.current) {
            x = scrolX + slider;
            Scroll.current.scrollTo(x, 0);
            setScrolX(x)
        }
    }

    const rigthSlider = () => {
        if (Scroll.current) {
            if (scrolX > 0) {
                x = scrolX - slider;
                Scroll.current.scrollTo(x, 0);
                setScrolX(x)
            }
        }

    }

    return (
    
            <div className="container-my-carousel">
                <div ref={Scroll} className="my-carousel-sroll" style={{}}>
                    <div className="my-carousel">
                        {children}
                    </div>
                    <div className="fim-carrosel"></div>
                </div>
                <div onClick={rigthSlider} className="btn-my-carrosel-left">
                    <button><i className="material-icons">chevron_left</i></button>
                </div>
       
                <div onClick={leftSlider} className="btn-my-carrosel-rigth">
                    <button><i className="material-icons">navigate_next</i></button>
                </div>
            </div>

    );
}

export default Slider;
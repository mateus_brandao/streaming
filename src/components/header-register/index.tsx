import React, { useContext, useEffect } from 'react';
import { useHistory } from 'react-router';
import logowite from '../../assets/imagens/Logo/SVG/branco.svg';
import { GeneralContext } from '../../contexts/GeneralContext';
import { RegisterContext } from '../../contexts/RegisterContext';
import api from '../../services/api';


function HeaderRegister() {
    const history = useHistory();
    const { email, setEmail } = useContext(RegisterContext);
    const { setOpen, setMessage } = useContext(GeneralContext);

    useEffect(() => {
        if (sessionStorage.getItem('mag_email') != null) {
            let temp = sessionStorage.getItem('mag_email') || '';
            api.get(`indexUserByEmail/${temp}`).then(response => {
                console.log(response.data);
                if (response.data.result) {
                    message('Este email já está sendo utilizado');
                    history.push('/');
                }
                else setEmail(temp);
            });

        } else history.push('/');
    }, []);

    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    return (
        <>
            <div className="container_logo">

                <div className="content-logo_left">

                    <div className="logo-ajust">
                        <a href="/">
                            <img src={logowite} alt="" />
                        </a>
                    </div>
                    <div className="logo_flex-item">
                        <div className="logo-border"></div>
                        <span>A Magnificat é para você. <br /> Sua experiência será personalizada</span>
                    </div>
                </div>
                <div>
                    <small>{email}</small>
                </div>
            </div>
        </>

    );
}
export default HeaderRegister;
import React, { useContext, useEffect, useState } from 'react';
import { GeneralContext } from '../../contexts/GeneralContext';

import './styles.css';

interface ModalProps{
    message: string;
    open: boolean;
}
function ModalMessage() {
    const {open, setOpen, message} = useContext(GeneralContext);
    return (
        <>
        {open && (
        <div className="overlay">
            <div className="container">
                <div style={{position: 'absolute', top: '.5rem', left: '.5rem', right: '.5rem'}}>
                <button className="closeModal" type="button" onClick={()=>setOpen(false)}>X</button>
                <h3 style={{margin: 0, padding: 0, textAlign: 'left'}}>MAGNIFICAT</h3>
                </div>
                
                <div className="container-body">
                    <p>{message}</p>
                </div>
            </div>
        </div>
        )}
        </>
    );
}
export default ModalMessage;
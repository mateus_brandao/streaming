import React, { ReactNode, useEffect, useState } from 'react';
import { getNodeMajorVersion, idText } from 'typescript';
import DefaltTubnal from '../../assets/imagens/magnificatbase-01.jpg';
import api from '../../services/api';
import './index.css'



const Carousel = (props) => {
    const [clientWidth, setClientWidth] = useState(0);
    const [windowWidth, setWindowWidth] = useState(0);
    const [qtdMovie, setQtdmovie] = useState(0);
    const [scrollX, setScrollX] = useState(0);
    
    const [scheduleOne, setScheduleOne] = useState(false);
    const [scheduletwo, setScheduletwo] = useState(false);
    const [schedulethree, setSchedulethree] = useState(false);
    const [schedulefour, setsChedulefour] = useState(false);
    const [animate, setAnimate] = useState(0)

  

    const filme = [
        { name: 'teste', id: 0 },
        { name: 'teste', id: 2 },
        { name: 'teste', id: 3 },
        { name: 'teste', id: 4 },
        { name: 'teste', id: 5 },
        { name: 'teste', id: 6 },
        { name: 'teste', id: 7 },
        { name: 'teste', id: 8 },
        { name: 'teste', id: 9 },
        { name: 'teste', id: 10 },
        { name: 'teste', id: 11 },
        { name: 'teste', id: 13 },
        { name: 'teste', id: 14 },
        { name: 'teste', id: 15 },
        { name: 'teste', id: 16 },
        { name: 'teste', id: 17 },
        { name: 'teste', id: 18 },
        { name: 'teste', id: 19 },
        { name: 'teste', id: 20 },
        { name: 'teste', id: 21 },
        { name: 'teste', id: 22 },
    ]

    useEffect(() => {
        getClientWidth()
        getMovie()

    }, [])
    function getClientWidth() {
        let element: any;
        let movieRow: any;
        element = document.querySelector('.movieRow--item');
        movieRow = document.querySelector('.movieRow');
        setClientWidth(element.clientWidth);
        setWindowWidth(movieRow.clientWidth);
    }
    function getMovie() {
        setQtdmovie(filme.length)
    }
    

    const handleLeftArrow = () => {
        let x = scrollX + Math.round(windowWidth / 2);
        if (x > 0) {
            x = 0
        }
        setScrollX(x)

    }
    const handleRigthArrow = () => {
        let x = scrollX - Math.round(windowWidth / 2);
        let listW = qtdMovie * clientWidth;
        if ((windowWidth - listW) > x) {
            x = (windowWidth - listW);

        }
        setScrollX(x);
        
    }

    return (
        <>
            <div className="movieRow">
                <div className="progress-schedule">
                    <div
                        style={{
                            backgroundColor: `${scheduleOne ? '#333' : 'red'}`,
                        }}
                        className="schedule">
                    </div>
                    <div
                        style={{
                            backgroundColor: `${scheduletwo ? '#333' : 'red'}`,
                        }}
                        className="schedule">
                    </div>
                    <div
                        style={{
                            backgroundColor: `${schedulethree ? '#333' : 'red'}`,
                        }}
                        className="schedule">
                    </div>
                    <div
                        style={{
                            backgroundColor: `${schedulefour ? '#333' : 'red'}`,
                        }}
                        className="schedule">
                    </div>

                </div>
                <div className="movieRow--left" onClick={handleLeftArrow}>
                    <i className="fas fa-chevron-left ico-control"></i>
                </div>
                <div className="movieRow--rigth" onClick={handleRigthArrow}>
                    <i className="fas fa-chevron-right ico-control"></i>
                </div>

                <div className="movieRow--listarea">
                    <div className="movieRow--list"
                        style={{
                            width: qtdMovie * clientWidth,
                            marginLeft: scrollX,
                        }}
                    >
                        {filme.map(movie => {
                            return (
                                <div key={movie.id} className="movieRow--item">
                                    <img src={DefaltTubnal} alt="" />

                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
}
export default Carousel;
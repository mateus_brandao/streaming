import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import logoWite from '../../assets/imagens/Logo/SVG/branco.svg';
import api from '../../services/api';


// import './style.css';
// import './custom.css';
// import './layout.css';
interface User {
    first_name: string;
    last_name: string;
    email: string;
    permission: string;
}
function Header() {
    const history = useHistory();
    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'user' });
    const [access_token, setAccessToken] = useState("");

    useEffect(() => {
        if (sessionStorage.getItem('magnificat_access') == null) history.push('/');
        else setAccessToken(sessionStorage.getItem('magnificat_access') || "");

        window.scrollTo(0, 0);
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getUser();
        }
    }, [access_token]);

    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
                // if(response.data.permission === 'user') history.push('/reproduzir-default');

                //"https://magnificat.video/live/"; 
            }
        });
    }
    function logout() {
        localStorage.removeItem('magnificat_access');
        sessionStorage.removeItem('magnificat_access');

        api.post('auth/logout', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => { console.log(response.data); });

        history.push('/');
    }
    let navMobile: any;
    navMobile = document.querySelector('.nav-mobile');
    function openNavMobile() {

        navMobile.style.width = '100%'
    };
    function closeNavMobile() {
        navMobile.style.width = '0'
    };

    return (
        <header>
            <div className="container-header">
                <i className="fas fa-bars" onClick={openNavMobile}></i>
                <div className="header_content-left">

                    <div className="logo-header">
                        <div className="cut-img-nav" ><Link to="/inicio"><img src={logoWite} alt="" /></Link></div>
                    </div>
                    <nav>
                        <ul>
                            <li className="nav-font-size"><Link to="#">Filmes</Link></li>
                            {/* <li className="nav-font-size"><Link to="#">Séries</Link></li>
                            <li className="nav-font-size"><Link to="#">Infantil</Link></li>
                            <li className="nav-font-size"><Link to="#">Ebooks</Link></li>
                            <li className="nav-font-size"><Link to="#">Músicas & Podcast</Link></li> */}
                        </ul>
                    </nav>


                </div>

                <div className="container_not">
                    <i className="fas fa-search busca"></i>
                    <i className="fas fa-bell sino"></i>
                    
                    <div className="drop-user" style={{ fontFamily: "'Inter', sans-serif" }}>
                        <i className="far fa-user"></i>
                        <i className="fas fa-sort-up"></i>
                        <div className="menu-user">
                            <div className="menu-seur__perfil-acess-acont">
                                <Link to="/painel-de-controle">Conta</Link>
                                <Link to="/pagina-em-desenvolvimento">Centro de ajuda</Link>
                                {user.permission != 'user' && (
                                    <Link to="/painel-de-controle-adm">Dashboard</Link>
                                )}
                                <a href="#" onClick={logout}>Sair</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="nav-mobile">
                <i className="far fa-times-circle" onClick={closeNavMobile}></i>
                <ul>
                    <li>
                        <Link to="#">Filmes</Link>
                    </li>

                    {/* <li>
                        <Link to="#">Séries</Link>
                    </li>

                    <li>
                        <Link to="#">Infantil</Link>
                    </li>

                    <li>
                        <Link to="#">Ebooks</Link>
                    </li>
                    <li>
                        <Link to="#">Músicas & Podcast</Link>
                    </li> */}
                    <li className="mobile-search">
                        <input type="text" name="" id="" />
                        <i className="fas fa-search"></i>
                   
                    </li>
                    <li className="mobile-notification">
                    <i className="fas fa-bell"></i>
                    </li>

                </ul>
            </div>
        </header>


    );
}

export default Header;
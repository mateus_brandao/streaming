import React, { useContext } from 'react';
import { RegisterContext } from '../../contexts/RegisterContext';

function RegisterTwo() {
    const {
        toStep,
        firstName,
        password,
        setPassword,
        confirmPassword,
        setConfirmPassword
    } = useContext(RegisterContext);

    return (
        <div id="register-pass2">
            <div className="content_register">
                <div className="aline-number">
                    <div className="numbe-aliner-content"><span className="number-register">2</span> <p>{firstName}, crie uma senha para <br /> iniciar sua assinatura.</p></div>
                </div>
                <div className="container_input-register">
                    <div id="R-order1" className="input_group">
                        <label htmlFor="Fsenha">Senha</label>
                        <input type="password" name="Fsenha" id="Fsenha"
                            value={password} onChange={event => setPassword(event.target.value)} />
                    </div>
                    <div id="R-order2" className="input_group">
                        <label htmlFor="">Confirme sua senha.</label>
                        <input type="password" name="Csenha" id="Csenha"
                            value={confirmPassword} onChange={event => setConfirmPassword(event.target.value)} />
                    </div>
                    <div id="R-order3" className="input_group-txt">
                        <span>
                            - Não use espaço<br />
                            - Utilize no minimo 8 caracteres; <br />
                            - Para uma senha forte caracteres alfanuméricos e símbolos. <br />
                        </span>
                    </div>

                </div>
                <div className="alignRecordButton">
                    <div className="container_btn">
                        <button type="button" onClick={() => toStep(1)} className="btn-v"><span>Anterior</span></button>
                        <button type="button" onClick={() => toStep(3)} className="btn"> <span>Próximo</span></button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default RegisterTwo;
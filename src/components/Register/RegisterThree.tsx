import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { RegisterContext } from '../../contexts/RegisterContext';

function RegisterThree() {
    const { toStep, subscriptionPlan, setSubscriptionPlan, dataSubscriptionPlan, startSubscription } = useContext(RegisterContext);

    return (
        <div id="register-pass3">
            <div className="container_register-pass3">
                <div className="aline-number">
                    <div className="numbe-aliner-content">
                        <span className="number-register">3</span><p>Escolha seu plano</p>
                    </div>
                    <div className="content_register-text table-c1">
                        Sem compromisso, cancele quando quiser. <br />
                        Todo o conteúdo da Magnificat por um preço único e acessível. <br />
                        Assista o quanto quiser em todos os seus aparelhos. <br />
                    </div>
                </div>
            </div>

            <div id="table_grid">
                <div className="table_grid-th border-table-rigth table-c1">
                    <div className="table_grid-td"></div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>Preço por mês</div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>Resolução</div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>Telas simultâneas</div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>
                        Assistir em PC, <br />celular, tablet e TV
                    </div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}> Conteúdos simultâneos</div>
                    <div className="table_grid-td">Cancele quando quiser</div>

                </div>
                {dataSubscriptionPlan.map(plan => {
                    if (plan.id == 4) return null;
                    return (
                        <div className="table_grid-th border-table-rigth" key={plan.id}>
                            <div className="table_grid-td th-destaque">{plan.name}</div>
                            <div className="table_grid-td border-table-bottom">R$ {plan.price}</div>
                            <div className="table_grid-td border-table-bottom">{plan.resolution}</div>
                            <div className="table_grid-td border-table-bottom">{plan.screens}</div>
                            <div className="table_grid-td border-table-bottom">sim</div>
                            <div className="table_grid-td border-table-bottom">Sim</div>
                            <div className="table_grid-td">sim</div>
                            {/* {plan.profiles} */}
                        </div>
                    );
                })}
                <div className="table-c1"></div>
                <div className="btn-p" style={{ marginTop: '1rem' }}>
                    <button type="button" onClick={() => setSubscriptionPlan(1)}
                        className={`btn-p2 ${subscriptionPlan == 1 ? 'active' : ''}`}>
                        <span>Escolha</span>
                    </button>
                </div>
                <div className="btn-p" style={{ marginTop: '1rem' }}>
                    <button type="button" onClick={() => setSubscriptionPlan(2)}
                        className={`btn-p2 ${subscriptionPlan == 2 ? 'active' : ''}`}>
                        <span>Escolha</span>
                    </button>
                </div>
                <div className="btn-p" style={{ marginTop: '1rem' }}>
                    <button type="button" onClick={() => setSubscriptionPlan(3)}
                        className={`btn-p2 ${subscriptionPlan == 3 ? 'active' : ''}`}>
                        <span>Escolha</span>
                    </button>
                </div>
            </div>
            <div className="aline-itens-buton">

                <div className="container__free-plan">
                    <div>
                        <span>Experimente grátis a Magnificat.</span><br />
                        <span className="obs__free-plan">*Acesso limitado ao conteúdo.</span>
                    </div>
                    <button type="button" onClick={startSubscription} >
                        Gratuito
                    </button>
                </div>

                <div className="container_btn">
                    <button type="button" onClick={() => toStep(2)} className="btn-v">
                        <span>Anterior</span>
                    </button>
                    <button type="button" onClick={() => toStep(4)} className="btn">
                        <span>Próximo</span>
                    </button>
                </div>

            </div>
            <div className="txt-bootom">
                Sem compromisso, cancele quando quiser. <br />
                Todo o conteúdo da Magnificat por um preço único e acessível. <br />
                Assista o quanto quiser em todos os seus aparelhos. <br />
            </div>

        </div>
    )
}

export default RegisterThree;
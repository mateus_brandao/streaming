import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { RegisterContext } from '../../contexts/RegisterContext';

function Credit() {
    const { 
        cpf,
        phone,

        setCpf,
        setPhone,

        cardName,
        cardNumber,
        cardValidity,
        cardCode,
        cardMethod,

        setCardName,
        setCardNumber,
        setCardValidity,
        setCardCode,
        setCardMethod,

        toStep,
        startSubscription,
        
        subscriptionPlan, 
        setSubscriptionPlan, 
        dataSubscriptionPlan

    } = useContext(RegisterContext);

    const [selectedSubscriptionPlan, setSelectedSubscriptionPlan] = useState(0);

    useEffect(() => {
        setSelectedSubscriptionPlan(dataSubscriptionPlan.findIndex(plan => plan.id == subscriptionPlan));
    },[subscriptionPlan]);

    return (
        <div id="register-pass4">
            <div className="container-top-credit">
                <div className="aline-number">
                    <div className="numbe-aliner-content"><span className="number-register">4</span><p> Informações de pagamento</p></div>
                </div>
            </div>
            <div className="pass4_checkalt">
                <div className="input0">Informe os Dados do seu cartão de crédito ou débito</div>
                <div className="checkaut-input">
                    <label htmlFor="cpf">CPF</label>
                    <input type="text" id="cpf" value={cpf} maxLength={14}
                        onChange={event => setCpf(event.target.value)}/>
                </div>
                <div className="checkaut-input">
                    <label htmlFor="phone">Telefone</label>
                    <input type="text" id="phone" value={phone} maxLength={15}
                        onChange={event => setPhone(event.target.value)}/>
                </div>
                <div className="checkaut-input">
                    <label htmlFor="card-name">Nome no Cartão</label>
                    <input type="text" id="card-name" value={cardName} 
                        onChange={event => setCardName(event.target.value)}/>
                </div>
                <div className="checkaut-input">
                    <label htmlFor="card-number">Numero do cartão de crédito</label>
                    <input type="text" id="card-number" value={cardNumber} maxLength={19}
                        onChange={event => setCardNumber(event.target.value)}/>
                </div>
                <div className="input4 checkaut-input">
                    <label htmlFor="card-validity">Validade</label>
                    <input type="text" id="card-validity" value={cardValidity} maxLength={7}
                        onChange={event => setCardValidity(event.target.value)}/>
                </div>
                <div className="input5 checkaut-input">
                    <label htmlFor="card-code">Código de Segurança</label>
                    <input type="text" id="card-code" value={cardCode} maxLength={3}
                        onChange={event => setCardCode(event.target.value)}/>
                </div>
                <div className="input6">Escolha sua forma de pagamento preferida</div>
                <div className="input7" style={{color: '#eee'}}>
                    <label>
                        <input type="radio" onClick={()=> setCardMethod('Crédito')} 
                            checked={cardMethod=='Crédito'?true:false}/>
                        Crédito
                    </label>
                    {/* <label>
                        <input type="radio" onClick={()=> setCardMethod('Débito')}
                            checked={cardMethod=='Débito'?true:false}/>
                        Débito
                    </label> */}
                </div>
            </div>
            {selectedSubscriptionPlan != dataSubscriptionPlan.length - 1 && (
                <div className="card-pass4 active" onClick={() => setSubscriptionPlan(
                    dataSubscriptionPlan[selectedSubscriptionPlan+1].id
                )}>
                    <div className="card-tilte">Após 30 dias</div>
                    <div className="escolha">Escolher</div>
                    <div className="card-main">R$ {dataSubscriptionPlan[selectedSubscriptionPlan+1].price}</div>
                    <div className="card-plan">Plano {dataSubscriptionPlan[selectedSubscriptionPlan+1].name}</div>
                </div>
            )}
            <div className={`card-pass4 ${selectedSubscriptionPlan == dataSubscriptionPlan.length-1?'active':'n-active'}`}>
                <div className="card-tilte">Após 30 dias</div>
                <div className="escolha">Escolheu</div>
                <div className="card-main">R$ {dataSubscriptionPlan[selectedSubscriptionPlan].price}</div>
                <div className="card-plan">Plano {dataSubscriptionPlan[selectedSubscriptionPlan].name}</div>
            </div>
            <div className="txt-botton">
                <p>Cartões que suportam transações de débito e de crédito
                poderão ser processados de ambas as formas. <br/> <br/><br/>

                Ao clicar no botão "Iniciar assinatura" abaixo, você concorda com
                nossos Termos de uso e com nossa Declaração de privacidade,
                confirma ter mais de 18 anos e aceita que a Magnificat renovará
                automaticamente sua assinatura e cobrará uma taxa mensal
                (atualmente R$xx,xx) da sua forma de pagamento. Você pode
                cancelar sua assinatura quando quiser para evitar novas cobranças.
                Para cancelar, acesse “Conta” e clique em "Cancelar assinatura".</p>
            </div>
            <div className="container-btn-pass4">
                <button type="button" onClick={startSubscription} className="btn-pass4 btn4-active">
                    iniciar assinatura</button>
                <button  type="button" onClick={()=> toStep(4)} className="btn-pass4 btn4-v">Anterior</button>
            </div>
        </div>

    );
}
export default Credit;
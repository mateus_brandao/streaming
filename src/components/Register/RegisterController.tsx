import React, { useContext, useEffect, useState } from 'react';
import { RegisterContext } from '../../contexts/RegisterContext';
import Footer from '../Footer';
import HeaderRegister from '../header-register';
import Credit from './Credit';
import Prepaid from './Prepaid';
import RegisterFour from './RegisterFour';
import RegisterOne from './RegisterOne';
import RegisterThree from './RegisterThree';
import RegisterTwo from './RegisterTwo';
function RegisterController() {
    const { step } = useContext(RegisterContext);

    React.useEffect(() => { window.scrollTo(0, 0); }, [step]);
    
    return (
        <div id="register_page1">
            <HeaderRegister/>
            {step==1&&(<RegisterOne/>)}
            {step==2&&(<RegisterTwo/>)}
            {step==3&&(<RegisterThree/>)}
            {step==4&&(<RegisterFour/>)}
            {step==5&&(<Credit/>)}
            {step==6&&(<Prepaid/>)}
            <Footer/>
        </div>
    );
}
export default RegisterController;
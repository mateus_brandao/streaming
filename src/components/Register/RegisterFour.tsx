import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { RegisterContext } from '../../contexts/RegisterContext';
import BtnBorderBlue from '../../components/Buttons/Button-border-blue'

function RegisterFour() {
    const { toStep, startSubscription } = useContext(RegisterContext);

    return (
        <div id="register-pass4">
            {/* <div className="mensagem-dev">
                <p>Os métodos de pagamentos estão em fase de teste, favor utilizar versão gratuita.</p>
            </div> */}
            <div className="content_register">
                <div className="aline-number">
                    <div className="numbe-aliner-content"><span className="number-register">4</span><p> Informações de pagamento</p></div>
                    <div className="content_register-text table-c1">
                        Sua assinatura começa assim que você configurar o pagamento. <br />
                        Sem compromisso. <br />
                        Cancele online quando quiser. <br />
                    </div>

                </div>
                <div className="container-btn-pass4">
                    <button type="button" onClick={startSubscription} className="btn-green">Versão gratuita.</button>
                    <button type="button" onClick={() => toStep(5)} className="btn-pass4 btn4-active">Cartão de crédito/Débito</button>
                    {/* <button type="button" disabled={true} onClick={() => toStep(6)} className="btn-pass4 btn4-v inative-btn">Cartão Magnificat Pré Pago</button> */}
                    <div className="bnt-voltar-border"><button type="button" onClick={() => toStep(3)} className="">Anterior</button></div>
                </div>
            </div>
            <div className="txt-bootom">
                Sua assinatura começa assim que você configurar o pagamento. <br />
                Sem compromisso. <br />
                Cancele online quando quiser. <br />
            </div>
        </div>
    );
}

export default RegisterFour;
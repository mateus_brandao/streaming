import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { RegisterContext } from '../../contexts/RegisterContext';

function RegisterOne() {
    const {
        toStep,
        firstName,
        setFirstName,
        lastName,
        setLastName,
    } = useContext(RegisterContext);
    const[checkOne, SetCheckOne] = useState(false);
    const[checkTwo, SetCheckTwo] = useState(false);
    const[disableBtn, setDisableBtn] = useState(true);
    
    useEffect(() => {
        if(checkOne === true && checkTwo == true){
            setDisableBtn(false)
        }else{
            setDisableBtn(true)
        }
    }, [checkOne,checkTwo]);


    return (
        <div className="content_register">
            <div className="aline-number">
                <div className="numbe-aliner-content" ><span className="number-register">1</span>Quem é você?</div>
            </div>
            <div className="container_input-register">
                <div id="R-order1" className="input_group">
                    <label htmlFor="">Nome</label>
                    <input placeholder="Seu nome" type="text" name="Fnome" id="Fnome"
                        value={firstName} onChange={event => setFirstName(event.target.value)} />
                </div>
                <div id="R-order2" className="input_group">
                    <label htmlFor="">Sobrenome</label>
                    <input placeholder="Seu Sobrenome" type="text"
                        value={lastName} onChange={event => setLastName(event.target.value)} />
                </div>

                <div id="R-order3" className="input_group-txt">
                    <span>
                        Faltam só mais alguns passos! <br />
                        Nós também detestamos formulários.
                    </span>
                </div>

            </div>
            <div className="alignRecordButton">
                <div className="accept-policy">
                    <Link to="/politica-de-privacidade">Conheça nossa politica de privacidade</Link> <br />
                    <div>
                        <input onClick={()=> SetCheckOne(!checkOne)} type="checkbox" name="" id="" />
                        <label htmlFor="">Aceitar regulamento Geral</label>
                    </div>
                    <div>
                        <input onClick={()=> SetCheckTwo(!checkTwo)} type="checkbox" name="" id="" />
                        <label htmlFor="">Aceitar Termos e Condições</label>
                    </div>
                </div>
                <div className="container_btn">
                    <button disabled={disableBtn} type="button" onClick={() => toStep(2)} className="btn"> <span> Próximo</span></button>
                </div>
            </div>
        </div>
    );
}
export default RegisterOne;
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { RegisterContext } from '../../contexts/RegisterContext';

function Prepaid() {
    const { 
        cardMagnificat,
        setCardMagnificat,

        toStep,
        startSubscription,

        subscriptionPlan, 
        setSubscriptionPlan, 
        dataSubscriptionPlan
    } = useContext(RegisterContext);

    const [selectedSubscriptionPlan, setSelectedSubscriptionPlan] = useState(0);

    useEffect(() => {
        setSelectedSubscriptionPlan(dataSubscriptionPlan.findIndex(plan => plan.id == subscriptionPlan));
    },[subscriptionPlan]);

    return (
        <div id="register-pass4">
            <div className="content_register">
                <div className="aline-number">
                    <div className="numbe-aliner-content"><span className="number-register">4</span><p> Informações de pagamento</p></div>
                </div>
            </div>
            <div className="content-page-prepago">
                <span>Informe o código do Cartão Magnificat Pré Pago</span>
                <input type="text" value={cardMagnificat} 
                    onChange={event => setCardMagnificat(event.target.value)}/>
                <span className="txt-rigth">Não tem o cartão Magnificat Pré Pago? 
                    <a href="#"> Saiba onde comprar</a> <br />
                    ou troque para cartão de crédito ou débito.
                </span>
            </div>
            {selectedSubscriptionPlan != dataSubscriptionPlan.length - 1 && (
                <div className="card-pass4 active" onClick={() => setSubscriptionPlan(
                    dataSubscriptionPlan[selectedSubscriptionPlan+1].id
                )}>
                    <div className="card-tilte">Após 30 dias</div>
                    <div className="escolha">Escolher</div>
                    <div className="card-main">R$ {dataSubscriptionPlan[selectedSubscriptionPlan+1].price}</div>
                    <div className="card-plan">Plano {dataSubscriptionPlan[selectedSubscriptionPlan+1].name}</div>
                </div>
            )}
            <div className={`card-pass4 ${selectedSubscriptionPlan == dataSubscriptionPlan.length-1?'active':'n-active'}`}>
                <div className="card-tilte">Após 30 dias</div>
                <div className="escolha">Escolheu</div>
                <div className="card-main">R$ {dataSubscriptionPlan[selectedSubscriptionPlan].price}</div>
                <div className="card-plan">Plano {dataSubscriptionPlan[selectedSubscriptionPlan].name}</div>
            </div>
            <div className="txt-botton">
                <p>
                    Ao clicar no botão "Iniciar assinatura" abaixo, você concorda com
                    nossos Termos de uso e com nossa Declaração de privacidade,
                    confirma ter mais de 18 anos e aceita que a Magnificat renovará
                    automaticamente sua assinatura e cobrará uma taxa mensal
                    (atualmente R$xx,xx) da sua forma de pagamento. Você pode
                    cancelar sua assinatura quando quiser para evitar novas cobranças.
                    Para cancelar, acesse “Conta” e clique em "Cancelar assinatura".
                </p>
                <div className="container-btn-pass4">
                    <button type="button" onClick={startSubscription} className="btn-pass4 btn4-active ">Resgatar e iniciar assinatura</button>
                    <button  type="button" onClick={()=> toStep(4)} className="btn-pass4 btn4-v">Anterior</button>
                </div>
            </div>
        </div>
    );
}

export default Prepaid;
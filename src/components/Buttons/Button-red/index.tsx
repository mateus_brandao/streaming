import React, { ButtonHTMLAttributes } from "react";
import { Link } from 'react-router-dom';
import './index.css'

interface ButtonPros extends ButtonHTMLAttributes <HTMLButtonElement>{
  name: string;
}

const ButtonRed: React.FC<ButtonPros> = (props) => {
    return(
      <button className="button-red" {...props}>
        {props.name}
      </button>
    );
}
export default ButtonRed;

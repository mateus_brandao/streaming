import React, { ButtonHTMLAttributes } from "react";
import { Link } from 'react-router-dom';
import './index.css'

interface ButtonPros extends ButtonHTMLAttributes <HTMLButtonElement>{
  icon: string;
  danger?: boolean;
}

const ButtonIcon: React.FC<ButtonPros> = (props) => {
    return(
        <div className={`button-icon ${props.danger && props.danger==true ? 'danger':''}`}>
          <button {...props} className={`${props.danger && props.danger==true ? 'danger':''}`}>
              <i className={props.icon}></i>
          </button>
        </div>

    );
}

export default ButtonIcon;
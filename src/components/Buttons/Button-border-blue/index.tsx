import React, { ButtonHTMLAttributes } from "react";
import { Link } from 'react-router-dom';
import './index.css'

interface ButtonPros extends ButtonHTMLAttributes <HTMLButtonElement>{
  name: string;
}

const ButtonsBorderBlue: React.FC<ButtonPros> = (props) => {
    return(
        <div className="border-blue">
          <button {...props}>
            {props.name}
          </button>
        </div>

    );
}

export default ButtonsBorderBlue;
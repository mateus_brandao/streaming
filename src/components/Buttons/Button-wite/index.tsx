import React from "react";
import { Link } from 'react-router-dom';
import './index.css'

interface ButtonPros {
  name: string;
  route: string;
}
const Buttonwite: React.FC<ButtonPros> = (props) => {
    return(
        <Link to={props.route} className="Btn-wite">{props.name}</Link>
    );
}

export default Buttonwite;
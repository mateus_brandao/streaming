import React from "react";
import { Link } from 'react-router-dom';
import './index.css'


interface NavPros {


}

const DashboardNav: React.FC<NavPros> = (props) => {


    function navAbas(n: number) {
        let aba: any
        let qtdAba: number
        let current: number
        aba = document.querySelectorAll('#container-aba-adm li')
        current = n;
        qtdAba = aba.length;
        if (current === 0) {
            aba[0].classList.add("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 1) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.add("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 2) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.add("adm-active");
            aba[3].classList.remove("adm-active");
        } else if (current === 3) {
            aba[0].classList.remove("adm-active");
            aba[1].classList.remove("adm-active");
            aba[2].classList.remove("adm-active");
            aba[3].classList.add("adm-active");
        }

    }
    return (
        <div id="dashboard-nav">
            <nav className="dashboard-nav__nav">
                <ul>
                    <li onClick={() => navAbas(0)}><a href="#">Dashboard</a></li>
                    <li onClick={() => navAbas(1)}><a href="#">add filme</a></li>
                    <li onClick={() => navAbas(2)}><a href="#">Filmes cadastrados</a></li>
                    <li onClick={() => navAbas(3)}><a href="#">Perfil</a></li>
                </ul>
            </nav>
            <ul id="container-aba-adm">
                <li className="dashboard-adm__aba adm-active">
                    <span> Conteudo 1</span>
                </li>
                <li className="dashboard-adm__aba">
                    <div className="content_add-movie">
                        <h5>Adicionar filme</h5>
                        <form>
                            <div className="adm_agroup-input">
                                <label htmlFor="id-vimeo">Id filme vimeo</label>
                                <input type="number" name="id-vimeo" id="id-vimeo" />
                            </div>
                            <div className="adm_agroup-input">
                                <label htmlFor="id-vimeo-trailler">Id traller vimeo</label>
                                <input type="number" name="id-vimeo-trailler" id="id-vimeo-trailler" />
                            </div>
                            <div className="adm_agroup-input">
                                <label htmlFor="MovieTitle">Titulo do filme</label>
                                <input type="text" name="MovieTitle" id="MovieTitle" />
                            </div>
                            <div className="adm_agroup-input">
                                <label htmlFor="indicatedAge">Classificalçao indicativa</label>
                                <input type="number" name="indicatedAge" id="indicatedAge" />
                            </div>
                            <div className="adm_agroup-input input-movieTime">
                                <label htmlFor="movieTime">tempo de duração do filme</label>
                                <input type="number" name="movieTime" id="movieTime" />
                            </div> 
                            <div className="adm_agroup-input">
                                <label htmlFor="genre">Genero</label>
                                <input placeholder=" EX: comédia, ação, aventura" type="text    " name="genre" id="genre" />
                            </div> 

                        </form>
                    </div>
                </li>
                <li className="dashboard-adm__aba">
                    <span>Conteudo 3</span>
                </li>
                <li className="dashboard-adm__aba">
                    <span>Conteudo 4</span>
                </li>
            </ul>
        </div>
    );
}
export default DashboardNav;
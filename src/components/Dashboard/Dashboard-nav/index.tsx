import React from "react";
import { Link } from 'react-router-dom';
import './index.css'


interface NavPros {
  
  
}

const DashboardNav: React.FC<NavPros> = (props) => {
    function controlNavUser(n:number){
        let aba: any
        let qtdAba: number
        let current: number
        aba = document.querySelectorAll('#nav-user li')
        current = n;
        qtdAba = aba.length;
        if (current === 0) {
            aba[0].classList.add("active-tab-user");
            aba[1].classList.remove("active-tab-user");
            aba[2].classList.remove("active-tab-user");
            aba[3].classList.remove("active-tab-user");
        } else if (current === 1) {
            aba[0].classList.remove("active-tab-user");
            aba[1].classList.add("active-tab-user");
            aba[2].classList.remove("active-tab-user");
            aba[3].classList.remove("active-tab-user");
        } else if (current === 2) {
            aba[0].classList.remove("active-tab-user");
            aba[1].classList.remove("active-tab-user");
            aba[2].classList.add("active-tab-user");
            aba[3].classList.remove("active-tab-user");
        } else if (current === 3) {
            aba[0].classList.remove("active-tab-user");
            aba[1].classList.remove("active-tab-user");
            aba[2].classList.remove("active-tab-user");
            aba[3].classList.add("active-tab-user");
        }

    }
    return(
            
                <nav className="dashboard-nav__nav">
                    <ul >
                        <li onClick={() => controlNavUser(0)}>Informações da conta</li>
                        <li onClick={() => controlNavUser(1)}>Perfis</li>
                        <li onClick={() => controlNavUser(2)}>Assinatura e pagamento</li>
                        <li onClick={() => controlNavUser(3)} >Configurações</li>
                    </ul>
                </nav>
            
    );
}
export default DashboardNav;
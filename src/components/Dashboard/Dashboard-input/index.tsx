import React, {InputHTMLAttributes} from "react";
import { Link } from 'react-router-dom';
import './index.css'

interface InputProps extends InputHTMLAttributes <HTMLInputElement> {
    name: string;
    Label: string;
}

const Input: React.FC<InputProps> = (props) =>{
    return(
        <div className="input_-dashboard">
            <label htmlFor={props.name}>{props.Label}</label>
            <input {...props}/>
        </div>
    );
}
export default Input;

import React, { useContext, useState } from 'react';

function changePlan() {

    return (
        <div id="register-pass3">
            <div className="content_register">
                <div className="aline-number">
                    <div className="numbe-aliner-content">
                        <span className="number-register">1</span><p>Escolha seu plano</p>
                    </div>
                    <div className="content_register-text table-c1">
                        Sem compromisso, cancele quando quiser. <br />
                        Todo o conteúdo da Magnificat por um preço único e acessível. <br />
                        Assista o quanto quiser em todos os seus aparelhos. <br />
                    </div>
                </div>
            </div>

            <div id="table_grid">
                <div className="table_grid-th border-table-rigth table-c1">
                    <div className="table_grid-td"></div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>Preço por mês</div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>Resolução</div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>Telas simultâneas</div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}>
                        Assistir em PC, <br />celular, tablet e TV
                    </div>
                    <div className="table_grid-td border-table-bottom" style={{ textAlign: 'center' }}> Conteúdos simultâneos</div>
                    <div className="table_grid-td">Cancele quando quiser</div>

                </div>

                <div className="table_grid-th border-table-rigth">
                    <div className="table_grid-td th-destaque">Básico</div>
                    <div className="table_grid-td border-table-bottom">R$ 9.90</div>
                    <div className="table_grid-td border-table-bottom">480p</div>
                    <div className="table_grid-td border-table-bottom">1</div>
                    <div className="table_grid-td border-table-bottom">sim</div>
                    <div className="table_grid-td border-table-bottom">Sim</div>
                    <div className="table_grid-td">sim</div>
                    {/* {plan.profiles} */}
                </div>
                <div className="table_grid-th border-table-rigth">
                    <div className="table_grid-td th-destaque">Padrão</div>
                    <div className="table_grid-td border-table-bottom">R$ 32.90</div>
                    <div className="table_grid-td border-table-bottom">1080p</div>
                    <div className="table_grid-td border-table-bottom">2</div>
                    <div className="table_grid-td border-table-bottom">sim</div>
                    <div className="table_grid-td border-table-bottom">Sim</div>
                    <div className="table_grid-td">sim</div>
                    {/* {plan.profiles} */}
                </div>
                <div className="table_grid-th border-table-rigth">
                    <div className="table_grid-td th-destaque">Premium</div>
                    <div className="table_grid-td border-table-bottom">R$ 45.90</div>
                    <div className="table_grid-td border-table-bottom">4K+HDR</div>
                    <div className="table_grid-td border-table-bottom">4</div>
                    <div className="table_grid-td border-table-bottom">sim</div>
                    <div className="table_grid-td border-table-bottom">Sim</div>
                    <div className="table_grid-td">sim</div>
                    {/* {plan.profiles} */}
                </div>

                <div className="table-c1"></div>
                <div className="btn-p" style={{ marginTop: '1rem' }}>
                    <button type="button" className="btn-p2 ">
                        <span>Escolha</span>
                    </button>
                </div>
                <div className="btn-p" style={{ marginTop: '1rem' }}>
                    <button type="button" className="btn-p2">
                        <span>Escolha</span>
                    </button>
                </div>
                <div className="btn-p" style={{ marginTop: '1rem' }}>
                    <button type="button"
                        className="btn-p2">
                        <span>Escolha</span>
                    </button>
                </div>
            </div>
            <div className="alignRecordButton">
                <div className="container_btn">
                    <button type="button" className="btn-v">
                        <span>Anterior</span>
                    </button>
                    <button type="button" className="btn">
                        <span>Próximo</span>
                    </button>
                </div>
            </div>
            <div className="txt-bootom">
                Sem compromisso, cancele quando quiser. <br />
                Todo o conteúdo da Magnificat por um preço único e acessível. <br />
                Assista o quanto quiser em todos os seus aparelhos. <br />
            </div>
        </div>
    );
}

export default changePlan;
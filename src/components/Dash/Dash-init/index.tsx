import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { GeneralContext } from '../../../contexts/GeneralContext';
import api from '../../../services/api';
import './index.css'
// redes sociais
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import AppleLogin from 'react-apple-login';


const idGoogle = '493230704833-jl67nrvh43s5krjhchh0b6id240puf5l.apps.googleusercontent.com';
const idFacebook = '2627295150727407';

function DashInit() {
    const {setOpen, setMessage} = useContext(GeneralContext);
    const [userError, setuserError] = useState(false);
    const { user, setLoadOpen, openLoad, loadPage } = useContext(GeneralContext);
    const [openModal, setOpenmodal] = useState(false);

    const [facebook, setFacebook] = useState(false);
    const [google, setGoogle] = useState(false);
    const [apple, setApple] = useState(false);

    const [access_token, setAccessToken] = useState('');
    useEffect(() => {
        window.scrollTo(0, 0);
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getSocialNetwork()
        }
    }, [access_token]);
    // social login
    async function responseFacebook(response) {
        const data = new FormData();
        data.append('name', response.name);
        data.append('email', response.name);
        data.append('token', response.userID);
        data.append('social_network', 'facebook')
        api.post('/auth/registerSocialNetwork', data, 
        { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                setFacebook(response.data)
                console.log(response.data)
            })
            .catch(function (error) {
                console.log(error)
                setFacebook(false)
            })
    }
    async function responseGoogle(response) {
        const data = new FormData();
        data.append('token', response.googleId);
        data.append('email', response.profileObj.email);
        data.append('name', response.profileObj.givenName);
        data.append('social_network', 'google')
        api.post('auth/registerSocialNetwork', data,
        { headers: { "Authorization": `Bearer ${access_token}` } })
        .then(response => {
            setGoogle(response.data)
            console.log(response.data)
        })
        .catch(function (error) {
            console.log(error)
            setGoogle(false)
        })
     
    }
    function message(text: string){
        setMessage(text);
        setOpen(true);
    }
    async function getSocialNetwork(){
        api.get('auth/indexSocialNetwork',{ headers: { "Authorization": `Bearer ${access_token}` } } ).then(response =>{
            if(response.data.result){
                let networks = response.data.response;
                setFacebook(networks.findIndex(network => network.social_network == 'facebook')>-1);
                setGoogle(networks.findIndex(network => network.social_network == 'google')>-1);
                setApple(networks.findIndex(network => network.social_network == 'apple')>-1);
            }else{
                message(response.data.response);
            }
        })
        .catch(function (error) {
            console.log(error)
            setFacebook(false)
        })
    }
    async function getUser(){
        api.get('/').then(response =>{
            if(response.data.result){
                console.log(response.data.response)
            }else{
                message(response.data.response);
            }
        })
        .catch(function (error) {
            console.log(error)
            setFacebook(false)
        })
    }
    // criar função para desvincular a conta

    return (
        <div id="container-subscriber">
            <div className="indication-area">
                <Link to="/inicio" className="user__link-btn">
                    Voltar para <strong>Magnificat</strong>
                </Link>
                <span>Área do Assinante</span>
            </div>
            <div className="greeting-subscriber">
                <p>
                    {/*  quando é adm não esta mostrando o nome */}
                    <span>Oi</span> {user.first_name},<br />
                    Boas vindas ao <br />
                    Painel Magnificat
                </p>
                <Link to="/inicio" className="user__link-btn">
                    Voltar para <strong>Magnificat</strong>
                </Link>
            </div>
            <div className="user-account-options">
                <Link to="/sua-conta" className="user-account-option">
                    <div>
                        <i className="fas fa-user"></i>
                        <span>Conta</span>
                    </div>
                </Link >
                <Link to="/assinatura-e-pagamento" className="user-account-option">
                    <div>
                        <i className="fas fa-money-check-alt"></i>
                        <span>Assinatura e cobrança</span>
                    </div>
                </Link>
                <div onClick={() => setOpenmodal(!openModal)} className="user-account-option">
                    <div>
                        <i className="material-icons">leak_add</i>
                        <span>Login com rede social</span>
                    </div>
                </div>

            </div>
            <div className="user__container">
                <div className="user__error-status">
                    <div>
                        <strong>Magnificat</strong>
                    </div>
                    <div>
                        {userError ? 'Tá OFF' : 'Tá ON'}
                    </div>
                    <div>
                        {userError ?
                            (<div> A Maginificat esta enfrentando problemas técnicos neste momento, nossa equipe esta agindo para solucionar todos os problemas.</div>)
                            :
                            (<div>No momento, Magnificat está <br />
                            funcionando perfeitamente. <br />
                            Nenhum erro foi reportado.</div>)
                        }
                    </div>
                </div>
               
            </div>
            <div className="user__error-btn2">
                <p>Não encontrou o que precisava? <br /><strong>Nós te ajudamos!</strong></p>
                <Link to="#" className="user__link-btn">Acesse <strong>Central de Ajuda</strong></Link>
            </div>
            {/* area em desenvolvimento - ficara oculto no momento */}

            {/* <div className="user__updated-information">
                <h5>NOVIDADES</h5>
                <ul>
                   
                    <li>
                        <div className="user__updated-information__date">
                            <span>02 de abril, 09h20</span>
                        </div>
                        <div className="user__updated-information__content">
                            <h1>Novo filme no catálogo</h1>
                            <p>Novo filme estreiou na Magnificat vai ficar aí esperando ou va... Leia mais</p>
                        </div>
                    </li>
                    <li>
                        <div className="user__updated-information__date">
                            <span>02 de abril, 09h20</span>
                        </div>
                        <div className="user__updated-information__content">
                            <h1>Novo filme no catálogo</h1>
                            <p>Novo filme estreiou na Magnificat vai ficar aí esperando ou va... Leia mais</p>
                        </div>
                    </li>
                </ul>
                <div className="user__aling-btn">
                    <button className="user__link-btn">Ver mais</button>
                </div>
            </div>
              */}
            {/* login-social-network */}
            <div className={openModal ? 'login-social-network active-modal-social' : 'login-social-network'}>
                <span onClick={() => setOpenmodal(false)} className="material-icons close">close</span>
                <span className="title">Vincule sua a conta a sua rede social preferida.</span>
                <div className="border-social-blue">
                    <FacebookLogin
                        appId={idFacebook}
                        callback={responseFacebook}
                        render={renderProps => (
                            <button className="btn-social" onClick={renderProps.onClick}><i className="fab fa-facebook-f"></i>facebook<span className={facebook? 'material-icons state-bond-gren': 'material-icons state-bond'}>{facebook? 'done': 'add'}</span></button>
                        )}
                    />
                </div>
                <div className="border-social-blue">
                    <GoogleLogin
                        clientId={idGoogle}
                        render={renderProps => (
                            <button className="btn-social" onClick={renderProps.onClick} ><i className="fab fa-google"></i>google<span className={google? 'material-icons state-bond-gren': 'material-icons state-bond'}>{google? 'done': 'add'}</span></button>
                        )}
                        buttonText="Login"
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        cookiePolicy={'single_host_origin'}
                    />
                </div>
                {/* <div className="border-social-blue">
                </div> */}
                    {/* <button disabled={true} className="btn-social "><i className="fab fa-apple"></i>Apple<span className={google? 'material-icons state-bond-gren': 'material-icons state-bond'}>{apple? 'done': 'add'}</span>
                    </button> */}
                            {/* <AppleLogin clientId="com.react.apple.login" redirectURI="https://redirectUrl.com" /> */}
            </div>
            
                <p>{message}</p>
          
            <div className="user__announcement">
                <p>Announcement</p>
            </div>
        </div>
    );
}

export default DashInit;
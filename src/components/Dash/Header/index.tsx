import React, { useContext, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import logoWite from '../../../assets/imagens/Logo/SVG/branco.svg';
import { GeneralContext } from '../../../contexts/GeneralContext';
import api from '../../../services/api';

import './index.css';
function HeaderDash() {
    const {user, logout} = useContext (GeneralContext);
    return (
        <div id="dash-container">
            <div className="dash-container__content">

                <Link to="/inicio" className="dash__logo">
                  <img src={logoWite} alt=""/>
                </Link>
                <div className="dash__user">

                    <i className="fas fa-bell"></i>
                    <i className="far fa-user"></i>
                   <div className="menu__dash">
                        <Link to="/inicio">Sair</Link>
                   </div>
                
                </div>
            </div>
        </div>
    );
}

export default HeaderDash;
import React, { useContext, useEffect, useState } from 'react'
import api from "../../../services/api";
import Input from '../../Dashboard/Dashboard-input';
import ButtonsBorderBlue from '../../Buttons/Button-border-blue';
import { Link } from 'react-router-dom';
import { GeneralContext } from '../../../contexts/GeneralContext';


interface User {
    firstName: string;
    lastName: string;
    email: string;
}
function DashAcont() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [birth, setBirth] = useState('0000-00-00');
    const [phone, setPhone] = useState('');
    const {setOpen, setMessage} = useContext(GeneralContext);
    const { user, setLoadOpen, openLoad, loadPage } = useContext(GeneralContext);
    // BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState('');
    useEffect(() => {
        if (access_token == "")
            setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);
    useEffect(() => {
        if (access_token != "") {
            api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
                if (response.data) {
                    let arrName = response.data.name.split(' ');
                    let firstName = arrName[0];
                    arrName.splice(0, 1);
                    let lastName = arrName.join(' ');
                    setFirstName(firstName);
                    setLastName(lastName);
                    setEmail(response.data.email);
                    if (response.data.birth != null) {
                        let date = new Date(response.data.birth);
                        setBirth(date.getFullYear() + '-' + addZero(date.getMonth() + 1) + '-' + addZero(date.getDate()));
                    }
                    else setBirth('0000-00-00');
                    setPhone(response.data.phone ?? '');
                }
            });
        }
    }, [access_token]);
    // END:: ACCESS_TOKEN

    function addZero(number: number) {
        if (number <= 9) return "0" + number;
        else return number;
    }

    function update() {
        const data = new FormData();

        // OBRIGATÓRIOS
        if (email.length > 5 && email.indexOf('@') != -1 && email.indexOf('.') != -1)
            data.append('email', email);
        else message('Preencha um email válido');
        if (firstName.length > 0) data.append('first_name', firstName);
        else message('Preencha o campo nome');
        if (lastName.length > 0) data.append('last_name', lastName);
        else message('Preencha o campo sobrenome');
        // OPICIONAIS
        if (birth.length > 0) data.append('birth', birth);
        if (phone.length > 0) data.append('phone', phone);

        api.post('auth/update', data, { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) { message('Usuário atualizado'); }
                else message(response.data.response);
            });
    }

    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }

    return (
        <form action="">
            <fieldset className="inpust__user">
                <Input name="Fnome" type="text" Label="Nome"
                    value={firstName} onChange={event => setFirstName(event.target.value)} />
                <Input name="Fsobrenome" type="text" Label="Sobrenome"
                    value={lastName} onChange={event => setLastName(event.target.value)} />
                <Input name="FdataNascimento" type="date" Label="Data de nascimento"
                    value={birth} onChange={event => setBirth(event.target.value)} />
                <Input name="Email" type="mail" Label="Email"
                    value={email} onChange={event => setEmail(event.target.value)} />
                <Input name="FdataNascimento" type="text" Label="Telefone"
                    value={phone} onChange={event => setPhone(event.target.value)} />
            </fieldset><br />
            <div className="agroup-btn-acont-user">
                <Link className="" to="/painel-de-controle">Voltar</Link>
                <button className="" onClick={update} type="button"> Atualizar</button>
            </div>
            {/* <ButtonsBorderBlue type="button" name="Atualizar" onClick={update}/>*/}
        </form>

    );
}

export default DashAcont;
import React from 'react';
import { Link } from 'react-router-dom';
import logowite from '../../assets/imagens/Logo/SVG/branco.svg';
import logoblue from '../../assets/imagens/Logo/SVG/logomagnificat.svg';


function Footer() {
    const Data = new Date().getFullYear();
    return (
        <>
            <footer>
                <div className="gradient-border">
                    <div className="efect-gradient">

                    </div>
                    <div className="efect-gradient2">

                    </div>
                </div>
                <div className="footer_content">
                    <Link to="/inicio" className="footer_card1">
                        <img src={logowite} alt="" />
                    </Link>
                    <div className="conteudo-left-footer">
                        <div className="footer_card">
                            <Link to="/pagina-em-desenvolvimento">Perguntas frequentes</Link>
                            <Link to="/pagina-em-desenvolvimento">Formas de assistir</Link>
                            <Link to="/pagina-em-desenvolvimento" href="#"> Entre em contato</Link>
                        </div>
                        <div className="footer_card">
                            <Link to="/pagina-em-desenvolvimento" href="#">Imprensa</Link>
                            <Link to="/pagina-em-desenvolvimento" href="#">Informações corporativas</Link>
                            <Link to="/pagina-em-desenvolvimento" href="#">Teste de velocidade</Link>
                            <Link to="/pagina-em-desenvolvimento" href="#">Avisos legais</Link>
                        </div>
                        <div className="footer_card">
                            <Link to="/pagina-em-desenvolvimento">Centro de ajuda</Link>
                            <Link to="/painel-de-controle" href="#">Conta</Link>
                        </div>
                    </div>
                </div>
                <div className="footer_logo">
                    <div className="cut-img"><img src={logoblue} alt="" /></div>
                    <div className="copyricht">
                        <p><span className="lg_destaque">Magnificat </span> <span id="y">&copy;{Data} </span> - Todos os direitos reservados</p>
                    </div>
                </div>
            </footer>
        </>
    );

}

export default Footer;


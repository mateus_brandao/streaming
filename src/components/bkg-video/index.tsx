import React, { useState } from 'react';
import Vimeo from '@u-wave/react-vimeo';


function BkgVimeo({ infoMovie }: any) {
    let volumeV: number
    const [audioOf, setAudioOf] = useState(true)
    if(audioOf == true){
        volumeV = 0.5
    }else{
        volumeV = 0
    }
  
    return (
        <div className="bkg__vimeo">
            <div id="mudo">
                {/* <iframe src={`https://player.vimeo.com/video/${infoMovie.VimeoID}?autoplay=1&loop=1&byline=0&portrait=0"`}  allow="autoplay; fullscreen; picture-in-picture"></iframe>    */}
                    <Vimeo
                        video={infoMovie.VimeoID}
                        autoplay={true}
                        responsive={true}
                        loop={true}
                        volume={volumeV}
                    /> 

            </div>
            <span className="audio-control"   onClick={() => setAudioOf(!audioOf)}>
    
                {audioOf ? <i className="fas fa-volume-up"></i> :  <i className="fas fa-volume-mute"></i> }
            </span>
        </div>
    );
}



export default BkgVimeo;
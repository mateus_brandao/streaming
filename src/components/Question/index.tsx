import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './question.css'

interface QuestProps {
    title: string;
    description: string;
}


function Quest(props: QuestProps) {


    const [expanded, setExpanded] = useState(false);
    let altura: any
    let bkg: any
    let overflow: any
    if (expanded == false) {
        altura = 0
        bkg = '#ffffff';
        overflow = 'hidden';
    }
    else {
    altura = '500px';
    bkg = '#CECFD1'
   
    }


return (
    <>

        <div id="box-questao"
            style={{
                background: bkg,
            }}
        >
            <div className={expanded ? 'pergunta rotateico' : 'pergunta'} onClick={() => setExpanded(!expanded)}>
                <span> {props.title} </span>
                <i className="fas fa-plus"></i>
            </div>
            <div className="resposta"
                style={{
                    maxHeight: altura,
                    overflow: overflow,
                }}
            >
                <p>
                    {props.description}
                </p>
            </div>
        </div>

    </>
);
}

export default Quest;


{/*
     <div className={expanded ? 'respostaactive resposta' : 'resposta'}>
                    <p>
                        
                    {props.description}
                        
                    </p>
                </div>


*/}

import React, { useContext, useEffect } from 'react';
import './index.css'
import logoblue from '../../assets/imagens/Logo/SVG/logomagnificat.svg';
import { GeneralContext } from '../../contexts/GeneralContext';
import { useHistory } from 'react-router';

function Loaditem() {
    const {user, setLoadOpen, openLoad, loadPage} = useContext (GeneralContext);
    const history = useHistory();

   

    setTimeout(function(){
        let spinerTime: any
        spinerTime = document.querySelectorAll('.spiner-time');
        spinerTime[1].style.opacity = 0;
        spinerTime[0].style.opacity = 0;
    },10000)

    setTimeout(function(){
        let spinerTime: any
        spinerTime = document.querySelectorAll('.spiner-time');
        spinerTime[1].innerHTML = `Quase lá...`;
        spinerTime[1].style.opacity = 1;
    },11000)
    setTimeout(function(){
        let spinerTime: any
        spinerTime = document.querySelectorAll('.spiner-time');
        spinerTime[1].style.opacity = 0;
    },14000)
    setTimeout(function(){
        let spiner: any;
        let txtFinLoad: any;
        txtFinLoad = document.querySelector('.txt-final-load')
        spiner = document.querySelector('.load__content')
        spiner.style.opacity = 0;
        txtFinLoad.style.opacity = 1;
    },15000)
    setTimeout(function(){
        let txtFinLoad: any;
        let icoCoroa: any;
        txtFinLoad = document.querySelector('.txt-final-load')
        icoCoroa = document.querySelector('.spiner-ico-coroa')
        txtFinLoad.style.opacity = 0;
        icoCoroa.style.opacity = 1;
    },16000)
    setTimeout(function(){
        history.push('/inicio');
    },18000)
    return (
        <div id="container_load">
            <div className="spiner__salutation spiner-time">
                {user.first_name} estamos personalizando <br /> a magnificat para você!
                </div>
            <div className="load__content">
                <div className="spiner-center">
                </div>
            </div>

            <div className="txt-final-load">
                <span>Estamos prontos!</span>
            </div>
            <div className="spiner-ico-coroa">
                <img src={logoblue} alt=""/>
            </div>
            <div className="spiner-txt spiner-time">
                É rapidinho.... por favor aguarde.
            </div>

        </div>



    );
}

export default Loaditem;
import axios from 'axios';

let origin = 'https://api.magnificat.video/api';
let testServer = true;

if ((window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1") && !testServer){
   origin = 'http://localhost:8000/api';
}

const api = axios.create({ baseURL: origin, });

export default api;

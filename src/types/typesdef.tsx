import React from 'react';

// USER ================================
export interface User {
  first_name: string;
  last_name: string;
  email: string;
  permission: string;
}
// MOVIES ===============================
export interface Category {
  id: number;
  name: string;
}
export interface Genre {
  id: number;
  name: string;
}
export interface Trailer {
  id: number;
  url: string;
  name: string;
  thumbnail: string;
}
export interface Extra {
  id: number;
  url: string;
  name: string;
  thumbnail: string;
}
export interface Movie {
  id: number;
  name: string;
  description: string;
  sinopse: string;
  parental_rating: string;
  release: string;
  country: string;
  direction: string;
  production: string;
  script: string;
  composition: string;
  cast: string;
  categories: Category[];
  genres: Genre[];
  url: string;
  trailers?: Trailer[];
  extras?: Extra[];
  approve_trailers?: Trailer[];
  approve_extras?: Extra[];
  duration: string;
  approval: string;
  cost?: string;
  expiration: string;
  thumbnail: string;
  wallpaper: string;
  freeable: boolean;
  provider_id?: number;
}
export interface TrailerExtra {
  id: number;
  url: string;
  name: string;
  thumbnail: string;
}
// PODCASTS ============================
export interface RecordCompanies{
  id: number;
  name: string;
}
export interface Artists{
  id: number;
  name: string;
}
export interface Albums{
  id: number;
  name: string;
  type: string;
}
export interface Podcast {
  id: number;
  name: string;
  description: string;
  url: string;
  clip_url: string;
  duration: string;
  thumbnail: string;
  wallpaper: string;
  release: string;
  country: string;
  production: string;
  edition: string;
  episode_index: number;
  expiration: string;
  awards: string;

  record_company: RecordCompanies;
  artist: Artists;
  album: Albums;

  categories: Category[];
  genres: Genre[];

  approval?: string;
}
// SERIES ==============================
export interface Season{
  id: number;
  title: string;
  description: string;
  sinopse: string;
  indice: number;
  walpapper: string;
  thumbnail: string;
  approval?: string;
  disapproval_reason?: string;
  provider_id?: number;
  episodes?: Movie[];
}
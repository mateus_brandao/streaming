import React from 'react';
import Routes from './routes';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { GeneralProvider } from './contexts/GeneralContext';

function App() {
  return(
    <GeneralProvider>
      <Routes/>
    </GeneralProvider>
  );
}

export default App;

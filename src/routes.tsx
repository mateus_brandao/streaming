import React from 'react';
import { BrowserRouter, Switch, Route, HashRouter } from 'react-router-dom';

import Landing from './pages/Landing';
import Login from './pages/Login';
import Home from './pages/Home';
import Description from './pages/description';
import Play from './pages/Play';
import Dashboard from './pages/dashboard';
// import Dashboartadm from './pages/Dashboar-adm';
import Register from './pages/Register/Register';
import ChangePlan from './pages/dashboard/changePlan';
import PodCast from './pages/Podcast';
import ReturnNavUser from './pages/dashboard/return-navigation';
import SubscriptionPayment from './pages/dashboard/subscriptionPayment';
//nova dashboard adm
import Provisorio from './pages/Ndashboar-amd';
import InsertMedia from './pages/Ndashboar-amd/insert-media';
import AddMovie from './pages/Ndashboar-amd/add-movie';
import RegisteredMovies from './pages/Ndashboar-amd/registered-movies';
import RegisteredSeries from './pages/Ndashboar-amd/registered-series';
import RegisteredPodcasts from './pages/Ndashboar-amd/registered-podcasts';
import ConfigCatalogo from './pages/Ndashboar-amd/config-catalogo';
import InsertPodCast from './pages/Ndashboar-amd/insert-podcast';
import AddPartner from './pages/Ndashboar-amd/addPartner';
import ListPartner from './pages/Ndashboar-amd/list-partiner';
import ResetPassword from './pages/reset-password';
import PrivacyPolicy from './pages/privacy-policy';
import PageErro from './pages/erro';
// visualizar load
import Load from './components/Load-cadastro';
//
import Category from './pages/categoria';
// página em desenvolvimento
import DevPage from './pages/em-dev';
//import PlayDefault from './pages/PlayDefault';

//termos-de-servico
import TermoServico from './pages/termo-servico';
import instDados from './pages/instrucao-ex-dados';
// explocar catalogo
import ExCatalogo from './pages/ex-catalogo';




function Routes(){
    return(
        <HashRouter>
            <Switch>
                <Route path="/"  component={Landing} exact/>
                <Route path="/conecte-se" component={Login} />
                <Route path="/cadastre-se" component={Register}/>
                <Route path="/inicio" component={Home}/>
                <Route path="/descricao-do-filme/:id" component={Description}/>
                <Route path="/reproduzir/:id" component={Play}/>
                <Route path="/painel-de-controle" component={Dashboard} />
                <Route path="/sua-conta" component={ReturnNavUser} />
                <Route path="/assinatura-e-pagamento" component={SubscriptionPayment} />
                <Route path="/painel-de-controle-adm" component={Provisorio}/>
                <Route path="/alterar-plano" component={ChangePlan}/>
                <Route path='/pod-cast' component={PodCast}/>
                {/* <Route path='/painel' component={Provisorio}/> */}
                <Route path='/inserir-midia' component={InsertMedia}/>
                <Route path='/adicionar-filme/:id?' component={AddMovie}/>
                <Route path='/filmes-cadastrados' component={RegisteredMovies}/>
                <Route path='/series-cadastradas' component={RegisteredSeries}/>
                <Route path='/podcasts-cadastrados' component={RegisteredPodcasts}/>
                <Route path='/configurar-catalogo' component={ConfigCatalogo}/>
                <Route path='/inserir-podcast/:id?' component={InsertPodCast}/>
                <Route path='/load' component={Load}/>
                <Route path='/categoria/:id' component={Category}/>
                <Route path='/add-parceiro' component={AddPartner}/>
                <Route path='/lista-parceiro' component={ListPartner}/>
                <Route path='/pagina-em-desenvolvimento' component={DevPage}/>
                <Route path='/redefinir-senha' component={ResetPassword}/>
                <Route path='/politica-de-privacidade' component={PrivacyPolicy}/>
                <Route path='/erro' component={PageErro} />
                <Route path="/termo-de-servico" component={TermoServico}/>
                <Route path='/instrucao-de-exclusao-de-dados' component={instDados}/>
                <Route path='/explorar-catalogo' component={ExCatalogo}/>
              
            </Switch>
        </HashRouter>
    );
}

export default Routes;



/* */
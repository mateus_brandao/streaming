import { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import ModalMessage from '../components/Modal/ModalMessage';
import api from '../services/api';
import LoadPage from '../components/Load';


interface Category {
    id: number;
    name: string;
}
interface Genre {
    id: number;
    name: string;
}
interface Trailer {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}
interface Extra {
    id: number;
    url: string;
    name: string;
    thumbnail: string;
}
interface UserMovie {
    id: number;
    user_id: number;
    movie_id: number;
    current_time: string;
    viewed: boolean;
    favorite: boolean;
    rating: string;
    movie?: Movie;
}
interface Movie {
    id: number;
    name: string;
    description: string;
    sinopse: string;
    parental_rating: string;
    release: string;
    country: string;
    direction: string;
    production: string;
    script: string;
    composition: string;
    cast: string;
    categories: Category[];
    genres: Genre[];
    url: string;
    trailers: Trailer[];
    extras?: Extra[];
    users?: UserMovie[];
    duration: string;
    cost: string;
    expiration: string;
    thumbnail: string;
    wallpaper: string;
}
interface CategoryMovie {
    id: number;
    name: string;
    movies: Movie[];
}

interface MoviesContextData{
    watchingMovies: UserMovie[];
    setWatchingMovies: (param: UserMovie[]) => void;
    favoriteMovies: UserMovie[];
    setFavoriteMovies: (param: UserMovie[]) => void;
    divInvisibleFavorite: String[];
    setDivInvisibleFavorite: (param: String[]) => void;
    categoryMovies: CategoryMovie[];
    setCategoryMovies: (param: CategoryMovie[]) => void;
    breakpoint: number;
    setBreakpoint: (param: number) => void;

    getFavoriteMovies: (param?: string)=> void;
    getWatchingMovies: (param?: string)=> void;
    getMovies: (param?: string)=> void;

    toggleRating: (movie: Movie, rating: number) => void;
    toggleFavorite: (movie: Movie) => void;
    filterCategory: (param: string) => void;
}
interface MoviesProviderProps{
    children: ReactNode;
}

export const MoviesContext = createContext({} as MoviesContextData);

export function MoviesProvider({children,...rest} : MoviesProviderProps){    
    const [watchingMovies, setWatchingMovies] = useState<UserMovie[]>([handleDefaultUserMovie()]);
    const [favoriteMovies, setFavoriteMovies] = useState<UserMovie[]>([handleDefaultUserMovie()]);
    const [categoryMovies, setCategoryMovies] = useState<CategoryMovie[]>([]);
    const [divInvisibleFavorite, setDivInvisibleFavorite] = useState<String[]>([]);
    const [breakpoint, setBreakpoint] = useState(breakpoints());

    // BEGIN:: ACCESS_TOKEN
    const [access_token, setAccessToken] = useState("");
    useEffect(() => {
        if (access_token == "") setAccessToken(sessionStorage.getItem('magnificat_access') || "");
    }, []);

    // END:: ACCESS_TOKEN
    useEffect(() => {
        if (favoriteMovies) {
            let invisible = [] as string[];
            while ((favoriteMovies.length + invisible.length) < breakpoint) { invisible.push('ok'); }
            setDivInvisibleFavorite(invisible);
        }
    }, [favoriteMovies]);

    function getFavoriteMovies(token?:string) {
        if(token && !access_token) setAccessToken(token);
        api.get('auth/getFavoriteMovies', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setFavoriteMovies([...response.data.response]);
                }
                else message(response.data.response);
            });
    }
    function getWatchingMovies (token?:string) {
        if(token && !access_token) setAccessToken(token);
        api.get('auth/watchingMovies', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setWatchingMovies([...response.data.response]);
                }
                else message(response.data.response);
            });
    }
    function getMovies(token?:string) {
        console.log('entrou na função')
        if(token && !access_token) setAccessToken(token);
        api.get('auth/getMoviesCatalog', { headers: { "Authorization": `Bearer ${access_token}` } })
            .then(response => {
                if (response.data.result) {
                    setCategoryMovies([...response.data.response]);
                    console.log(response.data.response)
                }
                else message(response.data.response);
            });
    }
    function message(text: string) {
        // if (modalMessageOpen) { setModalMessageOpen(false); }
        // setModalMessageText(text);
        // setModalMessageOpen(true);
        console.log(text);
    }
    function breakpoints() {
        let windowWidth = window.innerWidth;
        let slidesToShow = 5;

        if (windowWidth > 1024) slidesToShow = 5;
        else if (windowWidth > 800) slidesToShow = 4;
        else if (windowWidth > 620) slidesToShow = 3;
        else if (windowWidth > 400) slidesToShow = 2;
        else slidesToShow = 1;

        return slidesToShow;
    }
    function filterCategory(name: string) {
        if (categoryMovies) {
            let index = categoryMovies.findIndex(category => category.name == name);
            if (index != -1) { console.log(categoryMovies[index].id); }
        }
    }
    function toggleRating(movie: Movie, rating: number) {
        const data = new FormData();

        data.append('rating', String(rating));
        data.append('movie_id', String(movie.id));

        api.post('auth/toggleRating', data, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                movie.users = [];
                movie.users[0] = response.data.response;
                updateOneMovie(movie);
            }
            else message(response.data.response);
        });
    }
    function toggleFavorite(movie: Movie) {
        const data = new FormData();
        data.append('movie_id', String(movie.id));
        api.post('auth/toggleFavorite', data, { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data.result) {
                getFavoriteMovies();
                movie.users = [];
                movie.users[0] = response.data.response;
                console.log(response.data.response);
                updateOneMovie(movie);
            }
            else message(response.data.response);
        });
    }
    function updateOneMovie(movie: Movie) {
        let temp: any;
        let index: any;

        if (watchingMovies) {
            temp = watchingMovies;
            index = watchingMovies.findIndex(watching => watching.movie?.id == movie.id);
            if (index != -1) {
                temp[index].movie = movie;
                setWatchingMovies([...temp]);
            }
        }

        temp = categoryMovies;
        index = -2;
        categoryMovies.map((category, i) => {
            index = category.movies.findIndex(category => category.id == movie.id);
            if (index != -1) temp[i].movies[index] = movie;
        });
        if (index != -2) setCategoryMovies([...temp]);

        if (favoriteMovies) {
            temp = favoriteMovies;
            index = favoriteMovies.findIndex(favorite => favorite.movie?.id == movie.id);
            if (index != -1) {
                temp[index].movie = movie;
                setFavoriteMovies([...temp]);
            }
        }
    }
    function handleDefaultUserMovie() {
        return {
            id: 0,
            user_id: 0,
            movie_id: 0,
            current_time: '0',
            viewed: false,
            favorite: false,
            rating: "1",
        };
    }

    return(
        <MoviesContext.Provider value={{
            watchingMovies, setWatchingMovies,
            favoriteMovies, setFavoriteMovies,
            divInvisibleFavorite, setDivInvisibleFavorite,
            categoryMovies, setCategoryMovies,
            breakpoint, setBreakpoint,

            getFavoriteMovies,
            getWatchingMovies,
            getMovies,

            toggleRating,
            toggleFavorite,
            filterCategory,
        }}>
            <>
                { children }
            </>
        </MoviesContext.Provider>
    );
}

export const useMovies = () => {
    return useContext(MoviesContext);
}
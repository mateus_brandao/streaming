import { createContext, ReactNode, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import ModalMessage from '../components/Modal/ModalMessage';
import api from '../services/api';
import LoadPage from '../components/Load';


interface User {
    first_name: string;
    last_name: string;
    email: string;
    permission: string;
}


interface GeneralContextData{
    open: boolean;
    setOpen: (param: boolean) => void;
    message: string;
    setMessage: (param: string) => void;
    loadOpen: boolean;
    setLoadOpen: (param: boolean) => void;
    user: User;
    setUser: (param: User) => void; 
    logout: () => void;
    openLoad: boolean;
    setOpenLoad: (param: boolean) => void;
    loadPage: () => void;
    getUser: () => void;
}
interface GeneralProviderProps{
    children: ReactNode;
}

export const GeneralContext = createContext({} as GeneralContextData);

export function GeneralProvider({children,...rest} : GeneralProviderProps){    
    const [open,setOpen] = useState(false);
    const [message,setMessage] = useState('');
    const [loadOpen, setLoadOpen] = useState(false);
    const history = useHistory();
    const [user, setUser] = useState<User>({ first_name: '', last_name: '', email: '', permission: 'user' });
    const [access_token, setAccessToken] = useState("");
    const [openLoad, setOpenLoad] = useState(false);

    useEffect(() => {
        if (sessionStorage.getItem('magnificat_access') !== null)
        setAccessToken(sessionStorage.getItem('magnificat_access') || "");

        window.scrollTo(0, 0);
    }, []);
    useEffect(() => {
        if (access_token != "") {
            getUser();
        }
    }, [access_token]);
    useEffect(() =>{
        if(openLoad &&  user && user.first_name.length > 0){
            console.log('key')
        }

    },[user, openLoad]);

    function logout() {
        localStorage.removeItem('magnificat_access');
        sessionStorage.removeItem('magnificat_access');

        api.post('auth/logout', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => { console.log(response.data); });

        history.push('/');
    }
    function getUser() {
        api.post('auth/me', "", { headers: { "Authorization": `Bearer ${access_token}` } }).then(response => {
            if (response.data) {
                let arrName = response.data.name.split(' ');
                let firstName = arrName[0];
                arrName.splice(0, 1);
                let lastName = arrName.join(' ');
                setUser({
                    'first_name': firstName,
                    'last_name': lastName,
                    'email': response.data.email,
                    'permission': response.data.permission
                });
                // if(response.data.permission === 'user') history.push('/reproduzir-default');

                //"https://magnificat.video/live/"; 
            }
        });
    }
    function loadPage(){
        let spiner: any;
        spiner = document.querySelector('.on-load-page')
        spiner.style.display = 'none'
    }
    return(
        <GeneralContext.Provider value={{
            open, setOpen, 
            message, setMessage,
            user, setUser,
            loadOpen, setLoadOpen,
            logout,
            openLoad, setOpenLoad,
            loadPage,
            getUser
            
        }}>
            <>
                { children }
                <ModalMessage/>
                {openLoad && (
                    <div className="on-load-page">
                    <LoadPage />
                    </div>
                )}
              

              
            </>
        </GeneralContext.Provider>
    );
}
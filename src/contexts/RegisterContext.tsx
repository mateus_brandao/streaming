import { getSuggestedQuery } from '@testing-library/dom';
import { createContext, ReactNode, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import api from '../services/api';
import { GeneralContext } from './GeneralContext';


interface RegisterContextData {
    step: Number;
    toStep: (param: Number) => void;
    dataSubscriptionPlan: SubscriptionPlanData[];
    setDataSubscriptionPlan: (param: SubscriptionPlanData[]) => void;
    email: string;
    setEmail: (param: string) => void;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    subscriptionPlan: Number;
    setFirstName: (param: string) => void;
    setLastName: (param: string) => void;
    setPassword: (param: string) => void;
    setConfirmPassword: (param: string) => void;
    setSubscriptionPlan: (param: Number) => void;
    cpf: string;
    phone: string;
    setCpf: (param: string) => void;
    setPhone: (param: string) => void;
    cardName: string;
    cardNumber: string;
    cardValidity: string;
    cardCode: string;
    cardMethod: string;
    setCardName: (param: string) => void;
    setCardNumber: (param: string) => void;
    setCardValidity: (param: string) => void;
    setCardCode: (param: string) => void;
    setCardMethod: (param: string) => void;
    cardMagnificat: string;
    setCardMagnificat: (param: string) => void;
    startSubscription: () => void;
}

interface RegisterProviderProps {
    children: ReactNode;
}

interface SubscriptionPlanData {
    id: number;
    name: string;
    price: number;
    resolution: string;
    screens: string;
    profiles: string;
}

export const RegisterContext = createContext({} as RegisterContextData);


export function RegisterProvider({ children, ...rest }: RegisterProviderProps) {
    const history = useHistory();
    const { setOpen, setMessage } = useContext(GeneralContext);
    const [dataSubscriptionPlan, setDataSubscriptionPlan] = useState<SubscriptionPlanData[]>(
        [{ 'id': 0, 'name': '', 'price': 0.00, 'resolution': '', 'screens': '', 'profiles': '', }]);

    const [step, setStep] = useState<Number>(1);
    const [email, setEmail] = useState<string>('');
    const [firstName, setFirstName] = useState<string>('');
    const [lastName, setLastName] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [confirmPassword, setConfirmPassword] = useState<string>('');
    const [subscriptionPlan, setSubscriptionPlan] = useState<Number>(2);

    const [cpf, setCpf] = useState<string>('');
    const [phone, setPhone] = useState<string>('');

    const [cardName, setCardName] = useState<string>('');
    const [cardNumber, setCardNumber] = useState<string>('');
    const [cardValidity, setCardValidity] = useState<string>('');
    const [cardCode, setCardCode] = useState<string>('');
    const [cardMethod, setCardMethod] = useState<string>('Crédito');

    const [cardMagnificat, setCardMagnificat] = useState<string>('');
    const { user, setUser } = useContext(GeneralContext);


    useEffect(() => { getSubscriptionPlan(); }, []);

    useEffect(() => {
        let temp = removePunctuation(cpf);

        if(temp.length >= 3) temp = temp.substr(0,3) + '.' + temp.substr(3);
        if(temp.length >= 7) temp = temp.substr(0,7) + '.' + temp.substr(7);
        if(temp.length >= 11) temp = temp.substr(0,11) + '-' + temp.substr(11);

        setCpf(temp);
    }, [cpf]);

    useEffect(() => {
        let temp = removePunctuation(cardValidity);

        if(temp.length >= 2) temp = temp.substr(0,2) + '/' + temp.substr(2);

        setCardValidity(temp);
    }, [cardValidity]);

    useEffect(() => {
        let temp = removePunctuation(cardNumber);

        if(temp.length >= 4) temp = temp.substr(0,4) + ' ' + temp.substr(4);
        if(temp.length >= 9) temp = temp.substr(0,9) + ' ' + temp.substr(9);
        if(temp.length >= 14) temp = temp.substr(0,14) + ' ' + temp.substr(14);

        setCardNumber(temp);
    }, [cardNumber]);

    useEffect(() => {
        let temp = removePunctuation(phone);

        if(temp.length > 0) temp = '(' + temp;
        if(temp.length >= 3) temp = temp.substr(0,3) + ') ' + temp.substr(3);
        if(temp.length >= 10) temp = temp.substr(0,10) + '-' + temp.substr(10);

        setPhone(temp);
    }, [phone]);

    function toStep(param: Number) {
        if (step == 1 && param == 2) {
            if (firstName.length > 0) {
                if (lastName.length > 0) setStep(2);
                else message('Preencha o campo Sobrenome');
            }
            else message('Preencha o campo Nome.');
        } else
            if (step == 2 && param == 3) {
                if (password.length >= 8) {
                    if (password == confirmPassword) {
                        setStep(3);
                        // startSubscription(); <-PULAR SELEÇÃO DE PLANO E FORMA DE PAGAMENTO
                    }
                    else message('A confirmação de senha não é correspondente a senha digitada');
                } else message('Preencha o campo Senha com pelo menos 8 caracteres');
            } else setStep(param);
    }
    function startSubscription() {
        const data = new FormData();
        console.log(validateCard(cardNumber));
        // let next = true; <-PULAR SELEÇÃO DE PLANO E FORMA DE PAGAMENTO
        let next = false;
        if (step == 5) { // Cartão de Crédito/Débito
            if (cpf.length == 14 && cpfValidationAlgorithm(cpf)) {
                if (phone.length >= 14) {
                    if (cardName.length > 0) {
                        if (cardNumber.length == 19 && validateCard(cardNumber)) {
                            if (cardValidity.length == 7 && validateCardValidity(cardValidity)) {
                                if (cardCode.length == 3) {
                                    data.append('payment_method', 'card');
                                    data.append('cpf', cpf);
                                    data.append('phone', phone);
                                    data.append('card_name', cardName);
                                    data.append('card_number', cardNumber);
                                    data.append('card_validity', cardValidity);
                                    data.append('card_code', cardCode);
                                    data.append('card_method', cardMethod);
                                    next = true;
                                } else message('Digite o código de Segurança');
                            } else message('Digite uma validade de Cartão Válida');
                        } else message('Digite um número de Cartão Válido');
                    } else message('Digite o nome escrito no Cartão');
                } else message('Digite um número de Telefone');
            } else message('Digite um CPF válido');
        } else 
        if(step == 6) { // Cartão Pré-Pago
            if(cardMagnificat.length > 0) {
                data.append('payment_method', 'card_magnificat');
                data.append('card_magnificat', cardMagnificat);
                next = true;
            } else message('Digite o código do Cartão Magnificat');
        }else 
        if (step == 3 || step == 4) { // Conta Gratuita
            setSubscriptionPlan(4); // Gratuito
            next = true;
        }
        if (next) {
            data.append('email', email);
            data.append('first_name', firstName);
            data.append('last_name', lastName);
            data.append('password', password);
            data.append('subscription_plan', String(subscriptionPlan));
            data.append('amount', String(getAmount()));
            data.append('permission', 'user');

            api.post('register', data).then(response => {
                if (response.data.result) sendLogin();
                else message(response.data.response);
            });
        }
    }
    function sendLogin() {
        const data = new FormData();

        data.append('email', email);
        data.append('password', password);

        api.post('auth/login', data).then(response => {
            if (response.data.result) {
                if (response.data.response.original.access_token) {
                    let access_token = response.data.response.original.access_token;
                    sessionStorage.setItem('magnificat_access', access_token);
                    let temp = user;
                    temp.first_name = firstName;
                    setUser(temp);
                    history.push('/load');
                } else message('Houve um erro inesperado ao tentar realizar o login.');
            }
            else message(response.data.response);
        });
    }
    function message(text: string) {
        setMessage(text);
        setOpen(true);
    }
    function getSubscriptionPlan() {
        api.get('indexSubscriptionPlan').then(response => {
            if (response.data.result) {
                setDataSubscriptionPlan(response.data.response)
            }
            else console.log(response.data.response);
        });
    }
    function getAmount() {
        let amount = 0;
        let index = dataSubscriptionPlan.findIndex(plan => plan.id == subscriptionPlan);
        amount = dataSubscriptionPlan[index].price;
        return amount;
    }
    function removePunctuation(temp: string){
        while(temp.indexOf('.') !== -1 || temp.indexOf('-') !== -1 || 
              temp.indexOf('/') !== -1 || temp.indexOf(' ') !== -1 ||
              temp.indexOf('(') !== -1 || temp.indexOf(')') !== -1){
            temp = temp.replace('.','');
            temp = temp.replace('-','');
            temp = temp.replace('/','');
            temp = temp.replace(' ','');
            temp = temp.replace('(','');
            temp = temp.replace(')','');
        }
        return temp;
    }
    function cpfValidationAlgorithm(strCPF: string){
        strCPF = removePunctuation(strCPF);
        
        let invalids = [
            '11111111111','22222222222','33333333333','44444444444','55555555555',
            '66666666666','77777777777','88888888888','99999999999'
        ];

        if(invalids.indexOf(strCPF)!=-1)
            return false;
        var soma: number;
        var resto: number;
        soma = 0;
        if (strCPF == "00000000000")
            return false;
        for (let i=1; i<=9; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(9, 10)))
            return false;
        soma = 0;
        for (let i = 1; i <= 10; i++)
            soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        resto = (soma * 10) % 11;
        if ((resto == 10) || (resto == 11))
            resto = 0;
        if (resto != parseInt(strCPF.substring(10, 11)))
            return false;
        return true;
    }
    function validateCard(nr: string) {
        nr = removePunctuation(nr);
        console.log(nr);
        const cards = {
            visa: /^4[0-9]{12}(?:[0-9]{3})/,
            mastercard: /^5[1-5][0-9]{14}/,
            amex: /^3[47][0-9]{13}/,
            diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
            discover: /^6(?:011|5[0-9]{2})[0-9]{12}/,
            jcb: /^(?:2131|1800|35\d{3})\d{11}/,
            hipercard: /^(606282\d{10}(\d{3})?)|(3841\d{15})/,
            elo: /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})/,
            aura: /^(5078\d{2})(\d{2})(\d{11})$/,
        };

        for (var card in cards) if (nr.match(cards[card])) return true;
        return false;
    }
    function validateCardValidity(date: string){
        let arr_date = date.split('/');
        
        if(arr_date.length != 2) return false;
        
        let month = arr_date[0];
        let year = arr_date[1];
        
        if(Number(month) > 12 || Number(month) < 1) return false;

        var date_now = new Date();
        var year_now = date_now.getFullYear();
        
        if(year_now > Number(year)) return false;

        if(year_now == Number(year)){
            if(date_now.getMonth() + 1 >= Number(month)) return false;
        }

        return true;
    }
    return (
        <RegisterContext.Provider value={{
            step,
            toStep,

            dataSubscriptionPlan,
            setDataSubscriptionPlan,

            email,
            setEmail,

            firstName,
            lastName,
            password,
            confirmPassword,
            subscriptionPlan,

            setFirstName,
            setLastName,
            setPassword,
            setConfirmPassword,
            setSubscriptionPlan,

            cpf,
            phone,

            setCpf,
            setPhone,

            cardName,
            cardNumber,
            cardValidity,
            cardCode,
            cardMethod,

            setCardName,
            setCardNumber,
            setCardValidity,
            setCardCode,
            setCardMethod,

            cardMagnificat,
            setCardMagnificat,

            startSubscription
        }}>
            { children}
        </RegisterContext.Provider>
    );
}